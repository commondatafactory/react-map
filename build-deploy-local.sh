#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

yarn build:k8s
docker build . -t localhost:32000/reactmap:latest
docker push localhost:32000/reactmap:latest

kubectl delete -n ory -f deploy-local.yaml || true
sleep 3
kubectl apply -n ory -f deploy-local.yaml


