// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

declare namespace MapboxDraw {
  type DrawMode = DrawModes[keyof DrawModes]

  interface DrawModes {
    IDLE: 'idle'
  }
}
