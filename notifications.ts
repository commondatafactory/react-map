// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

interface NotificationData {
  title?: string
  date: TDateISO
  description: string
  product?: ['DEGO' | 'DOOK']
  updateUrl: string
}

export const notificationsData: NotificationData[] = [
  // Sorteer updates van nieuw naar oud, bovenaan de lijst aanvullen.
  {
    date: '2023-04-25T00:00:00.001Z',
    title: `Energie-armoede data toegevoegd`,
    description: `
    Een nieuw tabblad over energie-armoede met verschillende indicatoren is nu beschikbaar.`,
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.2.3',
    product: ['DEGO'],
  },
  {
    date: '2023-01-20T00:00:00.001Z',
    title: `DOOK lagen zijn aangepast`,
    description: `
    Indien u een account toegewezen heeft gekregen en bent ingelogd, kunt u de benodigde de basisdataset ten behoeve van het informatiebeeld downloaden via de "informatiebeeld autobranche" tab.`,
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.2023.03',
    product: ['DOOK'],
  },
  {
    date: '2022-12-13T00:00:00.001Z',
    title:
      'Laag: Bereidheid Energietranstiemaatregelen toegevoegd aan Sociale Karakteristieken',
    description:
      'Om beter zicht te krijgen op de drijfveren van mensen om wel of niet te verduurzamen, heeft CBS onderzoek gedaan naar de verschillende factoren die meespelen bij de bereidheid van mensen om energietransitiemaatregelen te treffen aan hun woning. Deze laag is nu te bekijken onder Sociale Karakteristieken.',
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.2.2',
    product: ['DEGO'],
  },
  {
    date: '2022-12-13T00:00:00.000Z',
    title: 'Luchtfoto PDOK 25cm actueel, toegevoegd',
    description:
      'Rechtsonder vind u de mogelijkheid de luchtfoto aan te zetten op de kaart.',
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.2.2',
  },
  {
    date: '2022-09-12T00:00:00.000Z',
    title:
      'Grote data update: Nieuwe BAG, EAN codes, Energieklasse en energie gegevens',
    description:
      'Energie gegevens 2020 beschikbaar , Energie label klasse juli 2022, BAG gegevens juni 2022.',
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.2.0',
  },
  {
    date: '2022-09-14T00:00:00.000Z',
    title:
      'Verbetering data: Sociale Karakteristieken,Percentage huishoudens hadden foutieve getallen ',
    description:
      'De lagen Percentage huishoudens bevatte foutieve getallen, deze gingen over het percentage personen ipv huishoudens. Deze lagen bevatten nu de correcte getallen over huishoudens.',
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.2.1',
  },
  {
    date: '2022-07-05T00:00:00.000Z',
    description:
      'Achtergrond kaart geüpdatet - op basis van OSM data van juni 2022',
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.1.1',
  },
  {
    date: '2022-07-05T00:10:00.010Z',
    title: 'Nieuwe lagen in Informatiebeeld Autobranche',
    description: 'Autobranche is aangevuld met RDW en BOVAG data',
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.1.1',
    product: ['DOOK'],
  },
  {
    date: '2022-07-14T00:10:00.010Z',
    title: 'Exporteren adressen met BOVAG en RDW bedrijven mogelijk',
    description:
      'Autobranche is aangevuld met een laag om alle adressen, verblijfsobjecten, staanplaatsen en ligplaatsen te exporteren, met daaraan de BOVAG en RDW gegevens. ',
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.1.2',
    product: ['DOOK'],
  },
  {
    date: '2022-07-05T00:20:00.010Z',
    title: 'Nieuwe lagen in Energieverbruik',
    description:
      'Nieuwe lagen over het gemiddeld energieverbruik per buurt, wijk, gemeente onderverdeeld naar huur/koop en naar typewoning',
    updateUrl:
      'https://gitlab.com/commondatafactory/react-map/-/releases/0.1.1',
    product: ['DEGO'],
  },
  {
    date: '2022-07-14T00:00:00.000Z',
    title: 'DEGO komt naar je toe deze zomer',
    description:
      "In de zomermaanden juli en augustus organiseert het Kennisnetwerk Data en Smart Society een aantal regionale bijeenkomsten over datagedreven werken in de Energietransitie. Wij nodigen u en uw collega's van harte uit om hieraan deel te nemen.",
    product: ['DEGO'],
    updateUrl:
      'https://kennisnetwerkdata.pleio.nl/groups/view/5635563d-3ed7-4337-a81c-25a9798d4512/algemeen/blog/view/9c4fbcf3-aba1-45db-b7f7-51dcb32c66e1/dego-komt-naar-je-toe-deze-zomer',
  },
]
