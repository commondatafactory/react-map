// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Button, Icon } from '@commonground/design-system'
import { useRouter } from 'next/router'
import React, { useState } from 'react'

import { LabelProvider } from '../providers/label-provider'
import { useIsMobile } from '../hooks/use-is-mobile'
import { useMap } from '../hooks/use-map'
import { useTabsLayers } from '../hooks/use-tabs-layers'
import ExternalLinkIcon from '../../public/images/external-link.svg'
import useUser from '../hooks/use-user'
import { generateAuthUrl } from '../utils/generate-auth-url'
import { Legend } from './legend/Legend'
import { MapSpinner } from './map/map-spinner'
import { Modal } from './modal/Modal'
import { Notifications } from './notifications/Notifications'
import Map from './map/Map'
import MenuPanel from './menu-panel'

import * as Styled from './app.styles'

const App = () => {
  const { isMobile } = useIsMobile()
  const { isLoggedIn } = useUser()
  const router = useRouter()

  const [isCollapsed, setIsCollapsed] = useState(false)

  const authUrl = generateAuthUrl(isLoggedIn)

  const handleLogin = async (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()

    let params = `?return-to=${location.search}${location.hash}`
    await router.push(`${authUrl}${params}`)
  }

  return (
    <Styled.Container>
      <MenuPanel isCollapsed={isCollapsed} setIsCollapsed={setIsCollapsed} />

      {!isMobile && <Legend />}

      <LabelProvider>
        <Map />
      </LabelProvider>

      {!isMobile && (
        <>
          <Notifications />

          <Styled.TopRightContainer>
            <Styled.FlexColumn>
              <Button
                variant="secondary"
                as="a"
                href={authUrl}
                onClick={handleLogin}
                style={{ marginTop: '0rem' }}
              >
                {isLoggedIn
                  ? 'Terug naar instellingen'
                  : 'Inloggen/Registreren'}{' '}
                <Icon as={ExternalLinkIcon} inline />
              </Button>
              <MapSpinner />
            </Styled.FlexColumn>

            <Modal />
          </Styled.TopRightContainer>
        </>
      )}
    </Styled.Container>
  )
}

export default App
