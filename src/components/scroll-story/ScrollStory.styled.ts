// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const MainContainer = styled.div`
  position: absolute;
  width: 100vw;
  min-height: 100vh;
  max-height: 100vh;
  overflow-y: scroll;
  overflow-x: hidden;
  z-index: 100000;
  /* background-color: rgba(100,10,10,0.5); */
  /* padding: 10%; */
  padding-top: 100px;
  display: flex;
  flex-direction: column;
  row-gap: 100vh;
  align-items: center;
  /* padding-right: 15px; Avoid width reflow */
  cursor: not-allowed;
  img {
    padding: 20px;
    margin: auto;
  }
  h1 {
    max-width: 268px;
    overflow: hidden;
    text-overflow: ellipsis;
    font-weight: 700;
    font-size: 1.5rem;
    line-height: 2rem;
    z-index: 10;
    margin: 0;
  }
`

export const OpeningsScene = styled.div`
  width: 90%;
  background-color: white;
  padding: 5rem;
  border-radius: var(--spacing02);
  background-color: var(--colorBackground);
  box-shadow: var(--boxShadow);
  flex-grow: 5;
  text-align: center;
  opacity: 0.85;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  cursor: all-scroll;

  h1 {
    font-size: 2rem;
    margin-bottom: 0.5rem;
  }

  p {
    margin-bottom: 0.5rem;
  }
`

export const Chapter = styled.div`
  width: 100%;
  height: 50%;
  display: grid;
  grid-template-columns: 30rem auto;

  border-radius: var(--spacing02);
  /* background-color: var(--colorBackground); */
  box-shadow: var(--boxShadow);
  cursor: all-scroll;

  :last-child {
    background-color: var(--colorBackground);
    padding: 2rem;
    margin: 2rem;
    width: auto;
    margin-bottom: 30vh;
  }
`

export const ClosingContainer = styled.div`
  width: 50%;
  background-color: white;
  height: 50%;
  padding: 50px;
  border-radius: var(--spacing02);
  background-color: var(--colorBackground);
  box-shadow: var(--boxShadow);
  margin-bottom: 90vh;
  cursor: all-scroll;
`

export const Icon = styled.div`
  display: flex;
  cursor: pointer;
  color: rgba(117, 117, 117, 1);
`
