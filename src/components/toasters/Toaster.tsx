// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, useEffect, useRef, useState } from 'react'
import CheckIcon from '../../../public/images/check.svg'
import ErrorIcon from '../../../public/images/error.svg'
import CloseIcon from '../../../public/images/close.svg'

import * as Styled from './Toasters.styled'

interface ToasterProps {
  body?: string
  nth?: number
  title: string
  variant: 'success' | 'error'
  setToasters?: (arg: any) => void
  visibleTime?: number
}

export const Toaster: FC<ToasterProps> = ({
  title,
  body,
  nth,
  variant = 'success',
  setToasters,
  visibleTime = 5000,
}) => {
  const [startUnmount, setStartUnmount] = useState(false)
  const [baseHeight, setBaseHeight] = useState(0)
  const ref = useRef<HTMLDialogElement>()

  const handleRemoveToaster = () => {
    setToasters((prevToasters) =>
      prevToasters.filter((toast) => toast.nth !== nth)
    )
  }

  useEffect(() => {
    setTimeout(() => setStartUnmount(true), visibleTime - 300)
    setTimeout(() => handleRemoveToaster(), visibleTime)
  }, [])

  useEffect(() => {
    if (ref.current) {
      setBaseHeight(ref.current?.clientHeight)
    }
  }, [ref.current])

  return (
    <Styled.Toaster
      baseHeight={baseHeight}
      nth={nth}
      ref={ref}
      unmount={startUnmount}
      variant={variant}
    >
      {variant === 'success' && <CheckIcon />}
      {variant === 'error' && <ErrorIcon />}
      <div>
        {title && <p>{title}</p>}
        {body && <p>{body}</p>}
      </div>
      <CloseIcon onClick={() => handleRemoveToaster()} />
    </Styled.Toaster>
  )
}
