// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useState, useEffect } from 'react'

import { getTotals } from '../../../services/count/services/get-total'
import { StyleLayer } from '../../../data/data-layers'
import { useTabsLayers } from '../../../hooks/use-tabs-layers'

import * as Styled from './Count.styled'

interface CountProps {
  relevantStyleLayer: Partial<StyleLayer>
  selectedPolygonForCount: any
}
export const CountEanCodes: FunctionComponent<CountProps> = ({
  relevantStyleLayer,
  selectedPolygonForCount,
}) => {
  const { activeDataLayers } = useTabsLayers()
  const [totaalEANcodes, setTotaalEANcodes] = useState<number>()

  useEffect(() => {
    if (relevantStyleLayer && selectedPolygonForCount?.id) {
      if (relevantStyleLayer.attrName === 'pand_gas_ean_aansluitingen') {
        Promise.all([getTotals(selectedPolygonForCount, 'eancodes')]).then(
          (data: any) => {
            if (data[0]) {
              data = data[0].data.eancodes || 0
              setTotaalEANcodes(parseInt(data))
            }
          }
        )
      } else if (!selectedPolygonForCount) {
        setTotaalEANcodes(0)
      }
    }
  }, [activeDataLayers[0], selectedPolygonForCount, relevantStyleLayer])

  return (
    <>
      {totaalEANcodes >= 0 && (
        <Styled.Totals>
          <Styled.TotalText>
            <span>{totaalEANcodes.toLocaleString('nl-NL')}</span>{' '}
            gasaansluitingen
          </Styled.TotalText>
        </Styled.Totals>
      )}
    </>
  )
}
