// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useState, useEffect } from 'react'
import * as d3 from 'd3'
import { Feature } from 'maplibre-gl'

import { getVerblijfsobjectCount } from '../../../services/count/get-verblijfsobject-count'
import { StyleLayer } from '../../../data/data-layers'
import { useDraw } from '../../../hooks/use-draw'
import { useTabsLayers } from '../../../hooks/use-tabs-layers'
import Graph from '../../asset-components/graphs/verticalGraph'

import * as Styled from './Count.styled'

interface CountProps {
  relevantStyleLayer: Partial<StyleLayer>
  selectedPolygonForCount: Feature
}
export const CountBuildings: FunctionComponent<CountProps> = ({
  relevantStyleLayer,
  selectedPolygonForCount,
}) => {
  const [vizData, setVizData] = useState([])
  const [color, setColors] = useState()
  const [colorRange, setColorRange] = useState([])

  const { customCountFilter } = useDraw()
  const { activeDataLayers } = useTabsLayers()

  useEffect(() => {
    if (relevantStyleLayer && selectedPolygonForCount?.id) {
      const hasFilter = activeDataLayers[0]?.styleLayers?.find(
        (styleLayer) => styleLayer?.isRelevantLayer
      )?.filterValues

      Promise.all([
        getVerblijfsobjectCount(
          selectedPolygonForCount,
          relevantStyleLayer.attrName,
          hasFilter ? customCountFilter : null
        ),
      ]).then((data: any) => {
        if (data[0]) {
          data = data[0].data
          if (
            relevantStyleLayer.attrName === 'energieklasse_score' &&
            relevantStyleLayer.legendStops.length > 4
          ) {
            setVizData([
              {
                key: 'A++++',
                value: data['A++++'] ? parseInt(data['A++++'].count) : 0,
              },
              {
                key: 'A+++',
                value: data['A+++'] ? parseInt(data['A+++'].count) : 0,
              },
              {
                key: 'A++',
                value: data['A++'] ? parseInt(data['A++'].count) : 0,
              },
              {
                key: 'A+',
                value: data['A+'] ? parseInt(data['A+'].count) : 0,
              },
              { key: 'A', value: data.A ? parseInt(data.A.count) : 0 },
              { key: 'B', value: data.B ? parseInt(data.B.count) : 0 },
              { key: 'C', value: data.C ? parseInt(data.C.count) : 0 },
              { key: 'D', value: data.D ? parseInt(data.D.count) : 0 },
              { key: 'E', value: data.E ? parseInt(data.E.count) : 0 },
              { key: 'F', value: data.F ? parseInt(data.F.count) : 0 },
              { key: 'G', value: data.G ? parseInt(data.G.count) : 0 },
            ])
          } else if (
            relevantStyleLayer.attrName === 'energieklasse_score' &&
            relevantStyleLayer.legendStops.length === 4
          ) {
            setVizData([
              { key: 'D', value: data.D ? parseInt(data.D.count) : 0 },
              { key: 'E', value: data.E ? parseInt(data.E.count) : 0 },
              { key: 'F', value: data.F ? parseInt(data.F.count) : 0 },
              { key: 'G', value: data.G ? parseInt(data.G.count) : 0 },
            ])
          } else {
            const newData = Object.entries(data).map(
              ([key, { count }]: any) => ({
                key,
                value: parseInt(count),
              })
            )
            const totalRecords = d3.count(newData, (d: any) => d.key)
            // Making bins when too many categories
            if (totalRecords > 15) {
              const occuringValues = newData.map((d) => parseInt(d.key))
              const min = Math.max(d3.min(occuringValues), 1850)
              const max = Math.max(d3.max(occuringValues), 2023)
              const bin = d3.bin().domain([min, max]).thresholds(12) // Divide over a max of 12 bins
              const bins = bin(occuringValues)
              const dataForVisualisation = bins.map((element) =>
                element.length === 1
                  ? {
                      key: `${element[0]}`,
                      value: data[element[0]].count,
                    }
                  : {
                      key:
                        element.x0 === element.x1 - 1
                          ? element.x0
                          : `${element.x0}-${element.x1 - 1}`,
                      value: d3.sum(
                        newData.map(
                          (d) =>
                            d.key >= element.x0 && d.key <= element.x1 - 1
                              ? d.value
                              : 0,
                          (a: number, b: number) => a + b
                        )
                      ),
                    }
              )
              const colorValues = bins.map((element: any) =>
                element.length === 1 ? element[0] : element.x1
              )
              setColorRange([...colorValues, bins[bins.length - 1].x1])
              const lastBinnr = bins.length - 1
              setVizData([
                ...dataForVisualisation,
                {
                  key: '' + bins[lastBinnr].x1,
                  value: data[bins[lastBinnr].x1]?.count
                    ? data[bins[lastBinnr].x1].count
                    : 0,
                },
              ])
            } else {
              setColorRange(Object.keys(data))
              setVizData(newData)
            }
          }
        } else {
          setVizData([])
        }
      })
    } else if (!selectedPolygonForCount) {
      setVizData([])
    }
  }, [
    activeDataLayers[0],
    selectedPolygonForCount,
    relevantStyleLayer,
    customCountFilter,
  ])

  // Get color scale for graph
  useEffect(() => {
    if (relevantStyleLayer && vizData && vizData.length >= 1) {
      let stops
      if (relevantStyleLayer.expressionType === 'interpolate') {
        stops = colorRange.map((color) => relevantStyleLayer.colorScale(color))
      } else if (relevantStyleLayer.expressionType === 'match') {
        stops = relevantStyleLayer.colorScale.range()
      }
      setColors(stops || [])
    }
  }, [vizData, colorRange, relevantStyleLayer, activeDataLayers[0]])

  return (
    selectedPolygonForCount?.id && (
      <>
        {vizData?.length >= 1 && (
          <>
            <Styled.GraphTitle>
              Verdeling {activeDataLayers[0].name} van verblijfsobjecten
            </Styled.GraphTitle>

            {color && vizData?.length >= 1 && (
              <Graph
                dataSet={vizData}
                colors={color}
                margin={{ top: 10, right: 0, bottom: 65, left: 35 }}
                height={230}
                time={750}
                labels="true"
                average={undefined}
              />
            )}
          </>
        )}
      </>
    )
  )
}
