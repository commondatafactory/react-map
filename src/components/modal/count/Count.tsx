// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FC, useState, useEffect } from 'react'
import { Button, Icon } from '@commonground/design-system'

import { getTotals } from '../../../services/count/services/get-total'
import { getVerblijfsobjectDocument } from '../../../services/count/get-verblijfsobject-count'
import { StyleLayer } from '../../../data/data-layers'
import { Tooltip } from '../../asset-components/tooltip/Tooltip'
import { useConfig } from '../../../hooks/use-config'
import { useDraw } from '../../../hooks/use-draw'
import { useTabsLayers } from '../../../hooks/use-tabs-layers'
import Add from '../../../../public/images/add.svg'
import Delete from '../../../../public/images/delete.svg'
import { Download } from './Download'
import { CountEanCodes } from './CountEanCodes'
import { CountDownloadAutoBranche } from './CountDownloadAutoBranche'
import { CountBuildings } from './CountBuildings'

import * as Styled from './Count.styled'
import { CountDownloadKVK } from './CountDownloadHandelsRegister'

interface CountProps {
  handleModalClose: () => void
}

export type DownloadFileType = 'csv' | 'xlsx'

export const Count: FC<CountProps> = () => {
  const [relevantStyleLayer, setRelevantStyleLayer] =
    useState<Partial<StyleLayer>>()
  const config = useConfig()
  const [totaalVerblijfsObject, setTotaalVerblijfsObject] = useState(0)
  const [totaalWoningEq, setTotaalWoningEqu] = useState(0)
  const {
    selectedPolygonForCount,
    setDeleteSelectedFeature,
    setStartDrawing,
    featureCollection,
    customCountFilter,
  } = useDraw()
  const { activeDataLayers } = useTabsLayers()

  const [countFilter, setCountFilter] = useState(null)

  const handleDataDownload = (fileType: DownloadFileType) => {
    getVerblijfsobjectDocument(selectedPolygonForCount, countFilter, fileType)
  }

  // get the relevant StyleLayer and set the count filter
  useEffect(() => {
    const firstActiveDataLayer = activeDataLayers.find(
      (datalayer) => datalayer?.id
    )

    if (firstActiveDataLayer.styleLayers?.[0]) {
      const layer = firstActiveDataLayer.styleLayers.find(
        (currentLayer) => currentLayer.id !== 'layer0'
      )
      setRelevantStyleLayer(layer)
    }

    if (firstActiveDataLayer.id === 'layer51') {
      setCountFilter(
        'any_match-energieklasse=D&any_match-energieklasse=E&any_match-energieklasse=F&any_match-energieklasse=G'
      )
    } else {
      setCountFilter(customCountFilter || null)
    }
  }, [activeDataLayers])

  // Get general data requests
  useEffect(() => {
    if (relevantStyleLayer && selectedPolygonForCount?.id) {
      Promise.all([
        getTotals(selectedPolygonForCount, 'woningequivalent'),
      ]).then((data: any) => {
        if (data[0]) {
          data = data[0]
          setTotaalVerblijfsObject(parseInt(data.totaal))
          if (data.data) {
            setTotaalWoningEqu(parseInt(data.data.woningenquivalent))
          }
        }
      })
    } else {
      setTotaalVerblijfsObject(0)
      setTotaalWoningEqu(0)
    }
  }, [selectedPolygonForCount, relevantStyleLayer])

  const handleAddAreaClick = () => {
    setStartDrawing('polygons')
  }

  const handleDeleteAreaClick = () => {
    setDeleteSelectedFeature(true)
  }

  const tileSource = relevantStyleLayer?.tileSource

  return (
    <>
      {/* Je gaat tekenen: is zichtbaar bij geen selectie */}
      {!featureCollection ? (
        <Styled.Intro>
          <img
            src="/images/count-tool-instructions.gif"
            alt="uitleg tekenen op een kaart"
          />
          <p>
            Klik rondom een gebied op de kaart. Klik op het laatste punt om het
            gebied af te ronden.
          </p>
          <p>Let op, het vlak mag zichzelf niet kruisen.</p>
        </Styled.Intro>
      ) : (
        !selectedPolygonForCount?.id && (
          <Styled.Intro>
            <p>Selecteer of verwijder een bestaand gebied</p>
          </Styled.Intro>
        )
      )}

      {selectedPolygonForCount?.id && (
        <>
          {/* Totalen: altijd zichtbaar bij  */}
          <Styled.Totals>
            {activeDataLayers[0].id !== 'layerDook100' && (
              <Styled.TotalText>
                <span>{totaalVerblijfsObject.toLocaleString('nl-NL')}</span>
                Verblijfsobjecten
              </Styled.TotalText>
            )}

            {config.product === 'DEGO' && (
              <Styled.TotalText>
                <span>{totaalWoningEq.toLocaleString('nl-NL')}</span>
                <Tooltip
                  text="Niet alle utiliteitsgebouwen zijn even groot. Om toch te kunnen vergelijken, worden utiliteitsgebouwen vaak omgerekend naar woningequivalenten. Daarbij staat 130 m² utiliteit gelijk aan 1 woningequivalent (weq). Een woning is ook gelijk aan 1 woningequivalent."
                  position="bottom"
                >
                  Woningequivalenten
                </Tooltip>
              </Styled.TotalText>
            )}
            {relevantStyleLayer?.attrName === 'pand_gas_ean_aansluitingen' && (
              <CountEanCodes
                relevantStyleLayer={relevantStyleLayer}
                selectedPolygonForCount={selectedPolygonForCount}
              />
            )}
          </Styled.Totals>

          {/* Graph specifics: bij energie */}
          <>
            {((activeDataLayers[0].drawType === 'count' &&
              tileSource?.sourceTitle === 'bag' &&
              relevantStyleLayer.attrName !== 'pand_gas_ean_aansluitingen') ||
              (tileSource?.sourceTitle === 'cbs2022pc64spuk' &&
                relevantStyleLayer.attrName === 'energieklasse_score')) && (
              <CountBuildings
                relevantStyleLayer={relevantStyleLayer}
                selectedPolygonForCount={selectedPolygonForCount}
              />
            )}
          </>

          {/* Verblijfsobjecten tellen: bij KVK data */}
          <>
            {tileSource?.sourceTitle === 'verblijfsobjecten' &&
              (activeDataLayers[0].tab === 'autobranche' ||
                activeDataLayers[0].tab === 'kvk' ||
                activeDataLayers[0].tab === 'bedrijventerreinen') &&
              activeDataLayers[0].drawType === 'count' &&
              activeDataLayers[0].id !== 'verblijfsobjecten-download-000' && (
                <CountDownloadAutoBranche
                  selectedPolygonForCount={selectedPolygonForCount}
                  activeData={
                    activeDataLayers[0].tab === 'autobranche'
                      ? 'autobranche'
                      : 'bedrijventerreinen'
                  }
                />
              )}

            {/* Download: bij KVK data */}
            {(tileSource?.sourceTitle === 'bag' ||
              tileSource?.sourceTitle === 'verblijfsobjecten' ||
              tileSource?.sourceTitle === 'cbs2022pc64spuk') &&
              activeDataLayers[0].tab !== 'autobranche' &&
              activeDataLayers[0].id !== 'bedrijventerreinen-download-000' &&
              activeDataLayers[0].id !== 'layerDook100' &&
              activeDataLayers[0].drawType === 'count' && (
                <Download
                  handleDataDownload={handleDataDownload}
                  excelDisabled={totaalVerblijfsObject >= 100000}
                />
              )}

            {activeDataLayers[0].id === 'layerDook100' &&
              activeDataLayers[0].drawType === 'count' && (
                <CountDownloadKVK
                  selectedPolygonForCount={selectedPolygonForCount}
                  activeData={
                    activeDataLayers[0].tab === 'autobranche'
                      ? 'autobranche'
                      : 'bedrijventerreinen'
                  }
                />
              )}
          </>
        </>
      )}

      {!!featureCollection && (
        <Styled.Buttons>
          <Button variant="secondary" onClick={handleAddAreaClick}>
            <Icon as={Add} inline />
            <p>Gebied toevoegen</p>
          </Button>
          <Button variant="secondary" onClick={handleDeleteAreaClick}>
            {selectedPolygonForCount ? (
              <>
                <Icon as={Delete} inline /> <p>Gebied verwijderen</p>
              </>
            ) : (
              'Gebied selecteren'
            )}
          </Button>
        </Styled.Buttons>
      )}
    </>
  )
}
