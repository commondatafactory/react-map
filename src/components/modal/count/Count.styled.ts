// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Intro = styled.div`
  color: var(--colorPaletteGray600);
  font-size: var(--fontSizeSmall);
  line-height: var(--lineHeightText);
  text-align: center;
  margin-left: var(--spacing07);
  margin-right: var(--spacing07);
  > p {
    margin: var(--spacing04);
  }
`

export const Totals = styled.div`
  width: 100%;
  flex-wrap: wrap;
  position: relative;
  margin-bottom: var(--spacing07);
`
export const TotalText = styled.p`
  align-items: baseline;
  display: flex;
  min-width: 45%;
  color: var(--colorPaletteGray600);
  font-size: var(--fontSizeSmall);
  line-height: var(--lineHeightText);
  white-space: nowrap;

  & > span {
    color: var(--colorPaletteGray900);
    font-size: var(--fontSizeXXLarge);
    line-height: var(--lineHeightText);
    padding-right: 0.25rem;
  }
`

export const Buttons = styled.div`
  gap: var(--spacing05);
  display: flex;
  margin-top: var(--spacing07);
  p {
    font-size: var(--fontSizeSmall);
  }
`

export const GraphTitle = styled.p`
  text-align: center;
  margin-bottom: var(--spacing02);
`

export const Download = styled.div`
  text-align: center;
  margin-top: var(--spacing03);
  button {
    margin-left: 15px;
  }
`
