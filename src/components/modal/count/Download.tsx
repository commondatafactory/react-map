// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent } from 'react'
import { Button, Icon } from '@commonground/design-system'
import DownloadIcon from '../../../../public/images/download.svg'
import * as Styled from './Count.styled'

interface DownloadProps {
  handleDataDownload
  disabled?: boolean
  excelDisabled?: boolean
}

export const Download: FunctionComponent<DownloadProps> = ({
  handleDataDownload,
  disabled = false,
  excelDisabled,
}) => {
  return (
    <Styled.Download>
      Download
      <Button
        variant="link"
        size="small"
        onClick={() => handleDataDownload('csv')}
        disabled={disabled}
      >
        <Icon as={DownloadIcon} inline />
        CSV
      </Button>
      <Button
        variant="link"
        size="small"
        onClick={() => handleDataDownload('xlsx')}
        disabled={disabled || excelDisabled}
      >
        <Icon as={DownloadIcon} inline />
        Excel
      </Button>
      {excelDisabled && (
        <p style={{ fontSize: '0.875rem' }}>
          Bij meer dan 100.000 verblijfsobjecten kun je geen Excel downloaden.
          Maak je selectie kleiner.
        </p>
      )}
    </Styled.Download>
  )
}
