// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const MainContainer = styled.div<{ width: number }>`
  width: 30vw;
  max-width: ${(props) => props.width};
  border-radius: var(--spacing02);
  background-color: var(--colorBackground);
  box-shadow: var(--boxShadow);
  z-index: 2;
  /* max-height: 90vh; */
  max-height: calc(100vh - 230px);
  position: relative;
  display: flex;
  flex-direction: column;
  padding: var(--spacing07) var(--spacing07) 0;

  /* intermediate */
  @media screen and (max-width: 1250px) {
    max-width: 350px;
    max-height: 80vh;
    padding: var(--spacing05) var(--spacing05) 0;
    overflow: hidden;
  }
`
export const Header = styled.div`
  display: flex;

  > div {
    margin-left: var(--spacing05);
  }
  > div:first-of-type {
    margin-left: auto;
  }

  & h1 {
    max-width: 268px;
    overflow: hidden;
    text-overflow: ellipsis;
    font-weight: var(--fontWeightBold);
    font-size: var(--fontSizeLarge);
    line-height: var(--lineHightHeading);
    /* z-index: 10; */
    margin: 0;
  }
`

export const Icon = styled.div`
  display: flex;
  cursor: pointer;
  color: var(--colorPaletteGray600);
`

export const Description = styled.p`
  font-size: var(--fontSizeLarge);
  line-height: var(--lineHeightText);
`

export const Content = styled.div`
  padding: var(--spacing07) 0;
  overflow: auto;
  height: 100%;
  position: relative;
  @media screen and (max-width: 1250px) {
    padding: var(--spacing06) 0;
  }
`
