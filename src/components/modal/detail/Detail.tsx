// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, useState, useEffect } from 'react'
import { Icon, IconFlippingChevron } from '@commonground/design-system'

import ExternalIcon from '../../../../public/images/external-link.svg'
import { useMap } from '../../../hooks/use-map'
import * as Styled from './Detail.styled'

interface DetailProps {
  setTitle
  feature
  filteredFeatures
}

export const Detail: FC<DetailProps> = ({
  setTitle,
  feature,
  filteredFeatures,
}) => {
  const { selectedFeatureAndEvent } = useMap()

  const isOnlyFeature = filteredFeatures.length === 1

  const [isOpen, setIsOpen] = useState(isOnlyFeature)

  const content =
    selectedFeatureAndEvent.relevantStyleLayerDetailModal?.detailModal({
      properties: feature.properties,
      id: feature.id,
    })

  useEffect(() => {
    setTitle(
      isOnlyFeature
        ? content.title
        : filteredFeatures.length + ' resultaten gevonden'
    )
  }, [])

  const toggle = () => {
    if (!isOnlyFeature) {
      setIsOpen(!isOpen)
    }
  }

  return (
    <Styled.Feature onClick={toggle} role={!isOnlyFeature ? 'button' : ''}>
      {!isOnlyFeature && (
        <div className="heading">
          <h5>{content.title}</h5>
          <IconFlippingChevron
            flipHorizontal={isOpen}
            animationDuration={300}
          />
        </div>
      )}

      {isOpen && (
        <>
          {content?.content.map(
            (item, index) =>
              item && (
                <Styled.Item key={`${item},${index}`}>
                  <p>{item[0]}</p>
                  <p>{item[1]}</p>
                </Styled.Item>
              )
          )}
          {content?.url && (
            <Styled.Item>
              <a target="blank" href={content.url}>
                {content.urlTitle ? content.urlTitle : 'Externe link'}{' '}
                <Icon as={ExternalIcon} size="small" />
              </a>
            </Styled.Item>
          )}
          {selectedFeatureAndEvent?.relevantStyleLayerDetailModal?.tileSource
            ?.source !== 'cbs' && (
            <Styled.Item>
              {/* http://web.archive.org/web/20110903160743/http://mapki.com/wiki/Google_Map_Parameters#Street_View */}
              <a
                target="blank"
                onClick={(e) => {
                  e.stopPropagation()
                }}
                href={`http://maps.google.com/maps?layer=c&cbll=${selectedFeatureAndEvent.event.lngLat.lat},${selectedFeatureAndEvent.event.lngLat.lng}&cbp=11,90,0,0,0`}
              >
                Bekijk deze locatie op Google streetview{' '}
                <Icon as={ExternalIcon} size="small" />
              </a>
            </Styled.Item>
          )}
        </>
      )}
    </Styled.Feature>
  )
}
