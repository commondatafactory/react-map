// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Item = styled.div`
  display: flex;
  gap: var(--spacing05);

  > p {
    flex: 1 1 0%;
    font-size: var(--fontSizeSmall);
    line-height: var(--lineHeightText);
    color: var(--colorPaletteGray600);
    margin-bottom: var(--spacing01);
  }

  > p:last-child {
    font-weight: var(--fontWeightBold);
    margin-bottom: var(--spacing05);
    color: var(--colorPaletteGray900);
    font-size: var(--fontSizeMedium);
    align-items: center;
    display: flex;
  }

  > ul li {
    font-weight: var(--fontWeightBold);
    margin-bottom: var(--spacing01);
    color: var(--colorPaletteGray900);
    font-size: var(--fontSizeMedium);
    list-style: circle;
  }

  > ul {
    flex: 1 1 0%;
    margin-bottom: var(--spacing05);
  }

  > a {
    align-items: center;
    display: flex;
    font-weight: var(--fontWeightBold);
    text-decoration: underline;
    font-size: var(--fontSizeMedium);
    margin-bottom: var(--spacing05);
    color: var(--colorPaletteGray800);
  }
`
export const Address = styled.p`
  font-size: var(--fontSizeSmall);
  padding: var(--spacing03);
  border-bottom: var(--colorPaletteGray400) 1px solid;
`
export const Title = styled.p`
  color: var(--colorPaletteGray600);
  font-size: var(--fontSizeSmall);
  line-height: var(--lineHeightText);
  margin-bottom: var(--spacing01);
`
export const StyledGraph = styled.div`
  .graphTitle {
    margin-top: 1.125rem;
    text-align: center;
    margin-bottom: var(--spacing02);
  }
  > h4 {
    margin-bottom: var(--spacing05);
    color: var(--colorPaletteGray900);
    font-size: var(--fontSizeMedium);
  }
  > h5 {
    font-size: var(--fontSizeSmall);
    line-height: var(--lineHeightText);
    color: var(--colorPaletteGray600);
    flex: 1 1 0%;
    margin-bottom: var(--spacing01);
  }
  > div > h5 {
    font-size: var(--fontSizeSmall);
    line-height: var(--lineHeightText);
    color: var(--colorPaletteGray600);
    flex: 1 1 0%;
    margin-bottom: var(--spacing01);
  }
`

export const Feature = styled.div`
  padding: var(--spacing03);
  border-bottom: var(--colorPaletteGray400) 1px solid;

  &:last-child {
    border-bottom: 0;
  }

  .heading {
    display: flex;
    justify-content: space-between;

    > h5 {
      line-height: var(--lineHeightText);
      flex: 1 1 0%;
      margin-bottom: var(--spacing01);
      color: var(--colorPaletteGray900);
      font-weight: var(--fontWeightBold);
    }
  }
`
