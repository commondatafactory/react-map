// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, useEffect, useState } from 'react'
import { Icon, IconFlippingChevron } from '@commonground/design-system'

import { getRdwErkenning } from '../../../services/count/services/get-rdw-erkenning'
import ExternalIcon from '../../../../public/images/external-link.svg'

import { useMap } from '../../../hooks/use-map'
import * as Styled from './Detail.styled'

interface DetailProps {
  setTitle
  feature
  filteredFeatures
}

export const RDWdetail: FC<DetailProps> = ({
  setTitle,
  feature,
  filteredFeatures,
}) => {
  const { selectedFeatureAndEvent } = useMap()

  const isOnlyFeature = filteredFeatures.length === 1

  const [isOpen, setIsOpen] = useState(isOnlyFeature)
  const [erkenningLijst, setErkenningLijst] = useState<string[]>([])

  const content =
    selectedFeatureAndEvent.relevantStyleLayerDetailModal?.detailModal({
      id: feature.id,
      properties: feature.properties,
    })

  useEffect(() => {
    setTitle(
      isOnlyFeature
        ? content.title
        : filteredFeatures.length + ' resultaten gevonden'
    )
    ;(async () => {
      try {
        const { mappedErkenningen } = await getRdwErkenning(
          feature.properties.volgnummer
        )

        setErkenningLijst(mappedErkenningen)
      } catch (err) {
        console.warn('RDW API probleem:', err)
      }
    })()
  }, [])

  const toggle = () => {
    if (!isOnlyFeature) {
      setIsOpen(!isOpen)
    }
  }

  return (
    <Styled.Feature onClick={toggle} role={!isOnlyFeature ? 'button' : ''}>
      {!isOnlyFeature && (
        <div className="heading">
          <h5>{content.title}</h5>
          <IconFlippingChevron
            flipHorizontal={isOpen}
            animationDuration={300}
          />
        </div>
      )}

      {isOpen && (
        <>
          {content.content.map((item) =>
            item ? (
              <Styled.Item key={item.join('')}>
                <p>{item[0]}</p>
                <p>{item[1]}</p>
              </Styled.Item>
            ) : null
          )}

          {content.url && (
            <Styled.Item>
              <a target="blank" href={content.url}>
                {content.urlTitle ? content.urlTitle : 'Externe link'}{' '}
                <Icon as={ExternalIcon} size="small" />
              </a>
            </Styled.Item>
          )}

          {feature.properties.volgnummer && (
            <Styled.Item>
              <p>Erkenningen</p>
              <ul>
                {erkenningLijst.map(
                  (item) => item && <li key={item}>{item}</li>
                )}
              </ul>
            </Styled.Item>
          )}

          <Styled.Item>
            <a
              target="blank"
              href={`https://bagviewer.kadaster.nl/lvbag/bag-viewer/index.html#?searchQuery=${feature.properties.vid}`}
            >
              Bag viewer <Icon as={ExternalIcon} size="small" />
            </a>
          </Styled.Item>

          {selectedFeatureAndEvent && (
            <Styled.Item>
              {/* http://web.archive.org/web/20110903160743/http://mapki.com/wiki/Google_Map_Parameters#Street_View */}
              <a
                target="blank"
                href={`http://maps.google.com/maps?q=${feature.properties.straat} ${feature.properties.huisnummer} ${feature.properties.postcode} ${feature.properties.plaats},&layer=c&cbll=${selectedFeatureAndEvent.event.lngLat.lat},${selectedFeatureAndEvent.event.lngLat.lng}&cbp=11,90,0,0,0`}
              >
                Bekijk deze locatie op Google streetview{' '}
                <Icon as={ExternalIcon} size="small" />
              </a>
            </Styled.Item>
          )}
        </>
      )}
    </Styled.Feature>
  )
}
