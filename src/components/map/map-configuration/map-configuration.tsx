// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FC, useContext, MouseEvent, ChangeEvent } from 'react'

import { MapContext } from '../../../providers/map-provider'
import { RadioButton } from '../../asset-components/radio-button/RadioButton'
import { ToggleSwitch } from '../../asset-components/toggle-switch'
import { useLabels } from '../../../hooks/use-labels'
import { Tooltip } from '../../asset-components/tooltip/Tooltip'

import * as Styled from './map-configuration.styled'

interface MapConfigurationProps {
  disablethreeDimensional: boolean
}

export const MapConfiguration: FC<MapConfigurationProps> = ({
  disablethreeDimensional,
}): JSX.Element => {
  const { threeDimensional, toggleThreeDimensional } = useContext(MapContext)
  const {
    labelState,
    toggleLabelState,
    aerialPhotosState,
    setAerialPhotosState,
  } = useLabels()

  function handleClick(e: MouseEvent | ChangeEvent, value: string) {
    e.preventDefault()
    toggleLabelState(value)
  }

  return (
    <Styled.Container>
      <Styled.Item onClick={(e) => handleClick(e, 'geen')}>
        <RadioButton
          value="Geen Labels"
          checked={labelState === 'geen'}
          name="options"
          onChange={(e) => handleClick(e, 'geen')}
        />
        <span>Geen Labels</span>
      </Styled.Item>

      <Styled.Item onClick={(e) => handleClick(e, 'topo')}>
        <RadioButton
          value="Topografische labels"
          checked={labelState === 'topo'}
          name="options"
          onChange={(e) => handleClick(e, 'topo')}
        />
        <span>Topografische labels</span>
      </Styled.Item>

      <Styled.Item onClick={(e) => handleClick(e, 'admin')}>
        <RadioButton
          value="Administratieve labels en grenzen"
          checked={labelState === 'admin'}
          name="options"
          onChange={(e) => handleClick(e, 'admin')}
        />
        <Tooltip
          position="top"
          text="CBS buurt, wijk en gemeenten grenzen van 2021"
        >
          <span id="adminMetadata">Administratieve labels en grenzen</span>
        </Tooltip>
      </Styled.Item>

      <Styled.Item
        onClick={() => !disablethreeDimensional && toggleThreeDimensional()}
        disabled={disablethreeDimensional}
      >
        <ToggleSwitch
          checked={threeDimensional}
          disabled={disablethreeDimensional}
          name="Object hoogte"
          type="checkbox"
          onChange={(e) => e}
        />
        <span>Object hoogte</span>
      </Styled.Item>
      <Styled.Item>
        {/* <Checkbox
          name="Luchtfoto"
          onChange={(e) => setAerialPhotosState(!aerialPhotosState)}
        /> */}

        <ToggleSwitch
          checked={aerialPhotosState}
          name="Luchtfoto"
          type="checkbox"
          onChange={(e) => setAerialPhotosState(!aerialPhotosState)}
        />
        <span>Luchtfoto</span>
      </Styled.Item>
    </Styled.Container>
  )
}
