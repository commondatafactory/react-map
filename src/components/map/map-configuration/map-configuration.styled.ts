// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Container = styled.div`
  /* No mobile styling needed */
  position: absolute;
  user-select: none;
  min-height: 3.5rem;
  bottom: 0;

  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.24);
  backdrop-filter: blur(3px);
  background: rgba(255, 255, 255, 0.7);
  z-index: 2;

  @media screen and (min-width: 900px) {
    display: grid;
    grid-gap: 0;
    min-height: 3rem;
    grid-template-columns: repeat(3, auto);
    grid-template-rows: repeat(2, 1fr);
    justify-items: start;
    justify-content: space-between;
    padding: var(--spacing01);
    width: -moz-available;
    width: -webkit-fill-available;
    p,
    span {
      font-size: var(--fontSizeSmall);
    }
  }

  @media screen and (min-width: 1401px) {
    width: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: center;

    padding: var(--spacing04);

    p,
    span {
      font-size: var(--fontSizeMedium);
    }
  }
`

export const Item = styled.div<{ disabled?: boolean }>`
  align-items: center;
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
  display: flex;
  margin-left: var(--spacing06);
  margin-right: var(--spacing02);
  text-decoration: ${({ disabled }) => (disabled ? 'line-through' : '')};

  /* intermediate */
  @media screen and (max-width: 1250px) {
    margin-left: var(--spacing03);
    margin-right: var(--spacing03);
  }
`
