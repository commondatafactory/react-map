// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import { Alert as ImportAlert } from '@commonground/design-system'

export const Map = styled.div`
  display: block;
  position: absolute;
  height: 100%;
  max-height: calc(100vh - 64px);
  top: 64px;
  width: 100vw;
  margin: 0;
  padding: 0;
  transition: width 550ms ease-in-out;

  .maplibregl-ctrl-bottom-right {
    display: none;
  }

  &.mouse-pointer .maplibregl-canvas-container.maplibregl-interactive {
    cursor: pointer;
  }
  &.mouse-move .maplibregl-canvas-container.maplibregl-interactive {
    cursor: move;
  }
  &.mouse-add .maplibregl-canvas-container.maplibregl-interactive {
    cursor: crosshair;
  }

  &.mouse-pointer .maplibregl-canvas-container.maplibregl-interactive {
    cursor: pointer;
  }
  &.mouse-move .maplibregl-canvas-container.maplibregl-interactive {
    cursor: move;
  }
  &.mouse-add .maplibregl-canvas-container.maplibregl-interactive {
    cursor: crosshair;
  }
  &.mouse-move.mode-direct_select
    .maplibregl-canvas-container.maplibregl-interactive {
    cursor: grab;
    cursor: -moz-grab;
    cursor: -webkit-grab;
  }

  .maplibregl-draw_boxselect {
    pointer-events: none;
    position: absolute;
    top: 0;
    left: 0;
    width: 0;
    height: 0;
    background: rgba(0, 0, 0, 0.1);
    border: 2px dotted #fff;
    opacity: 0.5;
  }

  @media screen and (min-width: 900px) {
    grid-column: 2 / span 1;
    grid-row: 1;
    align-self: stretch;
    width: auto;
    height: 100vh;
    max-height: 100vh;
    position: relative;
    top: 0px;

    .maplibregl-ctrl-bottom-right {
      display: block;
      /* padding: 0; */
      padding-bottom: calc(3rem + var(--spacing02));
      font-size: var(--fontSizeSmall);
    }

    .maplibregl-ctrl-bottom-left {
      padding-bottom: var(--spacing09);
    }

    .maplibregl-ctrl.maplibregl-ctrl-attrib {
      box-shadow: none !important;
    }
    .maplibregl-ctrl.maplibregl-ctrl-scale {
      background: none !important;
      box-shadow: none !important;
    }

    & .maplibregl-popup.selection {
      pointer-events: none;
      font-size: var(--fontSizeMedium);
      h4 {
        font-weight: bold;
      }
    }
    & .maplibregl-popup-content {
      pointer-events: none;
      h4 {
        font-weight: bold;
      }
    }

    .maplibregl-ctrl.maplibregl-ctrl-group
      button.maplibregl-ctrl-graphImage
      span.maplibregl-ctrl-icon {
      background-image: url('/images/bar-chart.svg');
    }

    .maplibregl-ctrl.maplibregl-ctrl-group button.maplibregl-ctrl-spinner {
      pointer-events: none !important;
    }

    /* Custom styling controls */

    .maplibregl-ctrl.maplibregl-ctrl {
      margin: 0 !important;

      box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.24) !important;
    }

    .maplibregl-ctrl-bottom-right div.maplibregl-ctrl.maplibregl-ctrl-group {
      margin-right: 1rem !important;
      margin-bottom: 1rem !important;
    }

    .maplibregl-ctrl-top-left div.maplibregl-ctrl.maplibregl-ctrl-group {
      margin-top: 1rem !important;
      margin-left: 1rem !important;
    }

    .maplibregl-ctrl-bottom-left div.maplibregl-ctrl.maplibregl-ctrl-scale {
      margin-left: 1rem !important;
      margin-bottom: 1rem !important;
    }

    .maplibregl-ctrl.maplibregl-ctrl-group button {
      width: 36px;
      height: 36px;
      padding: 8px !important;
    }

    .maplibregl-ctrl-zoom-out > span {
      background-image: url('/images/subtract-line.svg') !important;
      background-size: 20px 20px !important;
      background-repeat: no-repeat;
    }

    .maplibregl-ctrl-zoom-in > span {
      background-image: url('/images/add-line.svg') !important;
      background-size: 20px 20px !important;
      background-repeat: no-repeat;
    }

    .maplibregl-ctrl-compass > span {
      background-image: url('/images/compass.svg') !important;
      background-size: 20px 20px !important;
      background-repeat: no-repeat;
    }

    .maplibregl-ctrl-fullscreen > span {
      background-image: url('/images/fullscreen.svg') !important;
      background-size: 20px 20px !important;
      background-repeat: no-repeat;
    }
  }
`

export const MapFooter = styled.div`
  display: none;

  @media screen and (min-width: 900px) {
    display: block;
    grid-column: 2 / span 1;
    grid-row: 1 / span 7;
    align-self: stretch;
    margin: 0;
    padding: 0;
    height: 100vh;
    pointer-events: none;

    > * {
      pointer-events: initial;
    }
  }
`

export const Alert = styled(ImportAlert)`
  > * {
    max-width: ${({ maxWidth }) => `calc(100% - ${maxWidth}px)` || ''};
  }
`
