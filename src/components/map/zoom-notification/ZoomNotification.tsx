// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Icon } from '@commonground/design-system'
import ZoomIconIn from '../../../../public/images/zoom.svg'
import ZoomIconOut from '../../../../public/images/zoom-out-line.svg'

import * as Styled from './ZoomNotification.styled'

interface ZoomNotificationProps {
  isVisible: boolean
  zoomDirection: 'in' | 'uit'
}

export const ZoomNotification = ({
  isVisible,
  zoomDirection,
}: ZoomNotificationProps) => {
  return (
    <Styled.Container $isVisible={isVisible}>
      <Icon as={zoomDirection === 'in' ? ZoomIconIn : ZoomIconOut} inline />
      <Styled.Text>
        Zoom {zoomDirection} om de geselecteerde kaartlaag te zien
      </Styled.Text>
    </Styled.Container>
  )
}
