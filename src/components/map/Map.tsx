// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useRef, useState, useEffect, FC } from 'react'
import maplibregl, { LngLatLike, StyleSpecification } from 'maplibre-gl'

import 'maplibre-gl/dist/maplibre-gl.css'
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css'

import { sourceSettings } from '../../data/tile-sources'
import { StyleLayer } from '../../data/data-layers'
import { useDraw } from '../../hooks/use-draw'
import { useMap } from '../../hooks/use-map'
import { useTabsLayers } from '../../hooks/use-tabs-layers'
import useUser from '../../hooks/use-user'
import MapContextLayers from './MapContextLayers'
import MapRelevantLayers from './MapRelevantLayers'
import { MapDrawTool } from './MapDrawTool'
import { MapConfiguration } from './map-configuration'
import MapStyleLight from './../../data/mapstyle-osm-grey-v2.json'

import * as Styled from './map.styles'

const drawerWidth = 553

const Map: FC = () => {
  const { setMapLoading, threeDimensional, setAnalyzeMode, analyzeMode } =
    useMap()

  const { activeDataLayers } = useTabsLayers()
  const { drawMode } = useDraw()
  const user = useUser()

  const [customSourcesAdded, setCustomSourcesAdded] = useState(false)
  const [map, setMap] = useState<maplibregl.Map>(null)
  const [relevantStyleLayer, setRelevantStyleLayer] = useState(
    {} as Partial<StyleLayer> | Record<string, never>
  )
  const [warning, setWarning] = useState<any>()

  const mapContainer = useRef(null)

  const styleLayersHaveExtrusion =
    activeDataLayers?.[0]?.styleLayers.some(
      (styleLayer) => styleLayer.hasExtrusion
    ) || relevantStyleLayer?.extrusionAttr

  // Set the relevant StyleLayer
  useEffect(() => {
    if (activeDataLayers[0]?.styleLayers?.length) {
      // TODO: Implement use of `isRelativeLayer`. Or is this not the same as
      // `styleLayer.isRelevantLayer`?
      const layer = activeDataLayers[0].styleLayers.find(
        (currentLayer) => currentLayer.id !== 'layer0'
      )
      setRelevantStyleLayer(layer)

      if (
        relevantStyleLayer?.tileSource !== layer?.tileSource &&
        analyzeMode !== 'count'
      ) {
        setAnalyzeMode(null)
      }
    }
  }, [activeDataLayers[0]])

  // INIT map & controls
  useEffect(() => {
    function isWebglSupported() {
      if (window.WebGLRenderingContext) {
        const canvas = document.createElement('canvas')
        try {
          // Note that { failIfMajorPerformanceCaveat: true } can be passed as a second argument
          // to canvas.getContext(), causing the check to fail if hardware rendering is not available. See
          // https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/getContext
          // for more details.
          const context =
            canvas.getContext('webgl2') || canvas.getContext('webgl')
          if (context && typeof context.getParameter == 'function') {
            return true
          }
        } catch (e) {
          // WebGL is supported, but disabled
        }
        return false
      }
      // WebGL not supported
      return false
    }
    if (!isWebglSupported()) {
      alert(
        'Deze applicatie maakt gebruik van WegGL. Uw browser ondersteunt deze technologie niet. Gebruik a.u.b. een nieuwere browser.'
      )
      return
    }

    const mapStartingLocation = {
      coordinates: [4.308188, 52.087049] as LngLatLike,
      zoom: 16.5,
    }

    const initMap = new maplibregl.Map({
      container: mapContainer.current,
      style: MapStyleLight as StyleSpecification,
      center: mapStartingLocation.coordinates,
      zoom: mapStartingLocation.zoom,
      minZoom: 8,
      maxZoom: 18,
      bearing: 0,
      pitch: 0,
      maxBounds: [
        [1.5, 50],
        [8.6, 54.2],
      ],
      hash: true,
      attributionControl: false,
      locale: {
        'NavigationControl.ZoomIn': 'Zoom in',
        'NavigationControl.ZoomOut': 'Zoom uit',
        'NavigationControl.ResetBearing': 'Draai de kaart terug op het Noorden',
        'FullscreenControl.Enter': 'Volledig scherm',
        'FullscreenControl.Exit': 'Exit volledig scherm',
      },
      transformRequest: (url) => {
        const protectedUrls = [
          'https://auth',
          'https://acc.auth',
          'http://auth',
        ]
        if (
          protectedUrls.some((protectedUrl) => url.startsWith(protectedUrl))
        ) {
          return {
            url,
            credentials: 'include',
          }
        }
      },
    })

    // Attribution control
    initMap.addControl(
      new maplibregl.AttributionControl({
        customAttribution: "<a href='https://commondatafactory.nl/'> VNG</a>",
      }),
      'bottom-right'
    )
    // Navigation control
    initMap.addControl(new maplibregl.NavigationControl({}), 'bottom-right')
    // Scale bar control
    initMap.addControl(
      new maplibregl.ScaleControl({
        maxWidth: 180,
        unit: 'metric',
      }),
      'bottom-left'
    )
    // Fullscreen control
    initMap.addControl(
      new maplibregl.FullscreenControl({
        container: document.querySelector('App'),
      }),
      'bottom-right'
    )

    const handleLoad = (e) => {
      if (!map) {
        setMap(initMap as maplibregl.Map)
        initMap.resize()
      }
    }

    const handleError = (e) => {
      console.warn('handleError', e)

      if (e?.source?.type === 'raster') {
        setWarning({
          title: 'Fout bij het ophalen van de kaart',
          description:
            'De data voor deze kaart is momenteel niet volledig beschikbaar',
        })
      }
    }

    initMap.on('load', handleLoad)
    initMap.on('error', handleError)

    return () => {
      initMap.off('load', handleLoad)
      initMap.off('error', handleError)
    }
  }, [])

  // map Loader/spinner on and off
  useEffect(() => {
    if (!map) {
      return
    }
    const handleRender = () => {
      setMapLoading(true)
    }

    const handleIdle = () => {
      setMapLoading(false)
    }

    map.on('render', handleRender)
    map.on('idle', handleIdle)

    return () => {
      map.off('render', handleRender)
      map.off('idle', handleIdle)
    }
  }, [map])

  // Add data mapSources (context and relevant)
  // Define your custom mapSources in TileSources
  useEffect(() => {
    if (!map) {
      return
    }

    const mapSources = Object.fromEntries(
      Object.entries(sourceSettings).map(([key, value]) => [key, value.source])
    )

    Object.entries(mapSources).forEach(([name, config]: [string, any]) => {
      if (name === 'eigenaren') {
        if (!user?.data?.permissions) {
          return
        }
      }

      const existingSource = map.getSource(name)
      if (!existingSource) {
        map.addSource(name, config)
      }
    })
    setCustomSourcesAdded(!customSourcesAdded)
  }, [map, user?.data])

  // 3D Toggle:  When toggling the 3D function, the map is tilted slightly
  useEffect(() => {
    if (map) {
      if (map.getPitch() === 0 && threeDimensional) {
        map.setPitch(40)
        return
      }
      if (map.getPitch() > 0 && !threeDimensional) {
        map.setPitch(0)
      }
    }
  }, [threeDimensional])

  return (
    <>
      <Styled.Map ref={(el) => (mapContainer.current = el)} />

      {map && drawMode && <MapDrawTool map={map} />}

      {customSourcesAdded && (
        <>
          <MapContextLayers map={map} />
          <MapRelevantLayers map={map} />
        </>
      )}

      <Styled.MapFooter>
        <MapConfiguration disablethreeDimensional={!styleLayersHaveExtrusion} />
        {warning && (
          <Styled.Alert
            title={warning.title}
            variant="warning"
            maxWidth={analyzeMode ? drawerWidth : null}
          >
            {warning.description}
          </Styled.Alert>
        )}
      </Styled.MapFooter>
    </>
  )
}

export default Map
