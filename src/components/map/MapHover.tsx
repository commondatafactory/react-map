// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { FC, useEffect, useState } from 'react'
import { MapGeoJSONFeature, MapLayerMouseEvent } from 'maplibre-gl'

import { useTabsLayers } from '../../hooks/use-tabs-layers'
import { MapPopup } from './MapPopup'

interface MapHoverProps {
  map: maplibregl.Map
  mapLayerId: string
  styleLayer: any
  selectedFeatureID: string | number
}

interface SelectedFeature {
  firstFeature: MapGeoJSONFeature
  event: {
    lngLat: maplibregl.LngLat
    type: string
  }
  relevantStyleLayer
}
interface PopupContextProps {
  feature?: any
  lngLat?: maplibregl.LngLat
  styleLayer?: any
}

const MapHover: FC<MapHoverProps> = ({
  map,
  mapLayerId,
  styleLayer,
  selectedFeatureID,
}) => {
  const [previousFeatureId, setPreviousFeatureId] = useState<string | number>()
  const [previousHover, setPreviousHover] = useState<SelectedFeature>()
  const [hoverFeature, setHoverFeature] = useState<SelectedFeature>()
  const [popupContext, setPopupContext] = useState<PopupContextProps>({})
  const { activeDataLayers } = useTabsLayers()

  // HOVER (map.on) user mouse action events on the map: HOVER
  useEffect(() => {
    if (!map || !mapLayerId) {
      return
    }
    // styling cursor on feature
    const handleMouseOver = () => {
      map.getCanvas().style.cursor = 'pointer'
    }

    // Styling cursor for  raster layers
    const handleAllMouseOver = () => {
      if (styleLayer?.geomType === 'raster' && styleLayer.featureRequest) {
        map.getCanvas().style.cursor = 'pointer'
      }
    }

    // Mouse over everytime the mouse moves: for Hover feature - pop-up feature
    const handleMouseMove = (event: MapLayerMouseEvent) => {
      if (event.features?.[0]) {
        setHoverFeature({
          firstFeature: event.features?.[0],
          event: {
            lngLat: event.lngLat,
            type: event.type,
          },
          relevantStyleLayer: styleLayer,
        })
      }
    }

    const handleMouseLeave = (event) => {
      map.getCanvas().style.cursor = 'unset'
      setHoverFeature(null)
      setPreviousFeatureId(null)
    }

    // mouseover fired when mouse enters the map or feature
    map.on('mouseover', mapLayerId, handleMouseOver)
    // General mouse on the map
    map.on('mouseover', handleAllMouseOver)
    // mousemove fired every time the mouse moves
    map.on('mousemove', mapLayerId, handleMouseMove)
    // When mouse leaves the layer
    map.on('mouseleave', mapLayerId, handleMouseLeave)
    map.on('mouseout', handleMouseLeave)

    return () => {
      if (styleLayer?.geomType !== 'raster' || !styleLayer.featureRequest) {
        map.getCanvas().style.cursor = 'unset'
      }
      map.off('mouseover', mapLayerId, handleMouseOver)
      map.off('mouseover', handleAllMouseOver)
      map.off('mousemove', mapLayerId, handleMouseMove)
      map.off('mouseleave', mapLayerId, handleMouseLeave)
      map.off('mouseout', handleMouseLeave)
    }
  }, [])

  // HOVER highlight functions
  useEffect(() => {
    if (!map) {
      return
    }

    const highlightLayer = activeDataLayers[0].styleLayers.find(
      (styleLayer) => styleLayer.isHighlightLayer
    )

    if (previousHover?.firstFeature.id !== hoverFeature?.firstFeature.id) {
      if (previousHover?.firstFeature.source) {
        map.setFeatureState(
          {
            source: previousHover.firstFeature.source,
            sourceLayer: previousHover.firstFeature.sourceLayer,
            id: previousHover.firstFeature.id,
          },
          {
            hover: false,
          }
        )

        if (highlightLayer) {
          map.setFeatureState(
            {
              source: highlightLayer.tileSource.sourceTitle,
              sourceLayer: highlightLayer['source-layer'],
              id: previousHover?.firstFeature.properties[
                highlightLayer.attrName
              ],
            },
            {
              hover: false,
            }
          )
        }
      }
      setPreviousHover(hoverFeature)
      return
    }

    if (hoverFeature) {
      map.setFeatureState(
        {
          source: hoverFeature.firstFeature.source,
          sourceLayer: hoverFeature.firstFeature.sourceLayer,
          id: hoverFeature.firstFeature.id,
        },
        {
          hover: 'hover',
        }
      )

      if (highlightLayer) {
        map.setFeatureState(
          {
            source: highlightLayer.tileSource.sourceTitle,
            sourceLayer: highlightLayer['source-layer'],
            id: previousHover?.firstFeature.properties[highlightLayer.attrName],
          },
          {
            hover: 'hover',
          }
        )
      }
    }
  }, [hoverFeature, previousHover, map])

  // HOVER Popup mousemove
  useEffect(() => {
    if (!map || !hoverFeature) {
      setPopupContext(null)
      return
    }

    if (hoverFeature.event && hoverFeature.event.type === 'mousemove') {
      const feature = hoverFeature.firstFeature
      const { lngLat } = hoverFeature.event

      if (hoverFeature.relevantStyleLayer?.popup) {
        setPreviousFeatureId(feature?.id)

        setPopupContext({
          feature,
          lngLat,
          styleLayer: hoverFeature.relevantStyleLayer,
        })
      }
    } else {
      setPopupContext(null)
      setPreviousFeatureId(null)
    }
  }, [hoverFeature, previousFeatureId])

  return (
    <div key="HoverPopup">
      {popupContext && selectedFeatureID !== hoverFeature?.firstFeature.id && (
        <MapPopup key="HoverPopup" map={map} context={popupContext} />
      )}
    </div>
  )
}

export default MapHover
