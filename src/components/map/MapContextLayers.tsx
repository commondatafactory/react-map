// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FC, useEffect } from 'react'
import {
  getSearchGeometryLayer,
  getBorderLayer,
  getAdminLabels,
  getSearchGeometryPolygonLayer,
  getAerialPhotos,
} from '../../data/context-style-layers'
import { useLabels } from '../../hooks/use-labels'
import { useMap } from '../../hooks/use-map'

interface MapContextLayersProps {
  map: maplibregl.Map
}

// This component takes care of the map and all related interactions.
const MapContextLayers: FC<MapContextLayersProps> = ({ map }) => {
  const { searchGeometry } = useMap()

  const { labelState, aerialPhotosState } = useLabels()

  const insertTop = 'labels_populated_places'

  // Context style layers
  const contextLayers = [
    {
      id: 'adminBorders',
      insert: insertTop,
      mapLayer: getBorderLayer('adminBorders'),
    },
    {
      id: 'adminLabels',
      insert: insertTop,
      mapLayer: getAdminLabels('adminLabels'),
    },
    {
      id: 'searchGeometryPolygonLine',
      insert: insertTop,
      mapLayer: getSearchGeometryPolygonLayer('searchGeometryPolygonLine'),
    },
    {
      id: 'searchGeometryPoint',
      insert: insertTop,
      mapLayer: getSearchGeometryLayer('searchGeometryPoint'),
    },
    {
      id: 'aerialPhotos',
      insert: 'osm_buildings',
      mapLayer: getAerialPhotos('aerialPhotos'),
    },
  ]

  // Add custom Context styleLayers
  useEffect(() => {
    if (map) {
      contextLayers.forEach((contextLayer) => {
        !map.getLayer(contextLayer.id) &&
          map.addLayer(contextLayer.mapLayer, contextLayer.insert)
      })
    }
  }, [map])

  // Toggle labels, such as streets.
  useEffect(() => {
    if (map) {
      // Toggle ALL style layers with type symbol and text-font layout property on and off
      if (labelState === 'geen') {
        map.getStyle().layers.forEach((mapLayer) => {
          if (mapLayer.type === 'symbol' && mapLayer.layout['text-font']) {
            map.setLayoutProperty(mapLayer.id, 'visibility', 'none')
          }
        })
      } else if (labelState === 'topo') {
        map.getStyle().layers.forEach((mapLayer) => {
          if (mapLayer.type === 'symbol' && mapLayer.layout['text-font']) {
            map.setLayoutProperty(mapLayer.id, 'visibility', 'visible')
            map.setLayoutProperty('adminLabels', 'visibility', 'none')
          }
        })
      } else if (labelState === 'admin') {
        map.getStyle().layers.forEach((mapLayer) => {
          if (mapLayer.type === 'symbol' && mapLayer.layout['text-font']) {
            map.setLayoutProperty(mapLayer.id, 'visibility', 'none')
          }
        })
        map.setLayoutProperty('adminLabels', 'visibility', 'visible')
      }
    }
  }, [labelState])

  // Toggle municipality and neighbourhood borders.
  useEffect(() => {
    if (map) {
      if (map.getLayer('adminBorders')) {
        labelState === 'admin'
          ? map.setLayoutProperty('adminBorders', 'visibility', 'visible')
          : map.setLayoutProperty('adminBorders', 'visibility', 'none')
      }
    }
  }, [labelState])

  // Set Luchtfotos
  useEffect(() => {
    if (map) {
      map.setLayoutProperty('aerialPhotos', 'visibility', 'visible')
      if (aerialPhotosState) {
        //TODO set opacity to top layer
        // map.getStyle().layers.forEach((mapLayer) => {
        //   if (mapLayer.type === 'fill') {
        //     map.setPaintProperty(mapLayer.id, 'fill-opacity', 0.6)
        //   }
        // })
      } else {
        map.setLayoutProperty('aerialPhotos', 'visibility', 'none')
        // TODO reset opacity to layer settings
        // map.getStyle().layers.forEach((mapLayer) => {
        //   if (mapLayer.type === 'fill') {
        //     map.setPaintProperty(mapLayer.id, 'fill-opacity', 1)
        //   }
        // })
      }
    }
  }, [aerialPhotosState])

  // Update searchGeometry
  useEffect(() => {
    if (
      map &&
      map.getLayer('searchGeometryPolygonLine') &&
      map.getLayer('searchGeometryPoint')
    ) {
      if (!searchGeometry) {
        map.setLayoutProperty('searchGeometryPolygonLine', 'visibility', 'none')
        map.setLayoutProperty('searchGeometryPoint', 'visibility', 'none')
        return
      }

      map.setLayoutProperty(
        'searchGeometryPolygonLine',
        'visibility',
        'visible'
      )
      map.setLayoutProperty('searchGeometryPoint', 'visibility', 'visible')
      ;(map.getSource('searchGeometry') as any).setData(
        searchGeometry.geometrie_ll
      )

      const zoomLevelInrichting = {
        provincie: {
          typesortering: 0.5,
          zoom: 10,
        },
        gemeente: {
          typesortering: 1,
          zoom: 11,
        },
        woonplaats: {
          typesortering: 2,
          zoom: 12,
        },
        weg: {
          typesortering: 3,
          zoom: 17,
        },
        postcode: {
          typesortering: 3.5,
          zoom: 16,
        },
        adres: {
          typesortering: 4,
          zoom: 18,
        },
        perceel: {
          typesortering: 5,
          zoom: 20,
        },
        hectometerpaal: {
          typesortering: 6,
          zoom: 21,
        },
        buurt: {
          typesortering: 7,
          zoom: 13,
        },
        wijk: {
          typesortering: 8,
          zoom: 12,
        },
      }
      // Typesortering: een door PDOK toegekende "standaard" volgorde voor
      // objecttypen. Bij de op de BAG gebaseerde gegevens gaat dit van groot
      // naar klein (gemeente, woonplaats, weg, postcode, adres).

      map.jumpTo({
        center: searchGeometry.centroide_ll.coordinates,
        zoom: zoomLevelInrichting[searchGeometry.type]?.zoom || 11.5,
        pitch: 0,
        bearing: 0,
      })
      map.triggerRepaint()
    }
  }, [map, searchGeometry])

  return <></>
}

export default MapContextLayers
