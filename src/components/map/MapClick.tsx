// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Dispatch, FC, SetStateAction, useEffect, useState } from 'react'
import { LngLatBounds, MapLayerMouseEvent } from 'maplibre-gl'

import { getFeatureInfoWMS } from '../../services/map/get-wms-feature-info'
import { DataLayerProps, StyleLayer } from '../../data/data-layers'
import { useTabsLayers } from '../../hooks/use-tabs-layers'
import { modalWidth } from '../modal/ModalBasics'
import { MapPopup } from './MapPopup'
import {
  SelectedFeaturesInfo,
  SelectedStyleLayerSource,
} from './MapRelevantLayers'

interface PopupContextProps {
  feature?: any
  lngLat?: maplibregl.LngLat
  styleLayer?: any
}

interface MapClickProps {
  map: maplibregl.Map
  mapLayerId: string
  styleLayer: DataLayerProps[][number]['styleLayers'][number]
  selectedFeaturesInfo: SelectedFeaturesInfo
  setSelectedFeaturesInfo: Dispatch<SetStateAction<SelectedFeaturesInfo>>
  currentEvent: { type: string; lngLat: maplibregl.LngLat }
  setCurrentEvent: Dispatch<SetStateAction<MapClickProps['currentEvent']>>
  isActive: boolean
  setSelectedStyleLayerSource: Dispatch<
    SetStateAction<SelectedStyleLayerSource>
  >
  setSelectedRelevantStyleLayer: Dispatch<
    SetStateAction<MapClickProps['styleLayer']>
  >
}

const MapClick: FC<MapClickProps> = ({
  map,
  mapLayerId,
  styleLayer,
  selectedFeaturesInfo,
  setSelectedFeaturesInfo,
  setSelectedStyleLayerSource,
  setSelectedRelevantStyleLayer,
  currentEvent,
  setCurrentEvent,
  isActive,
}) => {
  const { activeDataLayers } = useTabsLayers()
  const [selectedHighLightLayer, setSelectedHighLightLayer] =
    useState<Partial<StyleLayer>>()
  const [selectedHighlightAttribute, setSelectedHighlightAttribute] =
    useState(null)
  const [popupContext, setPopupContext] = useState<PopupContextProps>({})
  const [previousFeatureId, setPreviousFeatureId] = useState<string | number>()
  const [previousHighlightFeatureId, setPreviousHighlightFeatureId] = useState<
    string | number
  >()

  const resetSelectedFeatures = () => {
    setSelectedFeaturesInfo(null)
    setCurrentEvent(null)
    setPopupContext({})
  }

  // Map Libre click/touch events on the map
  useEffect(() => {
    if (!map || !mapLayerId) {
      return
    }
    // Highlight selection
    const highlightLayer = activeDataLayers[0].styleLayers.find(
      (styleLayer) => styleLayer.isHighlightLayer
    )
    if (highlightLayer) {
      setSelectedHighlightAttribute(highlightLayer?.attrName)
      setSelectedHighLightLayer(highlightLayer)
    }

    if (
      styleLayer &&
      selectedFeaturesInfo?.features[0]?.source ===
        styleLayer?.tileSource?.sourceTitle
    ) {
      setSelectedStyleLayerSource(styleLayer?.tileSource?.sourceTitle)
    }

    const handleClick = (event: MapLayerMouseEvent) => {
      event.originalEvent.stopPropagation()

      setSelectedStyleLayerSource(styleLayer.tileSource.sourceTitle)
      setCurrentEvent(event)
      setSelectedFeaturesInfo({
        features: event.features,
      })
      setSelectedRelevantStyleLayer(styleLayer)

      map.easeTo({
        center: [event.lngLat.lng, event.lngLat.lat],
        padding: { right: modalWidth },
      })
    }

    // NOTE: all raster clicks get siphoned through AllClicks
    const handleAllClicks = (event: MapLayerMouseEvent) => {
      if (event.originalEvent.cancelBubble === true) {
        return
      }

      // Click for raster
      if (styleLayer?.geomType === 'raster' && styleLayer.featureRequest) {
        const bounds = LngLatBounds.fromLngLat(event.lngLat, 1)

        getFeatureInfoWMS(bounds, styleLayer.featureRequest).then((info) => {
          if (info) {
            setSelectedStyleLayerSource(styleLayer.tileSource.sourceTitle)
            setCurrentEvent(event)
            setSelectedFeaturesInfo({
              features: [info],
            })
            setSelectedRelevantStyleLayer(styleLayer)
          } else {
            resetSelectedFeatures()
          }
        })
      }

      // remove all highlights on empty click
      if (styleLayer && styleLayer?.geomType !== 'raster') {
        setSelectedStyleLayerSource(null)
        resetSelectedFeatures()
      }
    }

    map.on('click', mapLayerId, handleClick)
    map.on('click', handleAllClicks)

    return () => {
      map.off('click', mapLayerId, handleClick)
      map.off('click', handleAllClicks)
    }
  }, [map, styleLayer, mapLayerId])

  // set Click layout aan!
  useEffect(() => {
    if (!map || !selectedFeaturesInfo || currentEvent?.type !== 'click') {
      return
    }

    if (selectedFeaturesInfo.features[0]) {
      if (previousFeatureId) {
        map.getSource(styleLayer.tileSource.sourceTitle) &&
          map.removeFeatureState(
            {
              source: styleLayer.tileSource.sourceTitle,
              sourceLayer:
                styleLayer['source-layer'] ||
                styleLayer.tileSource['source-layer'],
              id: previousFeatureId,
            },
            'click'
          )
      }

      map.setFeatureState(
        {
          source: styleLayer.tileSource.sourceTitle,
          sourceLayer:
            styleLayer['source-layer'] || styleLayer.tileSource['source-layer'],
          id: selectedFeaturesInfo.features[0].id,
        },
        {
          click: 'click',
        }
      )

      setPreviousFeatureId(selectedFeaturesInfo.features[0]?.id)

      if (selectedHighLightLayer) {
        if (previousHighlightFeatureId) {
          map.getSource(selectedHighLightLayer.tileSource.sourceTitle) &&
            map.removeFeatureState(
              {
                source: selectedHighLightLayer.tileSource.sourceTitle,
                sourceLayer: selectedHighLightLayer['source-layer'],
                id: previousHighlightFeatureId,
              },
              'click'
            )
        }

        map.setFeatureState(
          {
            source: selectedHighLightLayer.tileSource.sourceTitle,
            sourceLayer: selectedHighLightLayer['source-layer'],
            id: selectedFeaturesInfo.features[0].properties[
              selectedHighlightAttribute
            ],
          },
          {
            click: 'click',
          }
        )
        setPreviousHighlightFeatureId(
          selectedFeaturesInfo.features[0].properties[
            selectedHighlightAttribute
          ]
        )
      }

      if (styleLayer.popup && selectedFeaturesInfo) {
        setPopupContext({
          feature: selectedFeaturesInfo.features[0],
          lngLat: currentEvent.lngLat,
          styleLayer: styleLayer,
        })
      }
    } else {
      setPreviousFeatureId(null)
    }
  }, [selectedFeaturesInfo, currentEvent])

  // Set all Click layout uit
  useEffect(() => {
    if (!map.loaded() || isActive) {
      return
    }
    map.getSource(styleLayer?.tileSource?.sourceTitle) &&
      map.removeFeatureState({
        source: styleLayer.tileSource.sourceTitle,
        sourceLayer:
          styleLayer['source-layer'] || styleLayer.tileSource['source-layer'],
      })

    if (selectedHighLightLayer) {
      map.getSource(selectedHighLightLayer.tileSource.sourceTitle) &&
        map.removeFeatureState({
          source: selectedHighLightLayer.tileSource.sourceTitle,
          sourceLayer: selectedHighLightLayer['source-layer'],
        })
    }

    if (styleLayer?.popup) {
      setPopupContext({})
    }
  }, [isActive])

  return (
    <>
      {isActive && Object.keys(popupContext).length > 0 && (
        <MapPopup
          key="ClickPopup"
          map={map}
          context={popupContext}
          isSelected
        />
      )}
    </>
  )
}

export default MapClick
