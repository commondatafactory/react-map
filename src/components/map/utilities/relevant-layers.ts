// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Map } from 'maplibre-gl'
import { getBuildingLayer } from '../../../data/context-style-layers'
import { StyleLayer } from '../../../data/data-layers'
import {
  CustomLayerSpecification,
  makeInvertLayer,
  makeMapLayer,
} from '../../../data/make-map-layer'
import { CustomFilter } from '../../../providers/map-provider'

export const getDefaultInsertPoint = (
  map: Map,
  mapLayer: CustomLayerSpecification
) => {
  // Specific layers
  if (mapLayer.type === 'fill-extrusion') {
    const pointLayer = map
      .getStyle()
      .layers.find(
        (mapLayer) => mapLayer.type === 'circle' || mapLayer.type === 'symbol'
      )
    if (pointLayer) {
      return pointLayer.id
    }
    return ''
  }

  if (mapLayer.id === 'bag:panden:fill:1') {
    const rasterLayer = map
      .getStyle()
      .layers.find((mapLayer) => mapLayer.type === 'raster')
    if (rasterLayer) {
      return rasterLayer.id
    }
  }

  // Generic layers
  if (mapLayer.type === 'circle') {
    return 'labels_populated_places'
  } else if (mapLayer.type === 'raster') {
    // TODO: Something to do with liander or rioned layers? Find out if this
    // can be deleted, or needs fixing etc.
    if (map.getLayer('bag:panden:fill-extrusion:1')) {
      return 'bag:panden:fill-extrusion:1'
    }
  } else if (mapLayer.type === 'symbol') {
    return ''
  }

  // All other layers
  return 'road_labels'
}

export const generateMapLayer = (
  styleLayer: Partial<StyleLayer>,
  relevantLayer: Partial<StyleLayer>,
  threeDimensional: boolean,
  customFilter: CustomFilter
): CustomLayerSpecification => {
  if (styleLayer.id === 'layer0') {
    const buildingLayer = getBuildingLayer(
      'layer0',
      threeDimensional
    ) as CustomLayerSpecification
    return {
      ...buildingLayer,
      insert: styleLayer.insert || '',
    }
  }

  if (styleLayer.id === 'noDataLayer') {
    const invertLayer = makeInvertLayer({
      ...relevantLayer,
      id: 'noDataLayer',
    }) as CustomLayerSpecification
    return {
      ...invertLayer,
      insert: styleLayer.insert || '',
    }
  }

  if (styleLayer.geomType === 'symbol') {
    const layer = { ...relevantLayer }
    layer.geomType = 'symbol'

    const symbolLayer = makeMapLayer(
      { ...layer },
      threeDimensional,
      customFilter,
      styleLayer.attrName
    ) as CustomLayerSpecification
    return {
      ...symbolLayer,
    }
  }

  if (styleLayer.tileSource) {
    const l = makeMapLayer(
      styleLayer,
      threeDimensional,
      customFilter
    ) as CustomLayerSpecification
    return {
      insert: styleLayer.insert || '',
      ...l,
    }
  }
}
