// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Authentication = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-color: var(--colorPaletteGray400);
  padding-bottom: var(--spacing05);
  margin-bottom: var(--spacing06);
  padding-top: var(--spacing05);
  border-bottom-left-radius: 0.25rem;
  border-bottom-right-radius: 0.25rem;
  background-color: #ffd680;

  h2 {
    font-weight: var(--fontWeightBold);
    font-size: var(--fontSizeLarge);
    line-height: var(--lineHeightText);
    flex: 1 1 0%;
    height: 1.5rem;
    color: var(--colorPaletteGray900);
  }

  p {
    margin-top: var(--spacing03);
    font-size: var(--fontSizeSmall);
  }
`

export const AuthenticationContent = styled.div`
  margin-bottom: var(--spacing03);
  text-align: center;
`
