// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Spinner } from '@commonground/design-system'
import useUser from '../../hooks/use-user'
import * as Styled from './Authentication.styled'

const Authentication = () => {
  const { data: user, isLoading } = useUser()

  const userRole = user?.permissions?.find(
    (permission) =>
      permission.type === 'role' && permission.relation === 'access'
  )?.label

  const userOrganization = user?.permissions?.find(
    (permission) =>
      permission.type === 'organization' && permission.relation === 'access'
  )?.label

  return (
    !!user && (
      <Styled.Authentication>
        {isLoading ? (
          <Spinner />
        ) : (
          <>
            <Styled.AuthenticationContent>
              <h2>
                {userRole ? (
                  <>
                    Inzicht <span>{userRole}</span>
                  </>
                ) : (
                  <>Je hebt geen rol gelesecteerd</>
                )}
              </h2>
              <p>
                Je bent ingelogd namens: <strong>{userOrganization}</strong>
              </p>
            </Styled.AuthenticationContent>
          </>
        )}
      </Styled.Authentication>
    )
  )
}

export default Authentication
