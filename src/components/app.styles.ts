// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Container = styled.main`
  display: inline-block;
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  min-width: 150px;
  min-height: 300px;

  @media screen and (min-width: 900px) {
    display: grid;
    grid-template-columns: 25rem auto;
    grid-template-rows: 10vh 10vh repeat(3, 1fr) 2rem 3rem;
  }

  @media screen and (min-width: 1401px) {
    grid-template-columns: 30rem auto;
  }
`

// For mobile only
export const TopRightContainer = styled.div`
  display: flex;
  gap: var(--spacing05);
  position: absolute;
  pointer-events: none;
  top: var(--spacing05);
  right: var(--spacing05);

  > * {
    pointer-events: initial;
  }
`
export const FlexColumn = styled.div`
  display: flex;
  flex-direction: column;
  height: fit-content;
  gap: var(--spacing05);
`
