// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { FC, InputHTMLAttributes } from 'react'
import * as Styled from './RadioButton.styled'

export const RadioButton: FC<InputHTMLAttributes<HTMLInputElement>> = ({
  checked,
  children,
  ...rest
}): JSX.Element => {
  return (
    <Styled.Label>
      <Styled.RadioButton type="radio" checked={checked} readOnly {...rest} />
      <Styled.Text>{children}</Styled.Text>
    </Styled.Label>
  )
}
