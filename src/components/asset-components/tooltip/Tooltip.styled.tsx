// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const TooltipWrapper = styled.div`
  cursor: help;
  border-bottom: 1px black dotted;
`

export const Tooltip = styled.div`
  background-color: rgba(33, 33, 33, 0.9);
  border-radius: 6px;
  padding: 1rem;
  position: absolute;
  max-width: 250px;
  height: auto;
  z-index: 100;
  bottom: 0;

  &.top {
    bottom: 100%;
    margin-bottom: 0.2rem;
  }

  > p {
    text-align: left !important;
    font-size: 0.875rem !important;
    line-height: 1.5rem !important;
    color: var(--colorBackground) !important;
    white-space: normal !important;
  }

  &.left {
    left: 0%;
    margin-right: 0.2rem;
  }

  &.bottom {
    top: 100%;
    margin-top: 0.2rem;
  }
`
