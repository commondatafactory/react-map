// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FC, InputHTMLAttributes } from 'react'
import * as Styled from './Checkbox.styled'

// A reusable radio button in the styling of the CommonDataFactory
export const Checkbox: FC<InputHTMLAttributes<HTMLInputElement>> = ({
  name,
  value,
  checked,
  onChange,
  children,
  ...rest
}) => (
  <Styled.Label htmlFor={name}>
    <Styled.Checkbox
      type="checkbox"
      id={name}
      name={name}
      value={value}
      checked={checked}
      onChange={onChange}
      readOnly
      {...rest}
    />
    {children}
    <Styled.Text>{name}</Styled.Text>
  </Styled.Label>
)
