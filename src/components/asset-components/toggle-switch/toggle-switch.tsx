// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, InputHTMLAttributes } from 'react'
import { StyledLabel, StyledSlider } from './styled'

export const ToggleSwitch: FC<InputHTMLAttributes<HTMLInputElement>> = (
  props
): JSX.Element => {
  return (
    <StyledLabel>
      <input type="checkbox" {...props} onClick={(e) => e.stopPropagation()} />
      <StyledSlider />
    </StyledLabel>
  )
}
