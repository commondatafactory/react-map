// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const LegendContainer = styled.div`
  grid-column: 2 / span 1;
  grid-row: 6 / span 1;
  justify-self: end;
  align-self: end;
  z-index: 2;
  position: relative;
  background-color: var(--colorBackgroundAlt);
  border-radius: 4px;
  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.24);
  margin-bottom: calc(20px + 1rem);
  margin-right: calc(1rem + 36px + 1rem);
  max-height: 60vh;
  max-width: 530px;
  overflow-y: auto;
`

export const CloseButton = styled.button`
  position: sticky;
  right: var(--spacing03);
  top: var(--spacing03);
  height: 32px;
  width: 32px;
  float: right;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  color: var(--colorPaletteGray600);
`

export const LegendInfo = styled.div`
  width: 80%;
  padding: 0 var(--spacing05);
  margin: var(--spacing05) 0;

  display: flex;
  column-gap: var(--spacing04);
  flex-wrap: wrap;
  align-items: baseline;

  @media screen and (max-width: 1250px) {
    max-width: 430px;
    overflow-y: auto;
    padding: var(--spacing03);
    p {
      font-size: var(--fontSizeSmall);
    }
  }
`
export const ContentContainer = styled.div`
  padding-left: var(--spacing05);
  padding-right: var(--spacing05);

  & img {
    width: auto;
    height: auto;
  }

  & .legendItem {
    margin-right: 32px;

    display: flex;
    justify-content: flex-start;
    align-items: start;
    gap: var(--spacing03);
  }

  & .legendItem div {
    width: 24px;
    height: 16px;
  }

  & .legendItem input {
    width: 24px;
    height: 16px;
    cursor: pointer;
    appearance: none;
    opacity: 0.2 !important;
  }

  & .legendItem input:hover {
    opacity: 0.2 !important;
  }

  & .legendItem input:checked {
    opacity: 1 !important;
  }

  & .dot {
    height: 20px !important;
    width: 20px !important;
    border-radius: 50%;
    margin-top: 2px;
  }

  & .legendItem p {
    margin: 0;
  }

  & .continuous g.x.axis.horz g.tick text {
    font-size: var(--fontSizeSmall);
    font-family: 'Source Sans Pro', sans-serif;
    text-rendering: optimizeLegibility;
    fill: var(--colorPaletteGray600);
  }

  & svg.continuous g.x.axis.horz g.tick line {
    stroke-width: 2px;
    stroke: var(--colorBackground);
  }

  & text.average {
    font-size: var(--fontSizeSmall);
    font-family: 'Source Sans Pro', sans-serif;
    text-rendering: optimizeLegibility;
    fill: var(--colorPaletteGray600);
  }

  & .legendFootnote {
    width: 250px;
  }

  & .slider line {
    stroke: none;
  }

  @media screen and (max-width: 1250px) {
    max-width: 430px;
    overflow-y: auto;
    padding: var(--spacing03);

    & svg.continuous {
      width: 100%;
    }

    p {
      font-size: var(--fontSizeSmall);
    }

    & .legendItem div {
      width: 18px;
      height: 12px;
    }

    & .dot {
      height: 12px !important;
      width: 12px !important;
      border-radius: 50%;
    }
  }
`

export const LegendLine = styled.hr<{ $borderColor: string }>`
  border: 2px solid ${(props) => props.$borderColor};
  flex-shrink: 0;
  width: 20px;
`

export const LegendButton = styled.button`
  display: flex;
  cursor: pointer;
  fill: var(--colorPaletteGray600);
  margin: var(--spacing02);
  height: 32px;
  width: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
`
