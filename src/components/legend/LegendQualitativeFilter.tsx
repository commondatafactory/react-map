// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useEffect, useState } from 'react'
import { FilterSpecification } from 'maplibre-gl'
import { StyleLayer } from '../../data/data-layers'
import { useMap } from '../../hooks/use-map'
import { useDraw } from '../../hooks/use-draw'

const makeNewCustomFilter = (newActiveFilters, attrName) => {
  const mapFilter: FilterSpecification = Object.entries(
    newActiveFilters
  ).reduce(
    (acc, [key, value]) =>
      value ? [...acc, ['==', ['to-string', ['get', attrName]], key]] : acc,
    ['any']
  )

  let countFilter = ''
  for (const [key, value] of Object.entries(newActiveFilters)) {
    if (value) {
      countFilter = countFilter.concat('')
    } else {
      let n = attrName

      switch (attrName) {
        case 'energieklasse_score':
          n = 'energieklasse'
          break
        case 'elabel_voorlopig':
          n = 'labelscore_voorlopig'
          break
        case 'elabel_definitief':
          n = 'labelscore_definitief'
          break
      }

      const string = `&!contains-${n}=${key}`
      countFilter = countFilter.concat(string)
    }
  }

  return { mapFilter, countFilter }
}

interface LegendProps {
  styleLayer: Partial<StyleLayer>
  filterValues: Array<any>
  attrName: string
  filterNames: Array<string>
  id: string
}

const LegendQualitativeFilter: FunctionComponent<LegendProps> = ({
  styleLayer,
  filterValues,
  attrName,
  filterNames,
  id,
}) => {
  const { customFilters, setCustomFilters } = useMap()
  const { setCustomCountFilter } = useDraw()

  const [activeFilters, setActiveFilters] = useState({})
  const [toggleAll, setToggleAll] = useState(false)

  const updateCustomFilters = (newActiveFilters) => {
    const { mapFilter, countFilter } = makeNewCustomFilter(
      newActiveFilters,
      attrName
    )
    const updatedFilters = [
      ...(customFilters?.filter((filter) => id !== filter.id) || []),
      { id, filter: mapFilter, list: [] },
    ]
    setCustomFilters(updatedFilters)
    setCustomCountFilter(countFilter)
  }

  const handleToggleSingle = (value: string) => {
    const newActiveFilters = {
      ...activeFilters,
      [value]: !activeFilters[value],
    }

    setActiveFilters(newActiveFilters)
    updateCustomFilters(newActiveFilters)

    const areAllFiltersToggled = Object.values(newActiveFilters).every(
      (filterValue) => filterValue === toggleAll
    )
    if (areAllFiltersToggled) {
      setToggleAll(!toggleAll)
    }
  }

  const handleToggleAll = () => {
    const newActiveFilters = Object.keys(activeFilters).reduce(
      (acc, key) => ({ ...acc, [key]: toggleAll }),
      activeFilters
    )

    setActiveFilters(newActiveFilters)
    setToggleAll(!toggleAll)
    updateCustomFilters(newActiveFilters)
  }

  useEffect(() => {
    return () => {
      setCustomFilters(null)
      setCustomCountFilter(null)
    }
  }, [])

  useEffect(() => {
    const newActiveFilters = filterValues.reduce(
      (acc, element) => ({ ...acc, [element]: true }),
      {}
    )
    setActiveFilters(newActiveFilters)
  }, [filterValues])

  return (
    <>
      {filterValues.map((filterValue, i) => (
        <div key={filterValue} className="legendItem">
          <input
            onChange={() => handleToggleSingle(filterValue)}
            type="checkbox"
            style={{
              backgroundColor: (styleLayer as any).colorScale(filterValue),
              opacity: styleLayer.opacity || 1,
            }}
            checked={activeFilters[filterValue] || false}
          />
          <label>{filterNames[i]}</label>
        </div>
      ))}
      <div className="legendFootnote">
        <p style={{ fontSize: '0.9em', fontStyle: 'italic' }}>
          Klik op een legenda item om te filteren of{' '}
          <span
            style={{ textDecoration: 'underline', cursor: 'pointer' }}
            onClick={handleToggleAll}
          >
            {' '}
            zet alle waarden {toggleAll ? 'aan' : 'uit'}
          </span>
        </p>
      </div>
    </>
  )
}

export default LegendQualitativeFilter
