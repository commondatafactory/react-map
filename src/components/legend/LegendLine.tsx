// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useEffect, useState } from 'react'
import * as d3 from 'd3'

import { StyleLayer } from '../../data/data-layers'
import { useMap } from '../../hooks/use-map'
import * as Styled from './Legend.styled'

interface LegendProps {
  styleLayer: Partial<StyleLayer>
}

interface legendItems {
  color: string
  label: string
}

export const LegendLine: FunctionComponent<LegendProps> = ({ styleLayer }) => {
  const [legendItems, setLegendItems] = useState<legendItems[]>([])
  const { customFilters } = useMap()

  useEffect(() => {
    const legendItems = styleLayer.legendStops?.map((item, i) => ({
      label: item,
      color: d3.color(styleLayer.color).toString(),
    }))
    setLegendItems(legendItems)
  }, [styleLayer, customFilters])

  return (
    <>
      {legendItems?.map((item) => (
        <div key={item.label} className="legendItem">
          <Styled.LegendLine
            className="line"
            data-testid="color"
            $borderColor={item.color}
          />
          <p>{item.label}</p>
        </div>
      ))}
    </>
  )
}
