// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const FooterContainer = styled.div`
  display: none;
  background: var(--colorPaletteGray600);
  z-index: 10;
  box-shadow: 0 0 #0000, 0 0 #0000, 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);

  & a,
  .asA {
    align-items: center;
    cursor: pointer;
    display: flex;
    margin-left: 1rem;
    flex-wrap: nowrap;
  }
  & p {
    font-size: var(--fontSizeSmall);
    color: white;
  }
  & svg {
    color: white;
    margin-right: var(--spacing03);
  }

  @media screen and (min-width: 900px) {
    grid-row: 6 / span 2;
    grid-column: 1 / span 1;

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`

export const LinksContainer = styled.div`
  display: flex;
  margin-top: var(--spacing03);
`
