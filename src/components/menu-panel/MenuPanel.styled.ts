// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  height: 100%;
  bottom: 0;
  box-shadow: auto;
  padding: 0;
  overflow-y: scroll;
  position: fixed;
  width: 100vw;
  z-index: 10;
  pointer-events: auto;

  > * {
    pointer-events: auto;
  }

  button:last-of-type {
    margin-top: auto;
  }

  @media screen and (min-width: 900px) {
    display: block;
    position: relative;
    padding: var(--spacing05);
    grid-column: 1 / span 1;
    grid-row: 1 / span 5;
    justify-self: stretch;
    align-self: stretch;
    width: auto;
    height: auto;
    box-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1),
      0 4px 6px -4px rgb(0 0 0 / 0.1);
  }

  @media screen and (min-width: 1401px) {
    padding: var(--spacing07);
  }
`

export const CollapseButton = styled.button<any>`
  background: var(--colorPaletteGray100);
  height: var(--spacing08);
  top: ${(p) => (p.$isCollapsed ? 300 : 100)}px;
  width: 100vw;
  z-index: 10;

  > svg {
    margin: auto;
    transform: ${(p) => (p.$isCollapsed ? 'scaleY(-1)' : 'scaleY(1)')};
  }
`
