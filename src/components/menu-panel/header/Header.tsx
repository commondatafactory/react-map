// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useContext } from 'react'
import AppContext from '../../AppContext'
import * as Styled from './Header.styled'

const Header: React.FC = () => {
  const config = useContext(AppContext)

  return (
    <Styled.Container>
      <img src="/logo.svg" alt="Logo Datavoorziening op de kaart" />
      <h1>{config.title}</h1>
      <p>{config.subtitle}</p>
    </Styled.Container>
  )
}

export default Header
