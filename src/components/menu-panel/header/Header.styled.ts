// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Container = styled.header`
  width: 100vw;
  background: var(--colorBackground);
  box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.19);
  padding: var(--spacing03);

  display: grid;
  grid-template-columns: 80px 1fr;
  // grid-template-rows: repeat(2, 1fr);
  column-gap: var(--spacing05);

  > p {
    color: var(--colorPaletteGray600);
    font-size: var(--fontSizeSmall);
  }

  & img {
    align-self: center;
    // grid-row: span 2 / span 2;
  }
  & h1 {
    font-size: var(--fontSizeLarge);
    line-height: var(--lineHeightText);
  }
  & p {
    line-height: var(--lineHeightText);
  }

  @media screen and (min-width: 900px) {
    padding: 0;
    width: auto;
    box-shadow: none;
  }

  @media screen and (min-width: 1401px) {
    /* padding-bottom: var(--spacing07); */
  }
`
