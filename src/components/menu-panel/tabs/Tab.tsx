// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC } from 'react'
import { Collapse } from 'react-collapse'
import { IconFlippingChevron } from '@commonground/design-system'

import { useTabsLayers } from '../../../hooks/use-tabs-layers'
import { TabProps } from '../../../providers/tab-layer-provider'
import { setURLParams } from '../../../utils/url-params'
import * as Styled from './Tabs.styled'

type Props = {
  children: React.ReactNode
  hasActiveLayers: boolean
  iconSlug: string
  isActive: boolean
  slug: string
  tab: TabProps
  title: string
}

const Tab: FC<Props> = ({
  children,
  hasActiveLayers,
  iconSlug,
  isActive = false,
  slug,
  tab,
  title,
}) => {
  const { setActiveTab } = useTabsLayers()

  const handleTabClick = () => {
    const newActiveTab = !isActive && tab

    setActiveTab(newActiveTab)

    if (!hasActiveLayers) {
      if (newActiveTab) {
        setURLParams('tab', newActiveTab.id)
      } else {
        setURLParams({ tab: null, layer: null })
      }
    }
  }

  return (
    <Styled.TabContainer data-cy={`tab-${tab.title}`}>
      <button className="heading" onClick={handleTabClick}>
        <img
          src={iconSlug || `/images/categories/${slug}.svg`}
          width="24"
          height="24"
        />
        <h2>{title}</h2>
        <IconFlippingChevron
          flipHorizontal={isActive}
          animationDuration={300}
        />
      </button>

      <Collapse isOpened={isActive}>
        <div className="collapseContent">{children}</div>
      </Collapse>
    </Styled.TabContainer>
  )
}

export default Tab
