// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { ThemeProvider } from 'styled-components'
import { defaultTheme } from '@commonground/design-system'
import { render, screen } from '@testing-library/react'

import Tab from './Tab'

const changeActiveTab = jest.fn()
const children = 'Children'

const tab = {
  id: 'id1',
  slug: 'id1',
  title: 'Title 1',
}

const setup = (overrides?: any) => {
  const crypto = require('crypto')
  global.crypto = crypto
  return render(
    <ThemeProvider theme={defaultTheme}>
      <Tab
        tab={tab}
        changeActiveTab={changeActiveTab}
        slug={tab.slug}
        title={tab.title}
        {...overrides}
      >
        <h1>{children}</h1>
      </Tab>
    </ThemeProvider>
  )
}

describe('Tab', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should render tab and children', async () => {
    setup()
    await screen.findByRole('button', { name: tab.title })

    expect(
      screen.queryByRole('heading', { name: children })
    ).not.toBeInTheDocument()
    expect(
      screen.getByRole('heading', { hidden: true, name: children })
    ).toBeInTheDocument()
  })

  it('should render the tab expanded', async () => {
    setup({ isActive: true })

    await screen.findByRole('button', { name: tab.title })
    // expect(screen.getByRole('heading', { name: children })).toBeInTheDocument()
  })
})
