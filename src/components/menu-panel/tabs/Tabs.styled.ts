// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Container = styled.div`
  background: var(--colorBackground);
  padding: 1rem;
  overflow-y: scroll;
  overflow-x: hidden;
  display: block;
  bottom: 0;
  max-height: 60vh;
  width: 100vw;

  > div {
    box-sizing: content-box;
  }

  @media screen and (min-width: 900px) {
    width: auto;
    padding: 0;
    padding-top: var(--spacing07);
    padding-bottom: var(--spacing12);
    overflow: revert;
    max-height: revert;
  }
`

export const TabContainer = styled.div`
  border-color: var(--colorPaletteGray400);
  border-top-width: 1px;

  &:last-child {
    border-bottom-width: 1px;
  }
  > .heading {
    text-align: left;
    padding-top: var(--spacing05);
    padding-bottom: var(--spacing05);
    padding-left: var(--spacing03);
    padding-right: var(--spacing03);
    gap: var(--spacing03);
    justify-content: space-evenly;
    align-items: center;
    cursor: pointer;
    width: 100%;
    display: flex;

    > h2 {
      font-weight: var(--fontWeightBold);
      font-size: var(--fontSizeLarge);
      line-height: var(--spacing06);
      flex: 1 1 0%;
      color: var(--colorPaletteGray900);
    }
  }
  .headingSmall {
    color: var(--colorPaletteGray600);
    font-size: var(--fontSizeSmall);
    line-height: var(--spacing06);
    padding-top: var(--spacing05);
  }

  & .collapseContent {
    padding-left: var(--spacing08);
    padding-bottom: var(--spacing06);
    padding-right: var(--spacing03);
  }

  @media screen and (max-width: 1280px) {
    & .collapseContent {
      padding-left: var(--spacing06);
    }
  }

  .styled-description > a {
    color: var(--colorTextLink);
  }
`
