// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { FC, useEffect, useState } from 'react'
import { Icon } from '@commonground/design-system'

import Arrow from '../../../../../public/images/arrow-down-s-line.svg'
import { Tooltip } from '../../../asset-components/tooltip/Tooltip'
import Box from './../../../../../public/images/checkbox-blank-line.svg'
import BoxChecked from './../../../../../public/images/checkbox-blank-fill.svg'
import BoxIntermediate from './../../../../../public/images/checkbox-multiple-line.svg'
import Information from './../../../../../public/images/information-line.svg'
import { TreeNode } from './TreeView'

import * as Styled from './TreeView.styled'

interface TreeViewNodeProps {
  treeNode: Partial<TreeNode>
  onHandleDropdownClick: any
  onHandleSelectionClick: any
  searchText: string
  isVisible: boolean
}

export const TreeViewNode: FC<TreeViewNodeProps> = ({
  treeNode,
  onHandleDropdownClick,
  onHandleSelectionClick,
  searchText,
  isVisible,
}) => {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')

  const getDescription = async function (sbiCode): Promise<void> {
    const url = `https://ds.sbicodes.commondatafactory.nl/search/?match-code=${sbiCode}`

    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      })
      const data = await response.json()
      let desc = data.data[0].description

      desc = desc.replace(/@@\(/g, '<span style="color:#ffbc2c;">')
      desc = desc.replace(/\)@@/g, '</span>')
      if (searchText) {
        const reg = new RegExp(`${searchText}`, 'gmi')
        desc = desc.replace(
          reg,
          `<span style="color:#2c6fff; font-weight:bold;">${searchText}</span>`
        )
      }
      setDescription(`<p>${desc}<p>`)
    } catch (e) {}
  }

  useEffect(() => {
    setTitle(treeNode.title)
  }, [])

  useEffect(() => {
    if (!treeNode) return

    if (searchText.length >= 1) {
      const reg = new RegExp(searchText, 'gmi')
      setTitle(
        treeNode.title.replace(
          reg,
          `<span class="searchText">${searchText}</span>`
        )
      )
    } else {
      setTitle(treeNode.title)
    }
  }, [searchText])

  return (
    <>
      {isVisible && (
        <Styled.TreeNode
          onClick={(e) => onHandleDropdownClick(treeNode, e)}
          className={`${treeNode.isRoot ? 'main' : ''} ${
            treeNode.isActive === 'active' ? 'active' : ''
          } ${treeNode.hasChildren ? 'hasChildren' : ''}`}
        >
          <li
            onClick={(e) => onHandleSelectionClick(treeNode, e)}
            key={'Checkbox' + treeNode.code}
            style={{ margin: '0' }}
          >
            {treeNode.isActive === 'notActive' ? (
              <Icon as={Box} />
            ) : treeNode.isActive === 'active' ? (
              <Icon as={BoxChecked} />
            ) : (
              <Icon as={BoxIntermediate} />
            )}
          </li>

          <Styled.TreeNodeTitle key={'Title' + treeNode.code}>
            <span className="treeNodeCode">{treeNode.code}</span>
            <p dangerouslySetInnerHTML={{ __html: title }} />

            <Styled.MoreInformationWrapper
              onMouseEnter={() => getDescription(treeNode.code)}
            >
              {!treeNode.isRoot && (
                <Tooltip
                  position="left"
                  text={description}
                  style={{
                    marginLeft: -(16 + (treeNode.code.length - 1) * 16),
                  }}
                >
                  <Styled.IconWrapper>
                    <Icon as={Information} />
                  </Styled.IconWrapper>
                </Tooltip>
              )}
            </Styled.MoreInformationWrapper>
          </Styled.TreeNodeTitle>
          {treeNode.hasChildren && (
            <Icon
              className={`${treeNode.isExpanded ? 'collapsed' : ''}`}
              as={Arrow}
            />
          )}
        </Styled.TreeNode>
      )}
    </>
  )
}
