// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FC, useEffect, useState } from 'react'
import Input from '@commonground/design-system/dist/components/Form/TextInput/Input'
import ResetIcon from '../../../../../public/images/close.svg'
import * as Styled from '../../location-nav-bar/LocationNavBar.styled'
import { debounce } from '../../../../utils/debounce'
import { getSbiCodes } from '../../../../services/sbi/get-sbi-codes'

interface InputBarProps {
  startText: string
  setFilterList
  searchText: string
  setSearchText
}

export const InputBar: FC<InputBarProps> = ({
  startText,
  setFilterList,
  setSearchText,
  searchText,
}) => {
  const [isValid, setIsValid] = useState(true)

  const handleClear = () => {
    setSearchText('')
    setFilterList([])
    setIsValid(true)
  }

  const handleChange = async (e) => {
    setSearchText(e.target.value)
  }

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    const fetchData = debounce(async (text): Promise<void> => {
      if (text.length) {
        try {
          const data = await getSbiCodes(text, signal)

          if (!data) {
            return
          }

          setFilterList(data.data)
          setIsValid(true)
        } catch (e) {
          if (!signal.aborted) {
            setIsValid(false)
            setFilterList([])
          }
        }
      } else {
        setFilterList([])
      }
    }, 75)

    fetchData(searchText)

    return () => abortController.abort()
  }, [searchText])

  return (
    <Styled.NavBarContainer style={{ margin: '0', padding: '0' }}>
      <Input
        placeholder={startText}
        type="text"
        value={searchText}
        onChange={handleChange}
        iconAtEnd={searchText && ResetIcon}
        onSubmit={handleChange}
        autoComplete="off"
        autoFocus="off"
        spellCheck="false"
        style={{ width: '100%' }}
        maxLength={30}
        handleIconAtEnd={handleClear}
        className={isValid ? 'valid' : 'invalid'}
      />
    </Styled.NavBarContainer>
  )
}
