// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useEffect, useState } from 'react'
import { ExpressionFilterSpecification } from 'maplibre-gl'
import { Icon } from '@commonground/design-system'

import { useMap } from '../../../../hooks/use-map'
import { TreeViewNode } from './TreeViewNode'
import { sbiTree } from './sbi-data'
import { InputBar } from './InputBar'
import FilterOffIcon from './../../../../../public/images/filter-off-line.svg'
import FilterIcon from './../../../../../public/images/filter-fill.svg'
import CloseIcon from './../../../../../public/images/close.svg'
import MapFilterIcon from './../../../../../public/images/pin-distance-fill.svg'
import * as Styled from './TreeView.styled'

export interface TreeNode {
  id: string // uniek node id
  title: string // naam van de sbi code
  code: string // sbi code
  isRoot: boolean // hoofdklasses zijn root nodes.
  parentId?: string // unieke node id van de parent node
  hasChildren: boolean // of the node children heeft of niet
  isExpanded: boolean // node is uitgeklapt op niet. set by: cliking on the layer title. collapse arrow.
  isVisible: true | false // visible for filters or not
  isActive: 'active' | 'partialActive' | 'notActive' // code is active on the map, so map filter applies. set by: by clicking on the icon in front. alles aan/ alles uit button
}

interface Tree {
  [key: string]: TreeNode
}

export const TreeView = ({ id }) => {
  const { setCustomFilters } = useMap()

  const [currentTree, setCurrentTree] = useState<Tree>()
  const [allActive, setAllActive] = useState(false)
  const [filterList, setFilterList] = useState([])
  const [searchText, setSearchText] = useState('')
  const [amountOfActiveNodes, setAmountOfActiveNodes] = useState(0)

  const toggleAll = () => {
    if (!currentTree) return
    const updatedTree: Tree = Object.fromEntries(
      Object.entries(currentTree).map(([key, node]) => [
        key,
        {
          ...node,
          isActive: !allActive && node.isVisible ? 'active' : 'notActive',
        },
      ])
    )
    setCurrentTree(updatedTree)
  }

  const checkChildren = (
    parentKey: string,
    attribute: string
  ): TreeNode['isActive'] => {
    const children = Object.entries(currentTree).filter(([_, treeNode]) => {
      if (treeNode.parentId?.toString() === parentKey.toString()) {
        return treeNode
      }
    })
    const amountOfChildren = children.length

    const {
      amountOfPartialActiveChildren,
      amountOfActiveChildren,
      amountOfVisibleChildren,
    } = children.reduce(
      (prev, [, node]) => {
        if (node[attribute] === 'partialActive') {
          prev.amountOfPartialActiveChildren += 1
        } else if (node[attribute] === 'active') {
          prev.amountOfActiveChildren += 1
        }
        if (node.isVisible) {
          prev.amountOfVisibleChildren += 1
        }
        return prev
      },
      {
        amountOfPartialActiveChildren: 0,
        amountOfActiveChildren: 0,
        amountOfVisibleChildren: 0,
      }
    )

    if (
      amountOfChildren > amountOfVisibleChildren &&
      amountOfActiveChildren > 0
    ) {
      return 'partialActive'
    } else if (amountOfChildren === amountOfPartialActiveChildren) {
      return 'partialActive'
    } else if (amountOfChildren === amountOfActiveChildren) {
      return 'active'
    } else if (
      amountOfActiveChildren === 0 &&
      amountOfPartialActiveChildren === 0
    ) {
      return 'notActive'
    } else {
      return 'partialActive'
    }
  }

  const setParents = (parentKey: TreeNode['id'], attribute) => {
    const parentNode = Object.entries(currentTree).find(([_, treeNode]) => {
      return treeNode.id.toString() === parentKey.toString()
    })[1]
    parentNode[attribute] = checkChildren(parentKey, attribute)
    setCurrentTree((currentTree) => ({
      ...currentTree,
      [parentNode.id]: {
        ...parentNode,
      },
    }))
    parentNode.parentId && setParents(parentNode.parentId, attribute)
  }

  const setParentsVisible = (parentKey: TreeNode['id'], input: boolean) => {
    const parentNode = Object.entries(currentTree).find(([_, treeNode]) => {
      return treeNode.id.toString() === parentKey.toString()
    })[1]
    if (!parentNode.isVisible) {
      parentNode.isVisible = input
      setCurrentTree((currentTree) => ({
        ...currentTree,
        [parentNode.id]: {
          ...parentNode,
        },
      }))
      parentNode.parentId && setParentsVisible(parentNode.parentId, input)
    }
  }

  const setChildren = (parentTreeNode: TreeNode) => {
    Object.entries(currentTree).forEach(([_, treeNode]) => {
      if (treeNode.parentId?.toString() === parentTreeNode.id.toString()) {
        if (!treeNode.isVisible && parentTreeNode.isActive === 'notActive') {
          treeNode.isActive = 'notActive'
        } else if (treeNode.isVisible) {
          if (
            parentTreeNode.isActive !== 'notActive' &&
            !treeNode.hasChildren
          ) {
            treeNode.isActive = 'active'
          } else {
            treeNode.isActive = parentTreeNode.isActive
          }
        }

        setCurrentTree((currentTree) => ({
          ...currentTree,
          [treeNode.id]: {
            ...treeNode,
          },
        }))
        treeNode.hasChildren && setChildren(treeNode)
      }
    })
  }

  const onHandleSelectionClick = (currentTreeNode: TreeNode, e) => {
    e.preventDefault()
    e.stopPropagation()

    if (currentTreeNode.isActive === 'partialActive') {
      currentTreeNode.isActive = 'notActive'
    } else if (filterList.length > 0 && currentTreeNode.hasChildren) {
      if (currentTreeNode.isActive === 'active') {
        currentTreeNode.isActive = 'notActive'
      } else if (currentTreeNode.isActive === 'notActive') {
        currentTreeNode.isActive = 'active'
        currentTreeNode.hasChildren && setChildren(currentTreeNode) //sets only visible children
        // re-check node
        currentTreeNode.isActive = checkChildren(currentTreeNode.id, 'isActive')
      }
    } else {
      currentTreeNode.isActive =
        currentTreeNode.isActive !== 'notActive' ? 'notActive' : 'active'
    }

    currentTreeNode.hasChildren && setChildren(currentTreeNode)
    currentTreeNode.parentId && setParents(currentTreeNode.parentId, 'isActive')

    setCurrentTree((currentTree) => ({
      ...currentTree,
      [currentTreeNode.id]: {
        ...currentTreeNode,
      },
    }))
  }

  const onHandleDropdownClick = (currentChild: TreeNode, e) => {
    e.preventDefault()
    e.stopPropagation()
    currentChild.isExpanded = !currentChild.isExpanded
    setCurrentTree((currentTree) => ({
      ...currentTree,
      [currentChild.id]: {
        ...currentChild,
      },
    }))
  }

  const createItemsFromTree = (parentTreeKey: string) => {
    return Object.entries(currentTree).map(([key, treeNode]) => {
      if (treeNode.parentId?.toString() === parentTreeKey.toString()) {
        return (
          <React.Fragment key={key}>
            <TreeViewNode
              // test={}
              key={key}
              treeNode={treeNode}
              onHandleDropdownClick={onHandleDropdownClick}
              onHandleSelectionClick={onHandleSelectionClick}
              searchText={searchText}
              isVisible={treeNode.isVisible}
            />

            {treeNode.isExpanded && (
              <Styled.ChildrenTree id="treeLeave">
                {treeNode.hasChildren && createItemsFromTree(key)}
              </Styled.ChildrenTree>
            )}
          </React.Fragment>
        )
      }
    })
  }

  // Set up first tree root
  useEffect(() => {
    if (!sbiTree) {
      setCurrentTree(null)
      return
    }

    let initTree: Tree = {}
    sbiTree.forEach((element) => {
      const newNode: TreeNode = {
        id: element.Id.toString(),
        title: element.Title.toString(),
        code: element.Code.toString(),
        hasChildren: element.HasChildren,
        isExpanded: false,
        isRoot: element.IsRoot,
        isActive: 'notActive',
        parentId: element.ParentId?.toString() || null,
        isVisible: true,
      }

      initTree[newNode.id] = { ...newNode }
    })
    setCurrentTree(initTree)
  }, [])

  // Set map visibilities acties
  useEffect(() => {
    if (!currentTree) return

    const {
      amountOfActiveNodes,
      amountOfActiveVisibleNodes,
      amountOfVisibleNodes,
    } = Object.keys(currentTree).reduce(
      (prev, key) => {
        if (currentTree[key].isActive === 'active') {
          prev.amountOfActiveNodes += 1
        }
        if (
          currentTree[key].isActive === 'active' &&
          currentTree[key].isVisible
        ) {
          prev.amountOfActiveVisibleNodes += 1
        }
        if (currentTree[key].isVisible) {
          prev.amountOfVisibleNodes += 1
        }
        return prev
      },
      {
        amountOfActiveNodes: 0,
        amountOfActiveVisibleNodes: 0,
        amountOfVisibleNodes: 0,
      }
    )

    const activeNodes = Object.keys(currentTree).map((key) => {
      return (
        currentTree[key].isActive === 'active' && [
          currentTree[key].code,
          `l${currentTree[key].code.length}_code`,
        ]
      )
    })

    setAmountOfActiveNodes(amountOfActiveNodes)

    if (amountOfActiveNodes === Object.keys(currentTree).length) {
      // alles aan
      setAllActive(true)
      setCustomFilters([{ id, filter: ['all', true], list: [] }])
    } else if (amountOfActiveNodes === 0) {
      // alles uit
      setAllActive(false)
      setCustomFilters([{ id, filter: ['all', false], list: [] }])
    } else {
      setAllActive(true)
      // gekozen filters aan
      const filters = activeNodes
        .reduce((a, b) => a.concat(b[0]), [])
        .filter((el) => el !== undefined)
      // reduce list with cascading filters.
      const filteredFilters = filters.filter(
        (testElement, j) =>
          !filters.some(
            (compareElement, index) =>
              j !== index &&
              testElement.startsWith(compareElement) &&
              testElement !== compareElement
          )
      )
      const customMapFilters: ExpressionFilterSpecification = ['any']

      filteredFilters.forEach((el) => {
        return customMapFilters.push(['==', ['get', `l${el.length}_code`], el])
      })

      setCustomFilters([
        {
          id,
          filter: customMapFilters,
          list: filteredFilters,
        },
      ])
    }
  }, [currentTree])

  // Filter lijst op basis van tekst zoekveld
  useEffect(() => {
    if (!currentTree) {
      return
    }
    // 0. RESET all nodes to being visible TRUE if there is no filter list. (default all on )
    if (filterList.length <= 0) {
      const updatedTree: Tree = Object.fromEntries(
        Object.entries(currentTree).map(([key, node]) => [
          key,
          { ...node, isVisible: true },
        ])
      )
      setCurrentTree(updatedTree)
    }

    // 1. when there is a filterList:
    if (filterList.length > 0) {
      Object.entries(currentTree).reduce((prev, [key, treeNode]) => {
        const newNode = treeNode
        for (let i = 0; i < filterList.length; i++) {
          if (treeNode.code?.toString() === filterList[i].code.toString()) {
            newNode.isVisible = true
            newNode.parentId && setParentsVisible(newNode.parentId, true)
            return
          } else {
            newNode.isVisible = false
          }
        }
        return { ...prev, [key]: newNode }
      }, {} as any)
    }
  }, [filterList])

  return (
    <>
      <InputBar
        setFilterList={setFilterList}
        startText="Zoek activiteit"
        setSearchText={setSearchText}
        searchText={searchText}
      />
      <Styled.FilterOptions>
        {filterList.length > 0 && (
          <Styled.FilterOptionsTag>
            <Icon
              style={{ width: '1.5rem' }}
              as={filterList.length === 0 ? FilterOffIcon : FilterIcon}
            />
            <p>{filterList.length} resultaten tekst filter </p>
          </Styled.FilterOptionsTag>
        )}

        <Styled.FilterOptionsTag
          style={{
            backgroundColor: 'var(--colorInfoLight)',
            cursor: 'pointer',
          }}
          onClick={() => toggleAll()}
        >
          {amountOfActiveNodes > 0 ? (
            <>
              <Icon
                style={{ width: '1.5rem', marginLeft: 4 }}
                as={FilterOffIcon}
              />
              <p>{amountOfActiveNodes} sbi codes geselecteerd</p>
              <Icon as={CloseIcon} style={{ padding: 1, marginRight: 4 }} />
            </>
          ) : (
            <>
              <Icon
                style={{ width: '1.5rem', marginLeft: 4 }}
                as={MapFilterIcon}
              />
              <p
                style={{
                  paddingLeft: 10,
                  paddingRight: 10,
                  whiteSpace: 'nowrap',
                }}
              >
                Selecteer alle sbi codes
              </p>
            </>
          )}
        </Styled.FilterOptionsTag>
      </Styled.FilterOptions>

      <Styled.Tree>
        {currentTree &&
          Object.entries(currentTree)
            .sort(([, a], [, b]) => a.code.localeCompare(b.code))
            .map(([key, treeNode]) => {
              if (treeNode.isRoot && treeNode.isVisible) {
                return (
                  <React.Fragment key={key}>
                    <TreeViewNode
                      treeNode={treeNode}
                      onHandleDropdownClick={onHandleDropdownClick}
                      onHandleSelectionClick={onHandleSelectionClick}
                      searchText={searchText}
                      isVisible={treeNode.isVisible}
                    />

                    {treeNode.isExpanded && (
                      <Styled.ChildrenTree id="treeLeave">
                        {treeNode.hasChildren && createItemsFromTree(key)}
                      </Styled.ChildrenTree>
                    )}
                  </React.Fragment>
                )
              }
            })}
      </Styled.Tree>
    </>
  )
}
