// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FC } from 'react'
import { Icon, Spinner } from '@commonground/design-system'
import DownloadIcon from '../../../../public/images/download.svg'

import useUser from '../../../hooks/use-user'
import { DataLayerProps } from '../../../data/data-layers'
import { useAccess } from '../../../hooks/use-access'
import { createHandelsRegisterInfo } from '../../../services/count/create-bedrijventerreinen-export'
import { useMap } from '../../../hooks/use-map'
import { LayerAuth } from './LayerAuth'
import { LayerMetaData } from './LayerMetaData'

import * as Styled from './Layer.styled'
import { TreeView } from './FilterHierarchy/TreeView'
import { PolygonIcon } from './PolygonIcon'

interface MenuLayerProps {
  dataLayer: Partial<DataLayerProps>
}

export const LayerSbiCodes: FC<MenuLayerProps> = ({ dataLayer }) => {
  const { isLoggedIn, data, activeMunicipalityCode } = useUser()
  const { hasAccess } = useAccess()
  const { setAnalyzeMode, customFilters, searchGeometry } = useMap()

  const downloadSelection = () => {
    setAnalyzeMode(dataLayer.drawType || null)
  }
  const downloadAll = () => {
    if (
      searchGeometry &&
      searchGeometry.gemeentecode != activeMunicipalityCode
    ) {
      return alert(
        'Enkel de data van de eigen gemeente is te downloaden. Selecteer uw eigen gemeente en probeer het a.u.b. opnieuw.'
      )
    }
    const gemeenteCode = 'GM' + activeMunicipalityCode

    const filter = customFilters[0].list.map((el) => {
      return `any_match-l${el.length}_code=${el}`
    })
    filter.push(`match-gemeentecode=${gemeenteCode}`)
    const filterQuery = filter.join('&')

    if (filter.length || customFilters[0].filter[1]) {
      createHandelsRegisterInfo(null, 'xlsx', filterQuery)
    } else {
      console.warn('Geen resultaten gevonden')
    }
  }

  const isLayerAccessible = hasAccess(dataLayer, data)

  return (
    <Styled.Container>
      {isLayerAccessible ? (
        <React.Suspense fallback={<Spinner />}>
          <TreeView id={dataLayer.id} />

          {dataLayer?.drawType === 'count' && (
            <Styled.MetaDataItem>
              <Styled.MetaDataTitle>Download</Styled.MetaDataTitle>
              <Styled.DateWrapper>
                <Styled.CustomButton
                  variant="link"
                  size="small"
                  onClick={downloadAll}
                  style={{ marginRight: 20 }}
                  disabled={!customFilters?.[0].filter?.[1]}
                  // tabIndex={activeTab ? 0 : -1}
                >
                  <Icon as={DownloadIcon} inline />
                  SBI-code selectie
                </Styled.CustomButton>

                <Styled.CustomButton
                  variant="link"
                  size="small"
                  onClick={downloadSelection}
                  // tabIndex={activeTab ? 0 : -1}
                >
                  <PolygonIcon inline />
                  Gebied analyseren
                </Styled.CustomButton>
              </Styled.DateWrapper>
            </Styled.MetaDataItem>
          )}

          <LayerMetaData dataLayer={dataLayer} />
        </React.Suspense>
      ) : (
        <LayerAuth description={dataLayer.desc} isLoggedIn={isLoggedIn} />
      )}
    </Styled.Container>
  )
}
