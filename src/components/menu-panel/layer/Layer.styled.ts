// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import { Button } from '@commonground/design-system'

export const Layer = styled.div`
  background-color: var(--colorPaletteGray100);
  border-color: var(--colorPaletteGray400);
  border-style: solid;
  border-width: 1px 1px 0 1px;
  color: var(--colorPaletteGray800);
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  &:first-child {
    border-top-left-radius: 0.25rem;
    border-top-right-radius: 0.25rem;
  }
  &:last-child {
    border-bottom-width: 1px;
  }
  &:last-child {
    border-bottom-right-radius: 0.25rem;
    border-bottom-left-radius: 0.25rem;
  }
  &.active {
    background-color: var(--colorBackground);
  }
`

export const OptionLayer = styled.button`
  width: 100%;
  text-align: left;

  &:last-child {
    border-bottom-width: 1px;
  }

  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-left: 1rem;
  padding-right: 1rem;
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  color: var(--colorPaletteGray800);
  align-items: center;

  h3 {
    padding-left: 2rem;
    color: var(--colorPaletteGray800);
  }
  &.active {
    font-weight: var(--fontWeightBold);
  }
  div {
    display: flex;
    cursor: pointer;
    align-items: center;
    justify-content: flex-start;
  }
  input {
    cursor: pointer;
  }
`

export const LayerHeading = styled.button`
  text-align: left;
  cursor: pointer;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-left: 1rem;
  padding-right: 1rem;
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  color: var(--colorPaletteGray600);

  h3 {
    padding-right: 2rem;
    color: var(--colorPaletteGray800);
  }
  &.active {
    color: var(--colorBackground) !important;
    background-color: var(--colorInfo);
  }
  &.active > h3 {
    color: var(--colorBackground);
  }

  div {
    display: flex;
    cursor: pointer;
    align-items: center;
    justify-content: flex-start;
    margin-left: auto;
    gap: var(--spacing05);
  }
  input {
    cursor: pointer;
  }
`

export const LayerContent = styled.div`
  gap: 0.25rem;
  padding: 1rem;
  border-color: var(--colorInfo);
  border-width: 1px;
  width: 100%;
`

export const Container = styled.div`
  /* gap: 0.25rem; */
  /* padding: 1rem; */
  width: 100%;
`

export const LayerDetails = styled.div`
  p {
    margin-bottom: var(--spacing05);
  }

  .smallGrey {
    color: var(--colorPaletteGray600);
    font-size: var(--fontSizeSmall);
  }
`

export const CustomButton = styled(Button)`
  margin-bottom: var(--spacing05);
  margin-top: var(--spacing02);
`

export const MetaDataItem = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: var(--spacing05);

  svg {
    display: inline;
  }

  a {
    color: var(--colorTextLink);
  }
`

export const MetaDataTitle = styled.div`
  color: var(--colorPaletteGray600);
  font-size: var(--fontSizeSmall);
  line-height: var(--lineHeightText);
  margin-bottom: var(--spacing01);
`

export const ExternalLink = styled.a`
  display: flex;
  align-items: baseline;
  gap: var(--spacing03);
  > svg {
    fill: var(--colorTextLink);
  }
`
export const SourceTitle = styled.div`
  margin-top: var(--spacing03);
`
export const SourceDate = styled.div`
  display: flex;
  gap: 1rem;
`

export const DateWrapper = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  gap: var(--spacing03);
`

export const AuthenticationDetails = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  margin-bottom: var(--spacing05);

  & div {
    display: flex;
    flex-direction: row;
    margin-bottom: var(--spacing04);
    margin-top: var(--spacing04);
  }

  span {
    margin-left: var(--spacing03);
  }
`

export const SublayerDetails = styled.div`
  display: flex;
  gap: 0.25rem;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  width: 100%;
  margin-bottom: var(--spacing05);

  & div {
    gap: var(--spacing03);
    align-items: flex-start;
    cursor: pointer;
    width: 100%;
    display: flex;
    margin: 0.25rem;
  }
`
