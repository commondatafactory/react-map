// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FunctionComponent } from 'react'
import { Button, Icon } from '@commonground/design-system'

import ExternalLinkIcon from '../../../../public/images/external-link.svg'
import { useIsMobile } from '../../../hooks/use-is-mobile'
import { generateAuthUrl } from '../../../utils/generate-auth-url'
import { LockIcon } from './LockIcon'
import * as Styled from './Layer.styled'

interface LayerAuthProps {
  description?: string
  isLoggedIn?: boolean
}

export const LayerAuth: FunctionComponent<LayerAuthProps> = ({
  description,
  isLoggedIn,
}) => {
  const { isMobile } = useIsMobile()

  return (
    <Styled.LayerDetails onClick={(e) => e.stopPropagation()}>
      <p>{description}</p>
      <Styled.AuthenticationDetails>
        <div>
          <LockIcon />{' '}
          <span>
            {isLoggedIn && 'Geen permissie tot deze laag.'}
            {!isLoggedIn &&
              !isMobile &&
              'Log in om deze kaartlaag te bekijken.'}
            {!isLoggedIn &&
              isMobile &&
              'Inloggen is alleen beschikbaar op de desktopversie.'}
          </span>
        </div>
        {!isMobile && (
          <Button variant="secondary" as="a" href={generateAuthUrl(isLoggedIn)}>
            {isLoggedIn ? 'Permissies wijzigen' : 'Inloggen/Registreren'}
            <Icon as={ExternalLinkIcon} inline />
          </Button>
        )}
      </Styled.AuthenticationDetails>
    </Styled.LayerDetails>
  )
}
