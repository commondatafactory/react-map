// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Icon } from '@commonground/design-system'
import LockedLayer from '../../../../public/images/lock.svg'

export const LockIcon = (): JSX.Element => {
  return (
    <span data-testid="lockIcon" title="Alleen toegankelijk via inlog">
      <Icon as={LockedLayer} />
    </span>
  )
}
