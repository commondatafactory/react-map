// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

export const getSbiCodes = async (
  text: string,
  signal: AbortSignal
): Promise<any | null> => {
  try {
    const url = `https://ds.sbicodes.commondatafactory.nl/v0/search?search=${text}&pagesize=1000`

    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
      },
      signal,
    })

    if (!response.ok) {
      throw 'Could not get sbi-codes'
    }

    return await response.json()
  } catch (e) {
    console.warn(e)
    return null
  }
}
