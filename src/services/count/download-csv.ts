// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const downloadCSV = (csv: string) => {
  // const rows = CSVToArray(csv)
  // const result = rows.slice(1).map((row) => {
  //   return Object.fromEntries(rows[0].map((field, i) => [field, row[i]]))
  // })
  // return console.log(result)

  const url = window.URL.createObjectURL(new Blob([csv]))
  const a = document.createElement('a')
  a.href = url

  const datum = new Date().toLocaleDateString('nl-NL', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  })

  a.download = `Selectie download ${datum}.csv`

  document.body.appendChild(a)
  a.click()
  a.remove()
}

// Debug function for commented code above
function CSVToArray(strData: string, strDelimiter = ',') {
  var objPattern = new RegExp(
    '(\\' +
      strDelimiter +
      '|\\r?\\n|\\r|^)' +
      '(?:"([^"]*(?:""[^"]*)*)"|' +
      '([^"\\' +
      strDelimiter +
      '\\r\\n]*))',
    'gi'
  )

  var arrData = [[]]
  var arrMatches = null
  while ((arrMatches = objPattern.exec(strData))) {
    var strMatchedDelimiter = arrMatches[1]
    if (strMatchedDelimiter.length && strMatchedDelimiter != strDelimiter) {
      arrData.push([])
    }

    if (arrMatches[2]) {
      var strMatchedValue = arrMatches[2].replace(new RegExp('""', 'g'), '"')
    } else {
      var strMatchedValue = arrMatches[3]
    }

    arrData[arrData.length - 1].push(strMatchedValue)
  }

  return arrData
}
