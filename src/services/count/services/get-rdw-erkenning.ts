// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

export interface RDWErkenningAPI {
  volgnummer: number
  erkenning: string
}

export const getRdwErkenning = async (volgnummerId: number) => {
  try {
    const response = await fetch(
      `https://opendata.rdw.nl/resource/nmwb-dqkz.json?volgnummer=${volgnummerId}`
    )

    if (!response.ok) {
      throw new Error(response?.statusText)
    }

    const erkenningen: RDWErkenningAPI[] = await response.json()

    const mappedErkenningen: string[] = erkenningen.map((erkenning) => {
      return erkenning.erkenning
    })
    return { mappedErkenningen }
  } catch (e) {
    console.warn(`Could not fetch finding ${volgnummerId}. Message:`, e)
    return null
  }
}
