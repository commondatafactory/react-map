// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Building } from '../../../hooks/use-building'

export const getVerblijfsobjecten = async (
  geojson: {
    geometry?: any
  },
  filter?: string
): Promise<Building[]> => {
  const formData = new URLSearchParams()

  if (geojson) {
    const sgj = JSON.stringify(geojson.geometry)
    formData.append('geojson', sgj)
  }

  try {
    // FIX: Don't send `globalFilter`, since the API is not capable of
    // filtering.
    const response = await fetch(
      `https://ds.vboenergie.commondatafactory.nl/list/?${filter || ''}`,
      {
        method: 'POST',
        body: formData,
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      }
    )
    const data = await response.json()
    return data
  } catch (e) {
    console.error('Download error with Verblijfsobjecten api')
    return []
  }
}
