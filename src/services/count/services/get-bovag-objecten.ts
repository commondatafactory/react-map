// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export interface BOVAGObjectAPI {
  address: string
  bag_toevoeging: string
  bovag_adres: string
  bovag_plaats: string
  bovag_postcode: string
  bovag_toevoeging: string
  emailaddress: string
  geometry: string
  huisletter: string
  huisnummer: string
  huisnummertoevoeging: string
  lid: string
  lidid: string
  match_score: string
  name: string
  numid: string
  oppervlakte: string
  pid: string
  postcode: string
  sid: string
  telephone: string
  url: string
  vid: string
}

export const getBovagObjecten = async (
  geojson: {
    geometry?: any
  },
  limit = false
): Promise<{
  totaal: number
  data: BOVAGObjectAPI[]
}> => {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  try {
    const parameters = limit ? `reduce=count` : ''

    const bovagUrl =
      process.env.NEXT_PUBLIC_ENV === 'prod'
        ? `https://ds.bovag.commondatafactory.nl/list/?${parameters}`
        : process.env.NEXT_PUBLIC_ENV === 'acc'
        ? `https://acc.ds.bovag.commondatafactory.nl/list/?${parameters}`
        : `https://ds.bovag.commondatafactory.nl/list/?${parameters}`

    const response = await fetch(bovagUrl, {
      method: 'POST',
      body: formData,
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
      },
    })
    const totaal = Number(response.headers.get('total-items'))

    const data = await response.json()

    return { totaal, data: data || [] }
  } catch (e) {
    console.error('Download error with Bovag objecten')
    return { totaal: 0, data: [] }
  }
}
