// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Feature } from 'maplibre-gl'
import { wktPointToGeoJson } from '../map/wkt-to-geojson'
import { DownloadFileType } from '../../components/modal/count/Count'
import { getBovagObjecten } from './services/get-bovag-objecten'
import { getKVKObjecten } from './services/get-kvk-objecten'
import { getRDWobjecten } from './services/get-rdw-objecten'
import { getVerblijfsobjecten } from './services/get-verblijfsobjecten'
import { downloadCSV } from './download-csv'
import { fillArray, filterOnNumId, mapKeysToArray } from './utils'
import { formatKvkDate } from './services/get-total'
import { downloadXLSX } from './dowload-xlsx'

const baseHeaders = [
  'numid',
  'lid',
  'sid',
  'postcode',
  'straat',
  'huisnummer',
  'huisletter',
  'huisnummertoevoeging',
  'oppervlakte',
  'gebruiksdoelen',
  'pand_bouwjaar',
  'latitude',
  'longitude',
  'indicatie_RDW_erkenning',
  'indicatie_BOVAG_lidmaatschap',
  'indicatie_KVK_autobranche',
] as const

const rdwHeaders = [
  'RDW_volgnummer',
  'RDW_naam_bedrijf',
  'RDW_gevelnaam',
  'RDW_erkenningen',
  'RDW_matchscore',
  'RDW_toevoeging',
] as const

const bovHeaders = ['BOVAG_naam', 'BOVAG_adres', 'BOVAG_matchscore'] as const

const kvkHeaders = [
  'KVK_Nummer',
  'KVK_Handelsnaam',
  'KVK_SBI_code',
  'KVK_Bedrijfsactiviteit',
  'KVK_Inschrijving',
  'KVK_Vestigingsnummer',
  'KVK_Datum_inschrijving_vestiging',
  'KVK_Oprichting',
  'KVK_Url',
] as const

export const createAutoBrancheInfo = async (
  geojson: Feature,
  fileType: DownloadFileType
): Promise<void> => {
  Promise.all([
    getVerblijfsobjecten(geojson),
    getRDWobjecten(geojson),
    getBovagObjecten(geojson),
    getKVKObjecten(geojson, false, 'any_match-l2_code=45&any_match-l2_code=77'),
  ]).then((data) => {
    const bagData = data[0]
    const rdwData = data[1].data
    const bovData = data[2].data
    const kvkData = data[3].data

    const extendedVerblijfsobjecten = bagData.map((verblijfsobject) => {
      const filteredRdw = filterOnNumId(rdwData, verblijfsobject)
      const filteredBov = filterOnNumId(bovData, verblijfsobject)
      const filteredKvk = filterOnNumId(kvkData, verblijfsobject)

      const { coordinates } = wktPointToGeoJson(verblijfsobject.point)
      const updatedVerblijfsobject = {
        ...verblijfsobject,
        latitude: coordinates[1].toFixed(6),
        longitude: coordinates[0].toFixed(6),
      }

      const preparedRdw = filteredRdw.map((data) => ({
        RDW_volgnummer: data.volgnummer,
        RDW_naam_bedrijf: data.naam_bedrijf,
        RDW_gevelnaam: data.gevelnaam,
        RDW_erkenningen: data.rdw_erkenningen,
        RDW_matchscore: data.match_score,
        RDW_toevoeging: data.volgnummer,
      }))
      const preparedBov = filteredBov.map((data) => ({
        BOVAG_naam: data.name,
        BOVAG_adres: data.bovag_adres,
        BOVAG_matchscore: data.match_score,
      }))
      const preparedKvk = filteredKvk.map((data) => ({
        KVK_Nummer: data.dossiernr,
        KVK_Handelsnaam: data.hn_1x45,
        KVK_SBI_code: data.sbicode,
        KVK_Bedrijfsactiviteit: data.activiteit,
        KVK_Inschrijving: formatKvkDate(data.dat_inschr),
        KVK_Vestigingsnummer: data.vgnummer,
        KVK_Datum_inschrijving_vestiging: formatKvkDate(data.dat_vest),
        KVK_Oprichting: formatKvkDate(data.dat_oprich),
        KVK_Url: data.url,
      }))

      const indicatorFields = {
        indicatie_RDW_erkenning: preparedRdw.length,
        indicatie_BOVAG_lidmaatschap: preparedBov.length,
        indicatie_KVK_autobranche: preparedKvk.length,
      }

      return {
        base: { ...updatedVerblijfsobject, ...indicatorFields },
        rdw: preparedRdw,
        bov: preparedBov,
        kvk: preparedKvk,
      }
    })

    const csvHeader = [
      ...baseHeaders.map((header) => `"${header}"`),
      ...rdwHeaders.map((header) => `"${header}"`),
      ...bovHeaders.map((header) => `"${header}"`),
      ...kvkHeaders.map((header) => `"${header}"`),
    ].join(',')

    const csvContent = extendedVerblijfsobjecten
      .map((data) => {
        const baseRow = mapKeysToArray(baseHeaders, data.base)

        const rdwRows = data.rdw
          .sort((a, b) => a.RDW_naam_bedrijf.localeCompare(b.RDW_naam_bedrijf))
          .map((obj) => mapKeysToArray(rdwHeaders, obj))

        const bovRows = data.bov
          .sort((a, b) => a.BOVAG_naam.localeCompare(b.BOVAG_naam))
          .map((obj) => mapKeysToArray(bovHeaders, obj))

        const kvkRows = data.kvk
          .sort((a, b) => a.KVK_Handelsnaam.localeCompare(b.KVK_Handelsnaam))
          .map((obj) => mapKeysToArray(kvkHeaders, obj))

        const rows = new Array(
          Math.max(1, rdwRows.length, bovRows.length, kvkRows.length)
        )
          .fill(null)
          .map((_, i) =>
            [
              fillArray(rdwRows[i], rdwHeaders.length),
              fillArray(bovRows[i], bovHeaders.length),
              fillArray(kvkRows[i], kvkHeaders.length),
            ].join(',')
          )
          .join(
            `\n"${data.base.numid}",${'"",'.repeat(baseHeaders.length - 1)}`
          )

        return [baseRow, rows].join(',')
      })
      .join('\n')

    const csv = `${csvHeader}\n${csvContent}`
    if (fileType === 'csv') {
      downloadCSV(csv)
    } else {
      downloadXLSX(csv, 'auto')
    }
  })
}
