// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

export const wktPointToGeoJson = (
  wktPoint: string
): { type: string; coordinates: number[] } => {
  if (!wktPoint.includes('POINT')) {
    return {
      type: 'Point',
      coordinates: [0, 0],
    }
  }
  const coordinateTuple = wktPoint.split('(')[1].split(')')[0]
  const x = parseFloat(coordinateTuple.split(' ')[0])
  const y = parseFloat(coordinateTuple.split(' ')[1])
  return {
    type: 'Point',
    coordinates: [x, y],
  }
}
