// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const generateAuthUrl = (isLoggedIn: boolean) =>
  `${process.env.NEXT_PUBLIC_AUTH_URL}/${isLoggedIn ? 'permissies' : 'login'}`
