// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

type ParametersObject = { [key: string]: string }

// INFO: Either `setURLParams('key', 'value')` or `setURLParams({key: 'value'})`
export const setURLParams = (
  key: string | ParametersObject,
  value?: string
): void => {
  let newParams = typeof key === 'string' ? { [key]: value } : key

  const searchParams = new URLSearchParams(window.location.search)
  for (const [key, value] of Object.entries(newParams)) {
    if (value == null) {
      searchParams.delete(key)
    } else {
      searchParams.set(key, value)
    }
  }

  // TODO: Application doesn't listen to `popstate`, so using the back button
  // doesn't refresh the application state. Add a system that listens to this
  // event, and updates the application's state accordingly.
  const path = window.location.pathname
  const hash = window.location.hash
  const newurl = `${path}?${searchParams.toString()}${hash}`
  window.history.pushState(null, '', newurl)
}
