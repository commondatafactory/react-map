// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const parseDate = (date: string): string => {
  try {
    const today = new Date(Date.parse(date))

    let dd: number | string = today.getDate()
    let mm: number | string = today.getMonth() + 1
    const yyyy = today.getFullYear()

    if (dd < 10) {
      dd = `0${dd}`
    }

    if (mm < 10) {
      mm = `0${mm}`
    }

    return `${dd}-${mm}-${yyyy}`
  } catch {
    return date || 'Onbekend'
  }
}
