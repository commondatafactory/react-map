// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import {
  CircleLayerSpecification,
  ExpressionFilterSpecification,
  ExpressionInputType,
  FillExtrusionLayerSpecification,
  FillLayerSpecification,
  LineLayerSpecification,
  RasterLayerSpecification,
  SymbolLayerSpecification,
} from 'maplibre-gl'
import * as d3 from 'd3'
import { StyleLayer } from '../../data/data-layers'

export const makeFillLayer = (
  styleLayer: Partial<StyleLayer>,
  invert?: boolean
): Partial<FillLayerSpecification> => {
  return {
    type: 'fill',
    layout: {
      visibility: 'visible',
    },
    paint: {
      'fill-color': [
        'case',
        ['==', ['feature-state', 'hover'], 'hover'],
        getPaintExpression(1.5, styleLayer),
        ['==', ['feature-state', 'click'], 'click'],
        getPaintExpression(1.5, styleLayer),
        invert ? '#d2d2d2' : getPaintExpression(0, styleLayer),
      ],
      'fill-outline-color': [
        'case',
        ['==', ['feature-state', 'hover'], 'hover'],
        '#000000',
        ['==', ['feature-state', 'click'], 'click'],
        '#000000',
        invert ? '#d2d2d2' : getPaintExpression(1, styleLayer),
      ],
      'fill-opacity': styleLayer.opacity || 1,
    },
  }
}

export const makeFillExtrusionLayer = (
  styleLayer: Partial<StyleLayer>
): Partial<FillExtrusionLayerSpecification> => {
  return {
    type: 'fill-extrusion',
    layout: {
      visibility: 'visible',
    },
    paint: {
      'fill-extrusion-color': [
        'case',
        ['==', ['feature-state', 'hover'], 'hover'],
        getPaintExpression(1.5, styleLayer),
        ['==', ['feature-state', 'click'], 'click'],
        getPaintExpression(1.5, styleLayer),
        getPaintExpression(0, styleLayer),
      ],
      'fill-extrusion-height': [
        'interpolate',
        ['linear'],
        ['zoom'],
        13,
        0,
        16,
        ['*', 1.5, ['ceil', ['to-number', ['get', styleLayer.extrusionAttr]]]],
      ],
      'fill-extrusion-opacity': styleLayer.opacity || 1,
    },
  }
}

export const makeCircleLayer = (
  styleLayer: Partial<StyleLayer>,
  offset: number[]
): Partial<CircleLayerSpecification> => {
  return {
    type: 'circle',
    layout: {
      visibility: 'visible',
    },
    paint: {
      'circle-translate': offset as any,
      'circle-color': [
        'case',
        ['==', ['feature-state', 'hover'], 'hover'],
        getPaintExpression(2, styleLayer),
        ['==', ['feature-state', 'click'], 'click'],
        getPaintExpression(4, styleLayer),
        getPaintExpression(0, styleLayer),
      ],
      'circle-radius': [
        'interpolate',
        ['exponential', 1.2],
        ['zoom'],
        8,
        0,
        11,
        0.1,
        12,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          3,
          ['==', ['feature-state', 'click'], 'click'],
          3,
          1.5,
        ],
        16,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          8,
          ['==', ['feature-state', 'click'], 'click'],
          8,
          4,
        ],
        18,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          14,
          ['==', ['feature-state', 'click'], 'click'],
          14,
          7,
        ],
      ],
      'circle-opacity': [
        'interpolate',
        ['exponential', 1.2],
        ['zoom'],
        8,
        1,
        12,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          1,
          ['==', ['feature-state', 'click'], 'click'],
          1,
          styleLayer.opacity / 1.5 || 0.9,
        ],
        16,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          1,
          ['==', ['feature-state', 'click'], 'click'],
          1,
          styleLayer.opacity || 0.9,
        ],
      ],
      'circle-stroke-width': [
        'interpolate',
        ['exponential', 1.2],
        ['zoom'],
        8,
        0,
        11,
        0.5,
        12,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          3,
          ['==', ['feature-state', 'click'], 'click'],
          5,
          1,
        ],
        16,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          5,
          ['==', ['feature-state', 'click'], 'click'],
          10,
          2,
        ],
        18,
        [
          'case',
          ['==', ['feature-state', 'hover'], 'hover'],
          7,
          ['==', ['feature-state', 'click'], 'click'],
          12,
          3,
        ],
      ],
      'circle-stroke-color': [
        'case',
        ['==', ['feature-state', 'hover'], 'hover'],
        '#000',
        ['==', ['feature-state', 'click'], 'click'],
        '#000',
        getPaintExpression(1, styleLayer),
      ],
    },
  }
}

export const makeLineLayer = (
  styleLayer: Partial<StyleLayer>
): Partial<LineLayerSpecification> => {
  return {
    type: 'line',
    layout: {
      visibility: 'visible',
      'line-join': 'round',
      'line-cap': 'round',
    },
    paint: {
      'line-color': getPaintExpression(1, styleLayer) as string,
      'line-width': [
        'interpolate',
        ['linear'],
        ['zoom'],
        9,
        1,
        13.5,
        4,
        20,
        10,
      ],
      'line-opacity': styleLayer.opacity || 0.7,
    },
  }
}

export const makeRasterLayer = (
  styleLayer: Partial<StyleLayer>
): Partial<RasterLayerSpecification> => {
  return {
    type: 'raster',
    layout: {
      visibility: 'visible',
    },
    paint: {
      'raster-contrast': 0.4,
      'raster-brightness-min': 0,
      'raster-saturation': 0,
      'raster-opacity': styleLayer.opacity || 0.9,
    },
  }
}

export const makeSymbolLayer = (
  name: string
): Partial<SymbolLayerSpecification> => {
  return {
    type: 'symbol',
    layout: {
      'symbol-placement': 'point',
      'text-pitch-alignment': 'map',
      'text-field': `${name}`,
      'text-font': ['SourceSansPro-Bold'],
      'text-size': 12,
      'text-max-width': 5,
      'text-variable-anchor': [
        'top-right',
        'top-left',
        'bottom-right',
        'bottom-left',
        'right',
        'left',
        'top',
        'bottom',
      ],
      'text-line-height': 1.1,
      'text-radial-offset': 0.1,
      'text-justify': 'center',
      'text-padding': 5,
      'text-allow-overlap': false,
      'text-rotation-alignment': 'viewport',
    },
    paint: {
      'text-opacity': 1,
      'text-color': '#212121',
      'text-halo-color': '#F8F9FC',
      'text-halo-width': 1.5,
      'text-translate-anchor': 'viewport',
    },
  }
}

// Get colors from d3 color scale
const getInterpolateColors = (darker: number, colorScale): string[] => {
  const breakpoints = colorScale.domain()
  const extraStops = colorScale.ticks(20)
  const colorStops = breakpoints.concat(extraStops)
  const uniqueItems = Array.from(new Set(colorStops))
  uniqueItems.sort((a: number, b: number) => a - b)

  let colorArray: any[] = []
  for (const elem of uniqueItems) {
    let c = colorScale(elem)
    c = darker >= 0 ? d3.rgb(c).darker(darker).toString() : c
    colorArray = colorArray.concat(elem, c)
  }
  return colorArray
}

// Get colors from d3 color scale
const getColors = (darker: number, colorScale): string[] => {
  const breakpoints = colorScale.domain()
  let colorArray: any[] = []
  for (const elem of breakpoints) {
    const color = colorScale(elem)
    colorArray = colorArray.concat(
      elem,
      darker >= 0 ? d3.rgb(color).darker(darker).toString() : color
    )
  }
  return colorArray
}

function getPaintExpression(
  darker: number,
  styleLayer: Partial<StyleLayer>
): ExpressionFilterSpecification | ExpressionInputType {
  let firstPart, middlePart

  switch (styleLayer.expressionType) {
    case 'interpolate':
      firstPart = ['interpolate', ['linear'], ['get', styleLayer.attrName]]
      styleLayer.attrName === 'pt' ||
      styleLayer.tileSource.sourceTitle === 'cbsarmoede'
        ? (firstPart = [
            'interpolate',
            ['linear'],
            ['to-number', ['get', styleLayer.attrName]],
          ])
        : (firstPart = [
            'interpolate',
            ['linear'],
            ['get', styleLayer.attrName],
          ])

      middlePart = getInterpolateColors(darker, styleLayer.colorScale)

      return firstPart.concat(middlePart)
    case 'step':
      firstPart = ['step', ['get', styleLayer.attrName]]
      middlePart = getColors(darker, styleLayer.colorScale)
      middlePart.shift()

      return firstPart.concat(middlePart)
    case 'match':
      firstPart = ['match', ['get', styleLayer.attrName]]
      middlePart = getColors(darker, styleLayer.colorScale)
      middlePart.push('#e0e0e0')

      return firstPart.concat(middlePart)
    case 'case':
      if (styleLayer.caseArray) {
        firstPart = ['case']
        styleLayer.caseArray.forEach((element) => {
          const col = styleLayer.colorScale(element as any)
          return firstPart.push(
            ['!=', ['get', element], null],
            d3.color(col.toString()).darker(darker).toString()
          )
        })
        middlePart = 'grey'
      } else {
        firstPart = ['case', ['has', styleLayer.attrName]]
        middlePart = styleLayer.colorScale.range()
        if (darker >= 0)
          middlePart = middlePart.map((color) =>
            d3.rgb(color).darker(darker).toString()
          )
      }

      return firstPart.concat(middlePart)
    default:
      // default = 'fill'
      return d3
        .color(styleLayer.color || '#9ecf1b')
        .darker(darker)
        .toString()
  }
}
