// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

type DebounceFunction<T extends (...args: any[]) => any> = (
  fn: T,
  delay: number
) => T

export const debounce: DebounceFunction<(...args: any[]) => void> = (
  fn,
  delay
) => {
  let timeoutId: NodeJS.Timeout

  return function debounced(this: any, ...args: any[]) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const context = this

    clearTimeout(timeoutId)

    timeoutId = setTimeout(() => {
      fn.apply(context, args)
    }, delay)
  } as any
}
