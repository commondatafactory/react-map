// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import useSWR from 'swr'

import authorizationModels from '../../authorization-model.json'

export interface Name {
  last: string
  first: string
}

export interface Traits {
  name: Name
  email: string
}

export interface VerifiableAddress {
  id: string
  value: string
  verified: boolean
  via: string
  status: string
  verified_at: Date
}

export interface RecoveryAddress {
  id: string
  value: string
  via: string
}

export interface Identity {
  id: string
  schema_id: string
  schema_url: string
  traits: Traits
  verifiable_addresses: VerifiableAddress[]
  recovery_addresses: RecoveryAddress[]
}

export interface UserSession {
  id: string
  active: boolean
  expires_at: Date
  authenticated_at: Date
  issued_at: Date
  identity: Identity
  userType: 'administrator' | 'analyst'
  permissions: Partial<RelationTuple>[] | any
}

interface RelationTuple {
  namespace: string
  object: string
  relation: string
  subject_id: string
  subject_set: {
    namespace: string
    object: string
    relation: string
  }
}

type UseUserResponse = {
  data: UserSession
  isLoading: boolean
  error: any
  isLoggedIn: boolean
  // This is only the numerical part of the code, so minus the 'gm'
  activeMunicipalityCode: string
}

const fetcher = async (url: string) => {
  const response = await fetch(url, { credentials: 'include' })
  if (!response.ok) {
    throw response.statusText
  }
  return await response.json()
}

const userEndpoint = `${process.env.NEXT_PUBLIC_AUTH_URL}/api/users/whoami`

const useUser = (): UseUserResponse => {
  // The correct values for these properties are defined in an auth config file
  const namespaceDef = authorizationModels.organizations.namespace
  const relationDef = authorizationModels.organizations.relations.access

  const { data, error, isLoading } = useSWR<UserSession>(
    userEndpoint || null,
    fetcher,
    {
      shouldRetryOnError: false,
      revalidateIfStale: false,
    }
  )
  let activeMunicipalityCode = null

  if (data) {
    data?.permissions.some((permission: RelationTuple) => {
      if (
        permission.namespace !== namespaceDef ||
        permission.relation !== relationDef
      ) {
        return false
      }

      const regex = /(?<=gm)[a-zA-Z0-9]{4}/
      const match = regex.exec(permission.object)

      if (!match) {
        return false
      }

      activeMunicipalityCode = match[0]
      return true
    })
  }

  return {
    data,
    isLoading,
    error,
    isLoggedIn: data?.active,
    activeMunicipalityCode,
  }
}

export default useUser
