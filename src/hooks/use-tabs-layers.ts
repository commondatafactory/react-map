// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { useContext } from 'react'
import {
  TabLayerContext,
  TabLayerContextProps,
} from '../providers/tab-layer-provider'

export const useTabsLayers = (): TabLayerContextProps => {
  return useContext(TabLayerContext)
}
