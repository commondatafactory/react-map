// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import useSWR from 'swr'

const fetcher = async (url: string) => {
  const response = await fetch(url)
  return await response.json()
}

export type Building = {
  buurtcode: string
  buurtnaam: string
  energieklasse: string
  gas_ean_count: number
  gebruiksdoelen: string
  gemeentecode: string
  gemeentenaam: string
  gemiddelde_gemeente_woz: number
  huisletter: string
  huisnummer: number
  huisnummertoevoeging: string
  kwh_leveringsrichting_2022: number
  lid: number
  numid: number
  oppervlakte: number
  pand_bouwjaar: number
  pand_gebruiksoppervlakte: number
  pc6_eigendomssituatie_aantal_woningen_corporaties: number
  pc6_eigendomssituatie_perc_huur: number
  pc6_eigendomssituatie_perc_koop: number
  pc6_gas_aansluitingen_2022: number
  pc6_gasm3_2022: number
  pc6_gemiddelde_woz_waarde_woning: number
  pc6_grondbeslag_m2: number
  pc6_group_id_2022: string
  pc6_kwh_2022: number
  pid: string
  point: string
  postcode: string
  provinciecode: string
  provincienaam: string
  sbicode: string
  sid: number
  straat: string
  vid: number
  wijkcode: string
  wijknaam: string
  woning_type: string
  woningequivalent: number
}

type UseBuildingResponse = {
  building: Building[]
  isLoading: boolean
  error: boolean
}

const useBuilding = (buildingId: string): UseBuildingResponse => {
  buildingId = buildingId.toString().padStart(16, '0')
  // TODO: This seems to query on `pid`, while e.g. `filterOnNumId()` uses
  // `numid` as identifier. Why the difference?
  const { data, error } = useSWR(
    `https://ds.vboenergie.commondatafactory.nl/list/?match-pid=${buildingId}`,
    fetcher
  )

  return {
    building: data,
    isLoading: !error && !data,
    error: error,
  }
}

export default useBuilding
