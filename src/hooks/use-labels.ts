// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { useContext } from 'react'
import { LabelContext, LabelContextProps } from '../providers/label-provider'

export const useLabels = (): LabelContextProps => {
  return useContext(LabelContext)
}
