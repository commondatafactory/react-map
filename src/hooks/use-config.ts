// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { useContext } from 'react'
import AppContext, { ConfigProps } from '../components/AppContext'

export const useConfig = (): ConfigProps => {
  return useContext(AppContext)
}
