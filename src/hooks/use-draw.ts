// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { useContext } from 'react'
import { DrawContext, DrawContextProps } from '../providers/draw-provider'

export const useDraw = (): DrawContextProps => {
  return useContext(DrawContext)
}
