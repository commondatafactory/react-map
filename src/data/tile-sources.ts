// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { GeoJSONSource, RasterTileSource, VectorTileSource } from 'maplibre-gl'

export interface SourceSettingsProps {
  sourceTitle: string
  minzoom?: number // The minimum zoom level for the layer. At zoom levels less than the minzoom, the layer will be hidden.
  maxzoom?: number //The maximum zoom level for the layer. At zoom levels equal to or greater than the maxzoom, the layer will be hidden.
  'source-layer'?: string
  legendImage?: string
  source:
    | Partial<VectorTileSource>
    | Partial<RasterTileSource>
    | Partial<GeoJSONSource>
    | 'openmaptiles'
    | 'cbs'
}

// maxzoom in SourceSettingsProps.source:  Maximum zoom level for which tiles are available, as in the TileJSON spec. Data from tiles at the maxzoom are used when displaying the map at higher zoom levels.
// minzoom in SourceSettingsProps.source:  Minimum zoom level for which tiles are available, as in the TileJSON spec.

export interface Sources {
  [layerName: string]: SourceSettingsProps
}

// INFO: The array of sources gets reduced to an object indexed by the
// sourceTitle property at the end of the array.
export const sourceSettings: Sources = [
  {
    sourceTitle: 'bag',
    minzoom: 12,
    maxzoom: 22,
    'source-layer': 'panden2024',
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/kvenergie202406v3/kvenergie202406v3.json',
      // promoteId: 'group_id', // TODO kan niet! andere oplossing voor vinden ..
      minzoom: 8,
      maxzoom: 15,
    },
  },

  {
    sourceTitle: 'postcode_gas',
    minzoom: 7,
    maxzoom: 22,
    'source-layer': 'kv_pc6gas_2023',
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/kvenergie202406v2/kvenergie202406v2.json',
      promoteId: 'g_report_id',
      minzoom: 8,
      maxzoom: 15,
    },
  },

  {
    sourceTitle: 'postcode_elek',
    minzoom: 7,
    maxzoom: 22,
    'source-layer': 'kv_pc6elk_2023',
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/kvenergie202406v2/kvenergie202406v2.json',
      promoteId: 'e_report_id',
      minzoom: 8,
      maxzoom: 15,
    },
  },

  {
    sourceTitle: 'verblijfsobjecten',
    minzoom: 12,
    maxzoom: 22,
    'source-layer': 'bagpuntjes',
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/bagpuntjes/puntjes.json',
      minzoom: 12,
      maxzoom: 15,
    },
  },

  // CBS Bronnen
  {
    sourceTitle: 'cbs2022pc6',
    minzoom: 8,
    maxzoom: 22,
    'source-layer': 'buurtcijfers2022',
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/cbs_p6_2022_v1/cbs_p6_2022_v1.json',
      minzoom: 6,
      maxzoom: 15,
      promoteId: 'postcode', // TODO bagid, postcode gebieden hoveren en selecteren op basis van properties.postcode ???
    },
  },
  {
    sourceTitle: 'cbs2022pc64spuk',
    minzoom: 8,
    maxzoom: 22,
    'source-layer': 'buurtcijfers2022',
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/cbs_p6_2022_v1/cbs_p6_2022_v1.json',
      minzoom: 14,
      maxzoom: 15,
    },
  },
  {
    sourceTitle: 'cbsarmoede',
    minzoom: 8,
    maxzoom: 22,
    'source-layer': 'monitorarmoede2021',
    source: {
      type: 'vector',
      tiles: [
        'https://files.commondatafactory.nl/tiles/cbs_energiearmoede_2021/{z}/{x}/{y}.pbf',
      ],
      minzoom: 8,
      maxzoom: 14,
    },
  },
  {
    sourceTitle: 'cbs',
    minzoom: 8,
    maxzoom: 22,
    'source-layer': 'buurtcijfers2019',
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/cbs_20220113/cbs.json',
      minzoom: 8,
    },
  },
  {
    sourceTitle: 'eigenaren',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'vector',
      url:
        process.env.NEXT_PUBLIC_ENV === 'prod'
          ? 'https://auth.files.data.vng.nl/tiles/eigenaarstop7/eigenaarstop7.json'
          : 'https://acc.auth.files.commondatafactory.nl/tiles/eigenaarstop7/acc.eigenaarstop7.json',
    },
  },
  {
    sourceTitle: 'cbsenergiebereidheid',
    minzoom: 8,
    maxzoom: 22,
    'source-layer': 'bereidheid.energiemaatregelen_wijk',
    source: {
      type: 'vector',
      tiles: [
        'https://tileserver.commondatafactory.nl/bereidheid.energiemaatregelen_wijk/{z}/{x}/{y}.pbf',
      ],
      minzoom: 8,
    },
  },

  // cbsgrid: {
  //   type: 'vector',
  //   url: 'https://files.commondatafactory.nl/tiles/cbs_grid_20210512/cbs_grid.json',
  //   minzoom: 10,
  // },

  {
    sourceTitle: 'draagvlakdata',
    minzoom: 6,
    maxzoom: 22,
    'source-layer': 'bereidheidenergietransitie2021',
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/bereidheidenergietransitie2021/bereidheidenergietransitie2021.json',
    },
  },

  // Bedrijventerreinnen
  {
    sourceTitle: 'openmaptiles',
    source: 'openmaptiles',
    'source-layer': 'landuse',
    minzoom: 6,
    maxzoom: 22,
  },
  {
    sourceTitle: 'bedrijven',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/bedrijventerrein_20210330/bedrijventerrein.json',
    },
  },

  // KVK
  {
    sourceTitle: 'kvk2',
    minzoom: 6,
    maxzoom: 22,
    'source-layer': 'default',
    source: {
      type: 'vector',
      tiles: [
        process.env.NEXT_PUBLIC_ENV === 'prod'
          ? 'https://authorized.data.vng.nl/kvk.municipality_select/{z}/{x}/{y}.pbf'
          : process.env.NEXT_PUBLIC_ENV === 'acc'
          ? 'https://acc.authorized.data.commondatafactory.nl/kvk.municipality_select/{z}/{x}/{y}.pbf'
          : 'http://authorized.data.vng.com/kvk.municipality_select/{z}/{x}/{y}.pbf',
      ],
      // 'NOT UNIQUE FOR SUPERMARKTS FOR EXAMPLE. USE ID FIELD?
      promoteId: 'dossiernr',
    },
  },
  // KVK lokaal
  // kvk2: {
  //   minzoom: 6,
  //   maxzoom: 22,
  //   'source-layer': 'kvk.kvk_expand_sbi_anoniem',
  //   source: {
  //     type: 'vector',

  //     tiles: [
  //       'http://localhost:7800/kvk.kvk_expand_sbi_anoniem/{z}/{x}/{y}.pbf',
  //     ],
  //     promoteId: 'dossiernr',
  //   },
  // },
  {
    sourceTitle: 'rdw',
    minzoom: 7,
    maxzoom: 22,
    'source-layer': 'bovag.rdw_bag_matched',
    source: {
      type: 'vector',
      tiles: [
        process.env.NEXT_PUBLIC_ENV === 'prod'
          ? 'https://tileserver.commondatafactory.nl/bovag.rdw_bag_matched/{z}/{x}/{y}.pbf'
          : process.env.NEXT_PUBLIC_ENV === 'acc'
          ? 'https://acc.tileserver.commondatafactory.nl/bovag.rdw_bag_matched/{z}/{x}/{y}.pbf'
          : 'https://tileserver.commondatafactory.nl/bovag.rdw_bag_matched/{z}/{x}/{y}.pbf',
      ],
      minzoom: 7,
      maxzoom: 22,
    },
  },
  {
    sourceTitle: 'bovag',
    minzoom: 7,
    maxzoom: 22,
    'source-layer': 'bovag.garages',
    source: {
      type: 'vector',
      tiles: [
        process.env.NEXT_PUBLIC_ENV === 'prod'
          ? 'https://tileserver.commondatafactory.nl/bovag.garages/{z}/{x}/{y}.pbf'
          : process.env.NEXT_PUBLIC_ENV === 'acc'
          ? 'https://acc.tileserver.commondatafactory.nl/bovag.garages/{z}/{x}/{y}.pbf'
          : 'https://tileserver.commondatafactory.nl/bovag.garages/{z}/{x}/{y}.pbf',
      ],
      minzoom: 7,
      maxzoom: 22,
    },
  },

  {
    sourceTitle: 'faillissementen',
    minzoom: 6,
    maxzoom: 22,
    'source-layer': 'cir.insolvency_locations_resolved_epoch',
    source: {
      type: 'vector',
      tiles: [
        'https://tileserver.commondatafactory.nl/cir.insolvency_locations_resolved_epoch/{z}/{x}/{y}.pbf',
      ],
      promoteId: 'insolvency_id',
    },
  },

  // Overig
  {
    sourceTitle: 'eigendom',
    minzoom: 6,
    maxzoom: 22,
    'source-layer': 'eigenaren',
    source: {
      type: 'vector',
      url: 'https://files.commondatafactory.nl/tiles/apeldoorn_20211006/apeldoorn.json',
    },
  },

  // dook ai bronnen
  {
    sourceTitle: 'dookaiimages',
    minzoom: 8,
    maxzoom: 22,
    'source-layer': 'public.base_targetimage',
    source: {
      type: 'vector',
      tiles: [
        'https://api.dynalytix.ai/tiles/public.base_targetimage/{z}/{x}/{y}.pbf',
      ],
      minzoom: 8,
    },
  },
  {
    sourceTitle: 'dookailicenceplate',
    minzoom: 8,
    maxzoom: 22,
    'source-layer': 'public.base_licenseplate',
    source: {
      type: 'vector',
      tiles: [
        'https://api.dynalytix.ai/tiles/public.base_licenseplate/{z}/{x}/{y}.pbf',
      ],
      minzoom: 8,
    },
  },
  {
    sourceTitle: 'dookaitext',
    minzoom: 8,
    maxzoom: 22,
    'source-layer': 'public.base_company',
    source: {
      type: 'vector',
      tiles: [
        'https://api.dynalytix.ai/tiles/public.base_company/{z}/{x}/{y}.pbf',
      ],
      minzoom: 8,
    },
  },
  // Klimaat koppelkansen
  {
    sourceTitle: 'wmsHitte',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'raster',
      tiles: [
        'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=hitteeiland&STYLE=hitteeiland_r_hitte&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}',
      ],
      tileSize: 256,
    },
  },

  {
    sourceTitle: 'wmsTemp',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'raster',
      tiles: [
        'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png&TRANSPARENT=true&LAYERS=hittekaart_gevoelstemp_huidig&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}',
      ],
      tileSize: 256,
    },
  },

  {
    sourceTitle: 'wmsWater',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'raster',
      tiles: [
        'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=waterdiepte_neerslag_140mm_2uur&STYLE=waterdiepte_neerslag_1-1000_r_wateroverlast&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}',
      ],
      tileSize: 256,
    },
  },
  {
    sourceTitle: 'wmsWater2',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'raster',
      tiles: [
        'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=waterdiepte_neerslag_70mm_2uur&STYLE=waterdiepte_neerslag_1-100_r_wateroverlast&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}',
      ],
      tileSize: 256,
    },
  },

  {
    sourceTitle: 'wmsGeluid',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'raster',
      tiles: [
        'https://data.rivm.nl/geo/alo/wms?VERSION=1.3.0SERVICE=WMS&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=&LAYERS=rivm_20210201_g_geluidkaart_lden_alle_bronnen_v3&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
      ],
      tileSize: 256,
    },
  },
  {
    sourceTitle: 'wmsLucht',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'raster',
      tiles: [
        'https://data.rivm.nl/geo/alo/wms?VERSION=1.3.0SERVICE=WMS&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=&LAYERS=rivm_nsl_20210101_gm_PM102019_Int_v2&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
      ],
      tileSize: 256,
    },
  },
  {
    sourceTitle: 'wmsLucht2',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'raster',
      tiles: [
        'https://data.rivm.nl/geo/alo/wms?VERSION=1.3.0SERVICE=WMS&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=&LAYERS=rivm_nsl_20210101_gm_PM252019_Int_v2&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
      ],
      tileSize: 256,
    },
  },
  {
    sourceTitle: 'wmsLucht3',
    minzoom: 6,
    maxzoom: 22,
    source: {
      type: 'raster',
      tiles: [
        'https://data.rivm.nl/geo/alo/wms?VERSION=1.3.0SERVICE=WMS&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=&LAYERS=rivm_nsl_20210101_gm_NO22019_Int_v2&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
      ],
    },
  },
  {
    sourceTitle: 'wmsPercelen',
    minzoom: 6,
    maxzoom: 22,
    legendImage:
      'https://service.pdok.nl/rvo/brpgewaspercelen/wms/v1_0/legend/BrpGewas/Default.png',
    source: {
      type: 'raster',
      tiles: [
        'https://service.pdok.nl/rvo/brpgewaspercelen/wms/v1_0?VERSION=1.3.0&SERVICE=WMS&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=Default&LAYERS=BrpGewas&CRS=EPSG:3857&WIDTH=512&HEIGHT=512&bbox={bbox-epsg-3857}',
      ],
    },
  },
  // Ondergrond
  {
    sourceTitle: 'lianderNetten',
    minzoom: 12.5,
    maxzoom: 22,
    source: {
      type: 'raster',
      tiles: [
        'https://service.pdok.nl/liander/elektriciteitsnetten/wms/v1_0?SERVICE=wms&VERSION=1.3.0&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image%2Fpng&STYLES=' +
          // 'hoogspanningskabels,' +
          // 'middenspanningkabels,' +
          // 'laagspanningskabels,' +
          // 'laagspanningsverdeelkasten,' +
          // 'middenspanningsinstallaties,' +
          // 'onderstations' +
          '&layers=' +
          'hoogspanningskabels,' +
          // 'middenspanningkabels,' +
          // 'laagspanningskabels,' +
          // 'laagspanningsverdeelkasten,' +
          // 'middenspanningsinstallaties,' +
          // 'onderstations' +
          '&CRS=EPSG:3857&width=256&height=256&bbox={bbox-epsg-3857}',
      ],
      tileSize: 256,
      minzoom: 14,
    },
  },
  {
    sourceTitle: 'rioned',
    source: {
      type: 'raster',
      tiles: [
        'https://service.pdok.nl/rioned/beheerstedelijkwater/wms/v1_0?SERVICE=WMS&language=dut&VERSION=1.3.0&transparent=true&REQUEST=GetMap&FORMAT=image/png&' +
          'LAYERS=' +
          'AansluitingLeiding,' +
          'BeheerLeiding,' +
          'BeheerBouwwerk,' +
          'AansluitingPunt,' +
          'BeheerPomp,' +
          'BeheerLozing,' +
          'BeheerPut' +
          '&STYLES=' +
          'GWSW_Aansluitleiding,' +
          'GWSW_Leiding,' +
          'GWSW_Bouwwerk,' +
          'GWSW_Aansluitpunt,' +
          'GWSW_Pomp,' +
          'GWSW_Lozing,' +
          'GWSW_Put' +
          '&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
      ],
      tileSize: 256,
      minzoom: 13.6,
    },
  },

  // Bestemmingsplannen

  {
    sourceTitle: 'enkelbestemming',
    source: {
      type: 'raster',
      tiles: [
        `https://service.pdok.nl/kadaster/plu/wms/v1_0?SERVICE=WMS&VERSION=1.3.0&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=enkelbestemming&LAYERS=enkelbestemming&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}`,
      ],
      tileSize: 256,
      minzoom: 14,
    },
  },
  {
    sourceTitle: 'dubbelbestemming',
    source: {
      type: 'raster',
      tiles: [
        'https://service.pdok.nl/kadaster/plu/wms/v1_0?SERVICE=WMS&VERSION=1.3.0&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=dubbelbestemming&LAYERS=dubbelbestemming&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}',
      ],
      tileSize: 256,
      minzoom: 14,
    },
  },
  // bedrijvenBestemming: {
  //   source: {
  //     type: 'raster',
  //     tiles: [
  //       "https://geodata.nationaalgeoregister.nl/plu/wms?SERVICE=WMS&VERSION=1.3.0&TRANSPARENT=true&REQUEST=GetMap&FORMAT=image/png&STYLES=plu:Enkelbestemming&LAYERS=Enkelbestemming&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}&cql_filter=bestemmingshoofdgroep IN ('bedrijf','bedrijventerrein', 'kantoor' ,'detailhandel')",
  //     ],
  //     tileSize: 256,
  //     minzoom: 14,
  //   },
  // },

  // Overig
  {
    sourceTitle: 'searchGeometry',
    source: {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              name: 'Null Island',
            },
            geometry: {
              type: 'Point',
              coordinates: [0, 0],
            },
          },
        ],
      },
    },
  },
  {
    sourceTitle: 'luchtfotos',
    source: {
      type: 'raster',
      tileSize: 256,
      tiles: [
        'https://service.pdok.nl/hwh/luchtfotorgb/wms/v1_0?request=GetMap&service=wms&VERSION=1.3.0&TRANSPARENT=true&FORMAT=image/png&CRS=EPSG:3857&WIDTH=256&HEIGHT=256&bbox={bbox-epsg-3857}&Layers=Actueel_ortho25',
      ],
    },
  },
].reduce((memo, source) => {
  memo[source.sourceTitle] = source
  return memo
}, {})
