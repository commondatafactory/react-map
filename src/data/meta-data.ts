// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export interface MetaDataProps {
  referenceDate?: string
  url?: string
  updateFrequency?:
    | 'Jaarlijks'
    | '2x per jaar'
    | '2x per week'
    | 'Maandelijks'
    | 'Maandelijks geactualiseerd'
    | 'Eenmalig maatwerk'
    | '2x per week'
    | 'Onbekend'
    | 'Dagelijks'
  desc?: string
}

export interface MetaData {
  [metadataName: string]: MetaDataProps
}

export const metaDataList: MetaData = {
  BAG: {
    referenceDate: '2024-06',
    url: 'https://bagviewer.kadaster.nl',
    updateFrequency: 'Jaarlijks',
  },
  BAGdownload: {
    referenceDate: '2024-06',
    url: 'https://bagviewer.kadaster.nl',
    updateFrequency: 'Jaarlijks',
  },
  BAGverblijfsobject: {
    referenceDate: '2024-06',
    url: 'https://bagviewer.kadaster.nl',
    updateFrequency: 'Jaarlijks',
  },
  BAGverblijfsobjectDownload: {
    referenceDate: '2024-06',
    url: 'https://bagviewer.kadaster.nl',
    updateFrequency: 'Jaarlijks',
  },
  RVO: {
    referenceDate: '2024-06',
    updateFrequency: 'Jaarlijks',
  },
  // CBS: {
  //   referenceDate: '2019',
  // },
  CBS: {
    referenceDate: '2019',
    url: 'https://opendata.cbs.nl/statline/#/CBS/nl/dataset/84583NED/table?ts=1741097325744',
    desc: 'Kerncijfers wijken en buurten 2019',
  },
  CBSpc6: {
    referenceDate: '2022',
    url: 'https://www.cbs.nl/nl-nl/dossier/nederland-regionaal/geografische-data/gegevens-per-postcode',
    updateFrequency: 'Jaarlijks',
  },
  CBSEigenaren: {
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2021/02/energie-indicatoren-naar-regio-2018',
    referenceDate: '2018',
    updateFrequency: 'Eenmalig maatwerk',
  },
  CBSkookgas: {
    referenceDate: '2020-01-01',
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2021/47/woningen-met-kookgasaansluitingen-2020',
  },
  CBSEnergieIndicatoren: {
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2021/02/energie-indicatoren-naar-regio-2018',
    referenceDate: '2018',
  },
  CBSBereidheid: {
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2023/14/bereidheid-energietransitiemaatregelen-2021',
    referenceDate: '2021-01-01',
    updateFrequency: 'Eenmalig maatwerk',
  },
  CBSArmoede: {
    referenceDate: '2021',
    url: 'https://www.cbs.nl/nl-nl/maatwerk/2023/47/monitor-energiearmoede-2021',
    updateFrequency: 'Eenmalig maatwerk',
  },
  energie: {
    referenceDate: '2024-01-01',
    url: 'https://data.overheid.nl/datasets?sort=score%20desc%2C%20sys_modified%20desc&search=kleinverbruik&facet_theme%5B0%5D=http%3A//standaarden.overheid.nl/owms/terms/Natuur_en_milieu%7Chttp%3A//standaarden.overheid.nl/owms/terms/Energie',
    updateFrequency: 'Jaarlijks',
  },
  BAG3D: {
    referenceDate: '2024-06',
    url: 'https://bagviewer.kadaster.nl',
    updateFrequency: 'Jaarlijks',
  },
  EDSN: {
    referenceDate: '2024-06',
    updateFrequency: 'Jaarlijks',
  },
  KVK: {
    // referenceDate: '2024-10-03',
    updateFrequency: 'Dagelijks',
  },
  RDW: {
    url: 'https://opendata.rdw.nl/browse?category=Erkende+bedrijven&provenance=official',
    // referenceDate: '2024',
    updateFrequency: '2x per week',
  },
  BOVAG: {
    // referenceDate: '2024',
    url: 'https://www.bovag.nl/',
    updateFrequency: '2x per week',
  },
  Eigendom: {
    referenceDate: '2020-11',
  },
  klimaateffectatlas: {
    referenceDate: '2018',
    url: 'https://www.klimaateffectatlas.nl/nl/',
  },
  ruimtelijkeplannen: {
    url: 'https://nationaalgeoregister.nl/geonetwork/srv/dut/catalog.search#/metadata/17716ed7-ce0d-4bfd-8868-a398e5578a88?tab=general',
    referenceDate: '2023-10-01', //datum moet uit de metadata van de wms service gehaald gaan worden!  https://nationaalgeoregister.nl/geonetwork/srv/dut/q?_content_type=json&_draft=n+or+e&fast=index&uuid=17716ed7-ce0d-4bfd-8868-a398e5578a36 https://www.nationaalgeoregister.nl/geonetwork/srv/dut/csw?service=CSW&version=2.0.2&request=GetRecordById&outputschema=http://www.isotc211.org/2005/gmd&elementsetname=full&id=6c62e0a5-c215-4e47-94b0-a239e264417d
    updateFrequency: 'Maandelijks geactualiseerd',
  },
  brp: {
    url: 'https://www.nationaalgeoregister.nl/geonetwork/srv/dut/catalog.search#/metadata/1f7d475c-179d-4c71-89ca-4b5fd210ec18',
    referenceDate: '13-09-2023',
    updateFrequency: 'Onbekend',
  },
}
