// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { createGlobalStyle } from 'styled-components'

import { Fonts } from './fonts'
import { NlxDarkThemeVariables } from './nlx-dark-theme-variables'
import { NlxThemeVariables } from './nlx-theme-variables'
import { Normalize } from './normalize'
import { Reset } from './reset'

export const GlobalStyle = createGlobalStyle`

/* EXTERNAL FILES */

${Fonts}
${NlxDarkThemeVariables}
${NlxThemeVariables}
${Normalize}
${Reset}

/* ROOT */

:root {
  --app-height: 100vh;
  --app-width: 100vw;
}

body {
  background-color: var(--colorBackground);
  color: var(--colorText);
  margin: 0;
  overflow: hidden;
  padding: 0;
}

html {
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-family: "Source Sans Pro", ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";

  /* layout 0 - simplified */
  /* Mobile */
  @media screen and (min-width: 0px)   {
    /* background-color: yellow; */
    font-size: 14px;
  }
  /* Tablet & turned mobile */
  @media screen and (min-width: 568px)   {
    /* background-color: orange; */

  }
  /* Layout 1 - normal */
  /* turned tablet & small laptop */
  @media screen and (min-width: 900px)  {
    /* background-color: red; */
    font-size: 15px;

  }
  /* Larger laptops  */
  @media screen and (min-width: 1401px)  {
    /* background-color: blue; */
    font-size: 16px;



  }
  /* x large desktop screens */
  @media screen and (min-width: 1920px)  {
    /* background-color: purple; */
  }

}


#cssStylingTest {
  pointer-events: none;
  width: 100%;
  height: 100vh;
  opacity: 0.5;
  position:absolute;
  z-index: 1000000;

  /* layout 0 - simplified
  /* Mobile */
  @media screen and (min-width: 0px)   {
    background-color: yellow;
  }
  /* Tablet & turned mobile */
  @media screen and (min-width: 568px)   {
    background-color: orange;
  }
  /* Layout 1 - normal */
  /* turned tablet & small laptop */
  @media screen and (min-width: 900px)  {
    background-color: red;
  }
  /* Larger laptops  */
  @media screen and (min-width: 1401px)  {
    background-color: blue;
  }
  /* x large desktop screens */
  @media screen and (min-width: 1920px)  {
    background-color: purple;
  }

}

// Phones max-width 567px - mobile
// Tablets max-width 1024px
// Small screens max-width 1400px
// grote screens max-width 1920px
//  min-width 1920px  -




svg {
  flex-shrink: 0;
}

.ReactCollapse--collapse {
  transition: height 500ms;
}

/* ERROR MESSAGES FOR DESIGN SYSTEM INPUT */

p[data-testid|='error'] {
  > svg {
    display: initial;
    vertical-align: bottom;
  }
}

a {
  color: var(--colorTextLink)
}

@keyframes popIn {
  99% {
    visibility: hidden;
  }
  100% {
    visibility: visible;
  }
}
`
