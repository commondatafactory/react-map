// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import {
  createContext,
  Dispatch,
  FC,
  ReactNode,
  SetStateAction,
  useEffect,
  useMemo,
  useState,
} from 'react'

import { DataLayerProps } from '../data/data-layers'
import { useAccess } from '../hooks/use-access'
import useUser from '../hooks/use-user'

export interface TabProps {
  description?: string
  iconSlug?: string
  id: string
  title: string
}

interface TabLayerProviderProps {
  availableDataLayers: Partial<DataLayerProps>[]
  children: ReactNode
  tabs: TabProps[]
}

export interface TabLayerContextProps {
  activeDataLayers: Partial<DataLayerProps>[]
  isActiveDataLayer: (layer: Partial<DataLayerProps>) => boolean

  activeTab: TabProps
  // Only used to correctly select the sublayer's radio button in the menu
  // panel.
  activeSublayerName: string
  availableDataLayers: Partial<DataLayerProps>[]

  setActiveDataLayers: Dispatch<
    SetStateAction<TabLayerContextProps['activeDataLayers']>
  >
  setActiveTab: Dispatch<SetStateAction<TabLayerContextProps['activeTab']>>
  setActiveSublayerName: Dispatch<
    SetStateAction<TabLayerContextProps['activeSublayerName']>
  >

  visibleLayers: Partial<DataLayerProps>[]
  visibleTabs: TabProps[]
}

const TabLayerContext = createContext<TabLayerContextProps>(
  {} as TabLayerContextProps
)

const TabLayerProvider: FC<TabLayerProviderProps> = ({
  availableDataLayers,
  children,
  tabs,
}) => {
  const { hasAccess } = useAccess()
  const { data: user } = useUser()
  const [visibleLayers, setVisibleLayers] = useState([])
  const [activeTab, setActiveTab] = useState(null)
  const [activeDataLayers, setActiveDataLayers] = useState(
    [] as TabLayerContextProps['activeDataLayers']
  )
  const [activeSublayerName, setActiveSublayerName] = useState(null)

  // This grouping of layers per tab is only used to create the `visibleTabs`
  // variable below. If it weren't for that variable, we would only have to
  // calculate which layers should be visible at the moment (none, or only the
  // layers from the clicked tab).
  const layersPerTab = useMemo(() => {
    return tabs.reduce((memo, tab) => {
      memo[tab.id] = availableDataLayers.filter((layer) => {
        if (layer.id === 'layer0' || layer.tab !== tab.id) {
          return false
        }

        return hasAccess(layer, user) || layer.authLocked
      })

      return memo
    }, {})
  }, [tabs, user])
  // We determine the visible tabs after we determine the layers per tab,
  // because this latter step checks the uses's permissions per layer. If a tab
  // has no layers available, the tab can be hidden from view.
  const visibleTabs = useMemo(
    () => tabs.filter((tab) => layersPerTab[tab.id].length > 0),
    [tabs, user]
  )

  // TODO: Make plural activeDataLayers into singular, since there is always
  // just one data layer active.
  const isActiveDataLayer = (layer: Partial<DataLayerProps>) => {
    return !!activeDataLayers.find((activeLayer) => activeLayer.id === layer.id)
  }

  useEffect(() => {
    if (activeTab) {
      setVisibleLayers(layersPerTab[activeTab.id])
    }
  }, [activeTab, layersPerTab])

  useEffect(() => {
    const searchParams = new URLSearchParams(window.location.search)
    const tabKey = searchParams.get('tab')
    const layerKey = searchParams.get('layer')
    const subLayerKey = searchParams.get('sublayer')

    const newActiveTab = tabKey && visibleTabs.find((tab) => tab.id === tabKey)
    setActiveTab(newActiveTab || visibleTabs[0])

    if (!layerKey) {
      return
    }

    let activeLayer = availableDataLayers.find((l) => l.id === layerKey)
    if (!activeLayer || !hasAccess(activeLayer, user)) {
      return
    }

    if (activeLayer.sublayers) {
      if (!subLayerKey) {
        console.warn('Missing sublayer key')
        location.href = '/'
        return
      }

      const activeSubLayer = activeLayer.sublayers.find(
        (layer) => layer.id === subLayerKey
      )
      setActiveSublayerName(activeSubLayer.name)
      // FIX: This is a hack. A data layer with sublayers has no style layers
      // of its own, so this property is free to be abused. Which it is now.
      activeLayer = {
        ...activeLayer,
        styleLayers: activeSubLayer.styleLayers,
      }
    }

    setActiveDataLayers([activeLayer])
  }, [user])

  return (
    <TabLayerContext.Provider
      value={{
        isActiveDataLayer,
        activeDataLayers,
        activeSublayerName,
        activeTab,
        availableDataLayers,
        setActiveDataLayers,
        setActiveSublayerName,
        setActiveTab,
        visibleLayers,
        visibleTabs,
      }}
    >
      {children}
    </TabLayerContext.Provider>
  )
}

export { TabLayerProvider, TabLayerContext }
