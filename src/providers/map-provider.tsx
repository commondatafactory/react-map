// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, {
  Dispatch,
  useState,
  SetStateAction,
} from 'react'
import { useRouter } from 'next/router'
import { FeatureCollection } from 'geojson'
import {
  FilterSpecification,
  LngLat,
  LngLatLike,
  MapLayerMouseEvent,
} from 'maplibre-gl'
import { StyleLayer } from '../data/data-layers'

export type CustomFilter = {
  id: string
  filter: FilterSpecification
  list: string[]
}

export type SelectedFeatureAndEvent = {
  event: {
    lngLat: LngLat
    type: string
  }
  features: MapLayerMouseEvent['features']
  relevantStyleLayerDetailModal: Partial<StyleLayer>
}

type SearchGeometry = {
  centroide_ll: {
    type: string
    coordinates: LngLatLike
  }
  // TODO: Proper GeoJSON type
  geometrie_ll: {
    type: string
    coordinates: any
  }

  // There are more types, might be relevant. See `zoomLevelInrichting` in `MapContextLayers`
  type: string // 'gemeente' | 'woonplaats'
  gemeentecode: string
  gemeentenaam: string
}

export interface MapContextProps {
  customFilters: CustomFilter[]
  setCustomFilters: Dispatch<SetStateAction<MapContextProps['customFilters']>>
  searchGeometry: SearchGeometry
  threeDimensional: boolean
  mapLoading: boolean
  analyzeMode: 'count' | 'watching-selection' | 'creating-data'

  setAnalyzeMode: Dispatch<SetStateAction<MapContextProps['analyzeMode']>>
  setSearchGeometry: Dispatch<SetStateAction<MapContextProps['searchGeometry']>>
  setMapLoading: Dispatch<SetStateAction<MapContextProps['mapLoading']>>
  toggleThreeDimensional: Dispatch<SetStateAction<void>>
  featureCollection?: FeatureCollection | Record<string, never>
  selectedFeatureAndEvent: SelectedFeatureAndEvent
  setSelectedFeatureAndEvent: Dispatch<
    SetStateAction<MapContextProps['selectedFeatureAndEvent']>
  >
}

const MapContext = React.createContext<MapContextProps>({} as MapContextProps)

const MapProvider = ({ children }) => {
  const router = useRouter()
  const { asPath } = router || {}

  const [searchGeometry, setSearchGeometry] = useState(null)

  const [threeDimensional, setthreeDimensional] = useState(
    asPath?.includes('/0/40')
  )
  const [mapLoading, setMapLoading] = useState(true)

  const [selectedFeatureAndEvent, setSelectedFeatureAndEvent] = useState(null)
  const [customFilters, setCustomFilters] = useState()
  const [analyzeMode, setAnalyzeMode] = useState()

  // const [selectedFeaturesInfo, setSelectedFeaturesInfo] = useState({
  //   features: {},
  // })

  const toggleThreeDimensional = () => {
    setthreeDimensional(!threeDimensional)
  }

  return (
    <MapContext.Provider
      value={{
        customFilters,
        setCustomFilters,
        searchGeometry,
        threeDimensional,
        mapLoading,
        setSearchGeometry,
        setMapLoading,
        toggleThreeDimensional,
        analyzeMode,
        setAnalyzeMode,
        selectedFeatureAndEvent,
        setSelectedFeatureAndEvent,
      }}
    >
      {children}
    </MapContext.Provider>
  )
}

export { MapProvider, MapContext }
