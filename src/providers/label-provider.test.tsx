// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { render } from '@testing-library/react'
import { LabelProvider, LabelContext } from './label-provider'

describe('LabelProvider', () => {
  it('should have the correct initial state', () => {
    render(
      <LabelProvider>
        <LabelContext.Consumer>
          {() => {
            return <div />
          }}
        </LabelContext.Consumer>
      </LabelProvider>
    )

    expect(window.location.href.includes('label=topo')).toBeTruthy()
  })
})
