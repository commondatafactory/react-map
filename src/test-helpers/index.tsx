// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { ThemeProvider } from 'styled-components'
import { defaultTheme } from '@commonground/design-system'
import { Queries, render, RenderResult } from '@testing-library/react'
import AppContext, { ConfigProps } from '../components/AppContext'
import '@testing-library/jest-dom/extend-expect'
import devConfig from '../../app.json'
import {
  TabLayerContext,
  TabLayerContextProps,
} from '../providers/tab-layer-provider'

interface CustomRenderOptions {
  children?: JSX.Element
  configOverrides?: Partial<ConfigProps>
  layerContextOverrides?: Partial<TabLayerContextProps>
  testingLibraryOptions?: any
}

// based on https://testing-library.com/docs/react-testing-library/setup#custom-render
const AllTheProviders = ({
  children,
  configOverrides,
  layerContextOverrides,
}: CustomRenderOptions) => {
  return (
    <ThemeProvider theme={defaultTheme}>
      <AppContext.Provider
        value={{
          ...(devConfig as ConfigProps),
          ...configOverrides,
        }}
      >
        <TabLayerContext.Provider
          value={
            {
              visibleLayers: [],
              setVisibleLayers: jest.fn(),
              activeTab: '',
              setActiveTab: jest.fn(),
              tabs: [],
            } as any
          }
        >
          {children}
        </TabLayerContext.Provider>
      </AppContext.Provider>
    </ThemeProvider>
  )
}

const renderWithProviders = (
  ui: React.ReactElement,
  options?: CustomRenderOptions
): RenderResult<Queries, HTMLElement> =>
  render(ui, {
    wrapper: (props: any) => (
      <AllTheProviders
        {...props}
        configOverrides={options?.configOverrides}
        layerContextOverrides={options?.layerContextOverrides}
      />
    ),
    ...options?.testingLibraryOptions,
  })

// re-export everything
export * from '@testing-library/react'

// override render method
export { renderWithProviders }
