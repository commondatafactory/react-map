// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import dsVb from '../fixtures/ds-vb-energie.json'

describe('The modal count component', () => {
  beforeEach(() => {
    cy.intercept({ method: 'GET', url: /^https:\/\/files/ }, {}).as('files')
    cy.intercept({ method: 'POST', url: /^https:\/\/ds/ }, dsVb).as('energie')
    cy.intercept({ method: 'GET', url: /^https:\/\/acc/ }, {}).as('acceptatie')
    cy.intercept({ method: 'GET', url: '/api/**' }, {}).as('api')

    cy.visit('/')

    cy.wait('@files')
    cy.wait('@api')
  })

  it('shows the graph title when "verblijfsobjecten" are selected on the map', () => {
    cy.get('[data-cy="tab-Gebouwen"]').click()

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(200) // NOTE: something conflicts within the React Collapse package?
    cy.contains('Bouwjaar').should('be.visible')

    cy.contains('Bouwjaar').click()
    cy.contains('Het bouwjaar').should('be.visible')

    cy.contains('Gebied analyseren').click()

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(200)

    cy.get('.maplibregl-map').click(100, 100)
    cy.get('.maplibregl-map').click(200, 200)
    cy.get('.maplibregl-map').click(50, 200)
    cy.get('.maplibregl-map').click(50, 200)

    cy.contains('Verdeling Bouwjaar van verblijfsobjecten').should('be.visible')
  })
})
