// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

describe('Test API', () => {
  it('should return 200 and an array greater than zero results', () => {
    cy.request(
      'GET',
      'https://api.pdok.nl/bzk/locatieserver/search/v3_1/free?q=q%3DUtrecht&fl=id%20identificatie%20weergavenaam%20bron%20type%20subtype%20openbareruimte_id%20nwb_id%20openbareruimtetype%20straatnaam%20straatnaam_verkort%20adresseerbaarobject_id%20nummeraanduiding_id%20huisnummer%20huisletter%20huisnummertoevoeging%20huis_nlt%20postcode%20buurtnaam%20buurtcode%20wijknaam%20wijkcode%20woonplaatscode%20woonplaatsnaam%20gemeentecode%20gemeentenaam%20provinciecode%20provincienaam%20provincieafkorting%20kadastraal_object_id%20kadastrale_gemeentecode%20kadastrale_gemeentenaam%20kadastrale_sectie%20perceelnummer%20kadastrale_grootte%20gekoppeld_perceel%20kadastrale_aanduiding%20volgnummer%20gekoppeld_appartement%20wegnummer%20hectometernummer%20zijde%20hectometerletter%20waterschapsnaam%20waterschapscode%20rdf_seealso%20centroide_ll%20centroide_rd%20score&fq=type%3A%28gemeente%20OR%20woonplaats%20OR%20weg%20OR%20postcode%20OR%20adres%29&df=tekst&bq=type%3Aprovincie%5E1.5&bq=type%3Agemeente%5E1.5&bq=type%3Awoonplaats%5E1.5&bq=type%3Aweg%5E1.5&bq=type%3Apostcode%5E0.5&bq=type%3Aadres%5E1&start=0&rows=10&sort=score%20desc%2Csortering%20asc%2Cweergavenaam%20asc&wt=json'
    ).then((response) => {
      expect(response.status).to.eq(200)
      expect(response.body.response.docs).length.to.be.greaterThan(0)
    })
  })
})
