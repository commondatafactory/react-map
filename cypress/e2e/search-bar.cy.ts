// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

describe('The search bar', () => {
  beforeEach(() => {
    cy.intercept({ method: 'GET', url: /^https:\/\/files/ }, {}).as('files')
    cy.intercept({ method: 'POST', url: /^https:\/\/ds/ }, {}).as('energie')
    cy.intercept({ method: 'GET', url: /^https:\/\/acc/ }, {}).as('acceptatie')
    cy.intercept({ method: 'GET', url: '/api/**' }, {}).as('api')

    cy.intercept(
      'GET',
      'https://api.pdok.nl/bzk/locatieserver/search/v3_1/*q=*Ut*',
      { fixture: 'utrecht.json' }
    ).as('utrecht-fixture')

    cy.visit('/')
  })

  it('shows the correct result when entering a search query', () => {
    cy.get('[data-cy="location-search-input"]').click()
    cy.get('[data-cy="location-search-input"]').type('Utrecht', { delay: 300 })
    cy.get('[data-cy="location-search-results"]').should('be.visible')
    cy.get('[data-cy="location-search-results"] > li').should(
      'have.length.of.at.most',
      8
    )
    cy.contains('gemeente').should('be.visible')
    cy.contains('provincie').should('be.visible')
    cy.contains('woonplaats').should('be.visible')
    cy.contains('Gemeente Utrecht').click()
    cy.get('[data-cy="location-search-results"]').should('not.exist')
    cy.get('[data-cy="location-search-input"]').next().type('{enter}')
    cy.get('[data-cy="location-search-input"]').click()
  })
})
