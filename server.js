// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()
const cors = require('cors')

server.use(
  cors({
    origin: true,
    credentials: true,
    preflightContinue: false,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  })
)
server.options('*', cors())

let isAuthorized = false

server.get('/api/users/whoami', (req, res) => {
  if (isAuthorized) {
    res.jsonp({
      id: '5ca8e3c1-4a0c-458b-9288-3bd98df88a39',
      active: true,
      expires_at: '2026-11-23T14:01:02.104065Z',
      authenticated_at: '2024-11-22T14:01:02.104065Z',
      issued_at: '2024-11-22T14:01:02.104065Z',
      identity: {
        id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
        schema_id: 'user',
        schema_url: 'https://acc.id.commondatafactory.nl/schemas/dXNlcg',
        state: 'active',
        state_changed_at: '2024-11-19T11:39:18.78872Z',
        traits: {
          name: {
            last: 'Mortier',
            first: 'Daan',
          },
          email: 'daan@mortier.email',
        },
        verifiable_addresses: [
          {
            id: 'e718941e-8a07-4632-87e5-359253c201b5',
            value: 'daan@mortier.email',
            verified: true,
            via: 'email',
            status: 'completed',
            verified_at: '2024-11-19T11:40:02.930336Z',
          },
        ],
        recovery_addresses: [
          {
            id: '8279fb16-33c2-43d5-bc38-c7da41a8e581',
            value: 'daan@mortier.email',
            via: 'email',
          },
        ],
      },
      permissions: [
        {
          namespace: 'subdivisions',
          object: 'energie_dego',
          relation: 'issuer',
          subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
          id: 'energie_dego',
          label: 'Energie',
          type: 'role',
          roleId: 'energie',
        },
        {
          namespace: 'subdivisions',
          object: 'autobranche_demo_gm0848',
          relation: 'issuer',
          subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
          id: 'autobranche_demo_gm0848',
          label: 'Autobranche',
          type: 'role',
          roleId: 'autobranche',
        },
        {
          namespace: 'subdivisions',
          object: 'bedrijventerreinen_demo_gm0848',
          relation: 'issuer',
          subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
          id: 'bedrijventerreinen_demo_gm0848',
          label: 'Bedrijventerreinen',
          type: 'role',
          roleId: 'bedrijventerreinen',
        },
        {
          namespace: 'organizations',
          object: 'demo_gm0848',
          relation: 'access',
          subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
          id: 'demo_gm0848',
          label: 'Demo gemeente Son en Breugel',
          type: 'organization',
        },
        {
          namespace: 'organizations',
          object: 'demo_gm0848',
          relation: 'issuer',
          subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
          id: 'demo_gm0848',
          label: 'Demo gemeente Son en Breugel',
          type: 'organization',
        },
        {
          namespace: 'organizations',
          object: 'dego',
          relation: 'issuer',
          subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
          id: 'dego',
          label: 'DEGO',
          type: 'organization',
        },
        {
          namespace: 'subdivisions',
          object: 'bedrijventerreinen_demo_gm0848',
          relation: 'access',
          subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
          id: 'bedrijventerreinen_demo_gm0848',
          label: 'Bedrijventerreinen',
          type: 'role',
          roleId: 'bedrijventerreinen',
        },
        {
          namespace: 'subdivisions',
          object: 'buitengebied_demo_gm0848',
          relation: 'issuer',
          subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
          id: 'buitengebied_demo_gm0848',
          label: 'Buitengebied',
          type: 'role',
          roleId: 'buitengebied',
        },
      ],
      sortedPermissions: {
        subdivisions: {
          issuer: [
            {
              namespace: 'subdivisions',
              object: 'energie_dego',
              relation: 'issuer',
              subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
              id: 'energie_dego',
              label: 'Energie',
              type: 'role',
              roleId: 'energie',
            },
            {
              namespace: 'subdivisions',
              object: 'autobranche_demo_gm0848',
              relation: 'issuer',
              subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
              id: 'autobranche_demo_gm0848',
              label: 'Autobranche',
              type: 'role',
              roleId: 'autobranche',
            },
            {
              namespace: 'subdivisions',
              object: 'bedrijventerreinen_demo_gm0848',
              relation: 'issuer',
              subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
              id: 'bedrijventerreinen_demo_gm0848',
              label: 'Bedrijventerreinen',
              type: 'role',
              roleId: 'bedrijventerreinen',
            },
            {
              namespace: 'subdivisions',
              object: 'buitengebied_demo_gm0848',
              relation: 'issuer',
              subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
              id: 'buitengebied_demo_gm0848',
              label: 'Buitengebied',
              type: 'role',
              roleId: 'buitengebied',
            },
          ],
          access: [
            {
              namespace: 'subdivisions',
              object: 'bedrijventerreinen_demo_gm0848',
              relation: 'access',
              subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
              id: 'bedrijventerreinen_demo_gm0848',
              label: 'Bedrijventerreinen',
              type: 'role',
              roleId: 'bedrijventerreinen',
            },
          ],
        },
        organizations: {
          access: [
            {
              namespace: 'organizations',
              object: 'demo_gm0848',
              relation: 'access',
              subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
              id: 'demo_gm0848',
              label: 'Demo gemeente Son en Breugel',
              type: 'organization',
            },
          ],
          issuer: [
            {
              namespace: 'organizations',
              object: 'demo_gm0848',
              relation: 'issuer',
              subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
              id: 'demo_gm0848',
              label: 'Demo gemeente Son en Breugel',
              type: 'organization',
            },
            {
              namespace: 'organizations',
              object: 'dego',
              relation: 'issuer',
              subject_id: 'c84fa201-dd3c-442b-a8ce-c40de14a7ac9',
              id: 'dego',
              label: 'DEGO',
              type: 'organization',
            },
          ],
        },
      },
      userType: {
        id: 'analyst',
        type: 'user',
        label: 'Data-analist',
      },
    })
  } else {
    res.status(401).jsonp({
      error: {
        code: 401,
        status: 'Unauthorized',
        request: '478c5f52640fd82faa4add449c9a3f28',
        reason: 'No valid session cookie found.',
        message: 'The request could not be authorized',
      },
    })
  }
})

const getReturnUrl = (url) => {
  let returnUrl = decodeURIComponent(url)
  returnUrl = returnUrl.split('return_to=')
  returnUrl.shift()
  return returnUrl.join('')
}

server.get('/login', (req, res) => {
  isAuthorized = true

  const returnUrl = getReturnUrl(req.originalUrl)
  res.redirect(returnUrl)
})

server.get('/self-service/logout/browser', (req, res) => {
  res.jsonp({
    logout_url:
      'http://localhost:3080/self-service/logout?token=SyyBbke21l0kWCrAugUNuLecQ9by8eDI',
    logout_token: 'SyyBbke21l0kWCrAugUNuLecQ9by8eDI',
  })
})

server.get('/self-service/logout', (req, res) => {
  isAuthorized = false
  const returnUrl = getReturnUrl(req.originalUrl)
  res.redirect(returnUrl)
})

server.use(middlewares)

server.use(router)
server.listen(3000, () => {
  // eslint-disable-next-line no-console
  console.log('JSON Server is running on port 3000')
})
