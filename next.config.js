// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

module.exports = {
  output: 'standalone',

  typescript: {
    ignoreBuildErrors: true,
  },

  async headers() {
    return [
      {
        // Set CORS headers
        source: '/(.*)',
        headers: [
          {
            key: 'Content-Security-Policy',
            value:
              "default-src * 'unsafe-inline' 'unsafe-eval' blob:; script-src * blob: 'unsafe-inline' 'unsafe-eval'; connect-src * 'unsafe-inline'; img-src * data: blob: 'unsafe-inline'; frame-src *; style-src * 'unsafe-inline'",
          },
          {
            key: 'Access-Control-Allow-Origin',
            value:
              'http://devmap.example.com, http://map.example.com, http://localhost:3001, http://authorized.data.vng.com http://login.example.com http://login.vng.com',
          },
          {
            key: 'Access-Control-Allow-Credentials',
            value: 'true',
          },
          {
            key: 'Access-Control-Allow-Headers',
            value: '*',
          },
          {
            key: 'Referrer-Policy',
            value: 'same-origin',
          },
        ],
      },
    ]
  },
}
